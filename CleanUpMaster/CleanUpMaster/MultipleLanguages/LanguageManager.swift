//
//  LanguageManager.swift
//  SMSLocalization
//
//  Created by Macuser on 5/19/16.
//  Copyright © 2016 Macuser. All rights reserved.
//

import UIKit

class LanguageManager: NSObject {
    var availableLocales = [Locale]()
    static let sharedInstance = LanguageManager()
    
    override init() {
        let english = Locale1().initWithLanguageCode(languageCode: "en", countryCode: "gb", name: "United Kingdom")
        let finnish = Locale1().initWithLanguageCode(languageCode: "vi", countryCode: "vi", name: "VietNamese")
        self.availableLocales = [english as! Locale, finnish as! Locale]
    }
    
    func getCurrentBundle() -> Bundle {
        let bundlePath = Bundle.main.path(forResource: getSelectedLocale(), ofType: "lproj")
        let bundle = Bundle(path: bundlePath!)
        return bundle!
    }
    
    private func getSelectedLocale() -> String {
        let lang = NSLocale.preferredLanguages
        let languageComponents = NSLocale.components(fromLocaleIdentifier: lang[0])
        if let languageCode = languageComponents["kCFLocaleLanguageCodeKey"] {
            for locale in availableLocales {
                if locale.languageCode == languageCode {
                    return locale.languageCode!
                }
            }
        }
        return "en"
    }
    
    func setLocale(langCode: String) {
        UserDefaults.standard.set([langCode], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
    }
}

class Locale1: NSObject {
    var name: String?
    var languageCode: String?
    var countryCode: String?
    
    func initWithLanguageCode(languageCode: String, countryCode: String, name: String) -> AnyObject {
        self.name = name
        self.languageCode = languageCode
        self.countryCode = countryCode
        return self
    }
}

extension String {
    func localized() -> String {
        return NSLocalizedString(self, comment: self)
    }
}
