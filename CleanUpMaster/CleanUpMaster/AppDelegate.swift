//
//  AppDelegate.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 06/01/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    lazy var window: UIWindow? = {
        UIWindow(frame: UIScreen.main.bounds)
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        IPAService.shared.checkTransactionState()
        DeviceManagement.shared().addNetworkNotify(true)

        self.createFolder()
        let dashboardVC = DashboardViewController()
        let nav = UINavigationController(rootViewController: dashboardVC)
        nav.navigationBar.tintColor = .white
        nav.navigationBar.barTintColor = UIColor.colorFromHex("#0B96E0")
        nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        nav.isNavigationBarHidden = true
        window?.rootViewController = nav
        window?.makeKeyAndVisible()

        return true
    }
    
    private func createFolder() {
        if let url = FileProvider.documentsURL {
            if !url.checkFileExist() {
                FileProvider.shared.createFolder { bool in
                    print("Create folder")
                }
            }
        }else {
            print("Not exist FileManager.default.fileExists(atPath: urlStr)")
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }
}

