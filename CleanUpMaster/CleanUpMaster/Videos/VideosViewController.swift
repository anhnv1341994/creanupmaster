//
//  VideosViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/08/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Photos

class VideosViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    private let btnDelete = ButtonDeleteView()
    @IBOutlet weak var roundedView: RoundedView!

    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "kVideos".localized()
        }
    }

    @IBOutlet weak var lbNoData: UILabel! {
        didSet {
            lbNoData.text = "kNoVideos".localized()
        }
    }

    @IBOutlet weak var btnRightNav: UIButton!
    fileprivate var checkSelected: [Bool] = []

    private var isSelectAll = true {
        didSet {
            self.btnRightNav.setTitle(isSelectAll ? "kDeselectAll".localized() : "kSelectAll".localized(), for: .normal)
        }
    }
    let video = "kVideo".localized()
    let selected = "kSelected".localized()
    let ks = "kS".localized()

    private var countSelect: Int = 0 {
        didSet {

            self.navTitle.text = "\(countSelect > 0 ? "\(countSelect) " : "")\(video)\((countSelect > 1) ? "\(ks)" : "") \(selected)"
            self.isSelectAll = (countSelect == photos.count && countSelect != 0)

            self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: countSelect > 0)
            self.showButton(count: self.photos.count)
        }
    }

    var photos: [PHAsset] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.countSelect = 0
        self.setupButtonDelete()
        self.setupCollectionView()
        self.fetchAssetsAll()
        self.reloadData()
        PHPhotoLibrary.shared().register(self)
    }

    @IBAction func actionSelected(_ sender: Any) {
        self.isSelectAll = !self.isSelectAll
        self.checkSelected.removeAll()
        for _ in 0 ..< photos.count {
            self.checkSelected.append(isSelectAll)
        }
        self.countSelect = isSelectAll ? photos.count : 0

        self.reloadData()
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self

        if let flowLayout = self.collectionView.collectionViewLayout as? GridCollectionViewLayout {
            flowLayout.scrollDirection = .vertical
            flowLayout.sectionInset = UIEdgeInsets.init(top: -1, left: 1, bottom: 0, right: 1)
            flowLayout.interitemSpacing = 1
            flowLayout.lineSpacing = 1
            flowLayout.aspectRatio = 1
            flowLayout.footerReferenceLength = 30
            flowLayout.numberOfItemsPerLine = 4
        }
        collectionView.register(cellType: PhotoCollectionViewCell.self)
        collectionView.register(viewType: FooterView.self, forElementKind: UICollectionView.elementKindSectionFooter)
    }

    fileprivate func reloadData() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    private func fetchAssets() { // 1
        self.checkSelected.removeAll()
        for _ in 0 ..< photos.count {
            self.checkSelected.append(false)
            print("checkSelected")
        }
        self.showButton(count: self.photos.count)
        self.reloadData()
    }

    private func setupButtonDelete() {
        self.btnDelete.btnDelete.setTitle("kDeleteSelected".localized(), for: .normal)
        self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: false, alpha: 0)
        self.btnDelete.gotoPolicyView = { [weak self] type in
            let vc = PrivacyPolicyViewController()
            vc.type = type
            self?.pushViewWithScreen(vc)
        }

        self.btnDelete.didDelete = { [weak self] in
            guard let self = self else { return }
            var listRecybin: [String] = []
            if let currentRecybin = UserDefaults.listRecybin {
                listRecybin = currentRecybin
            }
            let group = DispatchGroup()
            var photosDraft: [PHAsset] = []
            var checkSelectedDraft: [Bool] = []

            for (index, isCheck) in self.checkSelected.enumerated() {
                group.enter()

                if isCheck {
                    listRecybin.append(self.photos[index].localIdentifier)
                    group.leave()
                } else {
                    checkSelectedDraft.append(self.checkSelected[index])
                    photosDraft.append(self.photos[index])
                    group.leave()
                }
            }

            group.notify(queue: .main) {
                self.checkSelected = checkSelectedDraft
                self.photos = photosDraft
                self.countSelect = self.checkSelected.filter { $0 == true }.count
                UserDefaults.listRecybin = listRecybin
                self.reloadData()
            }
        }
    }

    func fetchAssetsAll() {
        self.photos.removeAll()
        let listRecybin = UserDefaults.listRecybin ?? []

        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [
            NSSortDescriptor(
                key: "creationDate",
                ascending: false)
        ]

        fetchOptions.predicate = NSPredicate(format: "mediaType = %d ", PHAssetMediaType.video.rawValue)

        let allVideo = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        allVideo.enumerateObjects { (asset, index, bool) in
            if !listRecybin.contains(asset.localIdentifier) {
                self.photos.append(asset)
            }

            self.fetchAssets()
        }
    }
}

extension VideosViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footerView = collectionView.dequeueReusableCell(with: FooterView.self, elementKind: UICollectionView.elementKindSectionFooter, for: indexPath)
        if indexPath.section != 0 {
            footerView.lbTtitle.text = nil
        } else {
            footerView.lbTtitle.text = self.photos.count.toString + " \(video)\(self.photos.count > 0 ? "\(ks)" : "")"
        }
        return footerView
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PhotoCollectionViewCell.self, for: indexPath)
        let photo = self.photos[indexPath.item]
        cell.photoView.fetchImageAsset(photo, targetSize: cell.bounds.size) { _ in
//          cell.blurView.isHidden = !success
        }
        let check = self.checkSelected[indexPath.item]
        cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        cell.blurView.isHidden = !check
        cell.showImagePlay()
        cell.selectedPhoto = { [weak self] in
            guard let self = self else { return }

            self.checkSelected[indexPath.item] = !self.checkSelected[indexPath.item]
            let check = self.checkSelected[indexPath.item]
            cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
            self.countSelect = self.checkSelected.filter { $0 == true }.count
            self.isSelectAll = !self.checkSelected.contains(false)
        }
        return cell
    }
}

extension VideosViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let vc = PreviewPhotoViewController()
        vc.indexSelected = indexPath.item
        vc.photos = self.photos
        vc.checkSelected = self.checkSelected
        vc.delegate = self
        self.pushViewWithScreen(vc)
    }
}

extension VideosViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
    }
}

extension VideosViewController: PreviewPhotosDelegate {
    func didSelectedImage(index: Int) {
        self.checkSelected[index] = !self.checkSelected[index]
        self.countSelect = self.checkSelected.filter { $0 == true }.count
        self.isSelectAll = !self.checkSelected.contains(false)
        self.reloadData()
    }

    func showButton(count: Int) {
        if self.collectionView != nil, self.btnRightNav != nil {
            self.collectionView.isHidden = count == 0
            self.btnRightNav.alpha = count == 0 ? 0 : 1
            self.btnRightNav.isEnabled = count == 0 ? false : true
        }
    }
}
