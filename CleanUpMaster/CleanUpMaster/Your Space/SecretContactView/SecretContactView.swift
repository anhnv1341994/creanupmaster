//
//  SecuredView.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 11/04/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import SwiftyContacts

class SecretContactView: CustomViewXib {
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lbTitleLabel: UILabel! {
        didSet {
            lbTitleLabel.text = "btnSecretContact".localized()
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            self.tableView.sectionHeaderHeight = 30
            self.tableView.rowHeight = 50
            self.tableView.estimatedRowHeight = 50
            self.tableView.register(cellType: ContactTableViewCell.self)
        }
    }
    @IBOutlet weak var leftBarButton: UIButton! {
        didSet {
            leftBarButton.setTitle(titleDone, for: .normal)
        }
    }

    @IBOutlet weak var rightBarButton: UIButton! {
        didSet {
            rightBarButton.setTitle(titleSelect, for: .normal)
        }
    }
    
    
    @IBOutlet weak var lbNoData: UILabel! {
        didSet {
            lbNoData.text = "kNoSecrectContactTitle".localized()
        }
    }
    
    @IBOutlet weak var lbNoDataContent: UILabel! {
        didSet {
            lbNoDataContent.text = "kNoSecrectContactContent".localized()
        }
    }

    @IBOutlet weak var btnSharedContacts: UIButton! { didSet { btnSharedContacts.isHidden = true } }
    @IBOutlet weak var btnDeleteContacts: UIButton! { didSet { btnDeleteContacts.isHidden = true } }

    private var isShowBottomButton = false {
        didSet {
            self.btnSharedContacts.isHidden = !isShowBottomButton
            self.btnDeleteContacts.isHidden = !isShowBottomButton
        }
    }
    private let titleDone = "kClose".localized()
    private let titleSelect = "kSelect".localized()
    private let titleSelectAll = "kSelectAll".localized()
    private let titleDeselectAll = "kDeselectAll".localized()

    weak var homeVC: DashboardViewController?

    var totalContact = 0
    let contactServices = ContactsManagerment()

    private var listGroup = [ContactGroup]()
    private var resultContacts = [ContactGroup]()
    private var contacts: [Contact] = []
    var isSearching: Bool = false

    private var data: [ContactGroup] {
        if self.isSearching {
            return self.resultContacts
        }
        return self.listGroup
    }

    private var countSelect: Int = 0 {
        didSet {
            let contact = "kContact".localized()
            let selected = "kSelected".localized()
            let ks = "kS".localized()
            
            leftBarButton.setTitle(titleDone, for: countSelect > 0 ? .normal : .disabled)
            self.lbTitleLabel.text = "\(countSelect > 0 ? "\(countSelect) " : "")\(contact)\((countSelect > 1) ? "\(ks)" : "") \(selected)"
            self.isSelectAll = (countSelect == totalContact && countSelect != 0)
            self.isShowBottomButton = countSelect > 0
        }
    }

    private var isSelectAll = false {
        didSet {
            self.rightBarButton.setTitle(isSelectAll ? titleDeselectAll : titleSelectAll, for: .normal)
        }
    }

    private var isChooseSelect = false {
        didSet {
            self.tableView.reloadData()
            if isChooseSelect {
                isSelectAll = false

            } else {
                countSelect = 0
                rightBarButton.setTitle(titleSelect, for: .normal)

                for contact in self.contacts {
                    contact.isSelect = false
                }
                self.requestContact(contacts: self.contacts)
            }
        }
    }

    private var selectContactVC: SelectContactView?
    private var pointTop: CGRect = .zero
    private var pointBottom: CGRect = .zero
    private var scrollToTop: Bool = true
    var didCloseView: (() -> Void)?

    override func setupUI() {
        self.requestContactSecret()
    }

    @IBAction func actionSharedContacts(_ sender: UIButton) {
        var contacts: [Contact] = []

        for section in self.data {
            contacts += section.contactsSelect
        }
        self.homeVC?.sharedContact(contacts: contacts.map({ $0.contact }))
    }

    private func deleteContacts(_ contacts: [Contact]) {
        let delete = "kDelete".localized()
        let contact = "kContact".localized()
        let kss = "kSs".localized()
        
        let message = "\(delete) \(self.totalContact - contacts.count) \(contact)\(kss)?"
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "kCanncel".localized(), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "kAlertOK".localized(), style: .default, handler: { _ in
            UserDefaults.listContact = try? CNContactVCardSerialization.data(with: contacts.map({ $0.contact }))
            self.countSelect = 0
            self.requestContact(contacts: contacts)
        }))
        self.homeVC?.present(alert, animated: true, completion: nil)
    }

    private func addToMyContact(_ contacts: [Contact]) {
        var contactsSelect: [Contact] = []
        for section in self.data {
            contactsSelect += section.contactsSelect
        }

        UserDefaults.listContact = try? CNContactVCardSerialization.data(with: contacts.map({ $0.contact }))
        self.contacts = contacts
        self.countSelect = 0
        if contacts.count > 0 {
            self.requestContact(contacts: contacts)
        } else {
            self.contacts = []
            self.listGroup = []
            self.isChooseSelect = false
            self.reloadData()
        }

        let group = DispatchGroup()
        for contact in contactsSelect {
            group.enter()
            addContact(Contact: contact.contact.mutableCopy() as! CNMutableContact) { result in
                group.leave()
                switch result {
                case .success(let bool):
                    if bool {
                        print("addContact")
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }

    @IBAction func actionDeleteContacts(_ sender: UIButton) {
        var contacts: [Contact] = []

        for section in self.data {
            contacts += section.contactsNotSelect
        }

        self.homeVC?.showAlerYesNo(title: "kChooseAaction".localized(), "kDeleteSelected".localized(), "kSaveContact".localized(), "", yesHandler: { _ in
                self.deleteContacts(contacts)
            }, noHandler: { _ in
                self.addToMyContact(contacts)
            }, type: .actionSheet)
    }

    @IBAction func actionClose(_ sender: UIButton) {
        if self.isChooseSelect == false {
            self.didCloseView?()
        } else {
            self.isChooseSelect = false
        }
    }

    @IBAction func actionSelect(_ sender: UIButton) {
        if self.isChooseSelect == false {
            self.isChooseSelect = true
        } else {
            for section in self.data {
                for item in section.duplicates {
                    item.isSelect = !self.isSelectAll
                }
            }
            self.countSelect = self.getCountSelect()
            self.reloadData()
        }
    }

    @IBAction func actionAddContact(_ sender: UIButton) {
        self.selectContactVC = SelectContactView()
        guard let selectContactVC = self.selectContactVC else { return }
        self.addSubview(selectContactVC)
        selectContactVC.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handPanGesture(_:))))
        self.pointTop = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.pointBottom = CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: self.frame.height)
        selectContactVC.frame = self.pointBottom
        selectContactVC.homeVC = self.homeVC

        UIView.animate(withDuration: 0.3) {
            selectContactVC.frame = self.pointTop
        }

        selectContactVC.didClose = { [unowned self] contacts in
            self.isChooseSelect = false
            self.contacts += contacts
            let contacts = self.contacts

            self.requestContact(contacts: contacts)
            UserDefaults.listContact = try? CNContactVCardSerialization.data(with: self.contacts.map({ $0.contact }))

            if UserDefaults.isEnableContacts == true { // remove after import
                let deletContact = "kDeleteContact".localized()
                let ks = "kS".localized()
                let allow = "kAllow".localized()
                let dontAllow = "kDontAllow".localized()

                self.showAlerYesNo(title: "\(deletContact)\(contacts.count > 0 ? "\(ks)" : "")", "kDelete".localized(), dontAllow, "\(allow)\(contacts.count > 0 ? "\(ks)" : "")", yesHandler: { _ in
                        self.deleteContact(contacts)
                    }, noHandler: { _ in

                    }, type: .alert)
            } else {
                self.deleteContact(contacts)
            }

            UIView.animate(withDuration: 0.3) {
                self.selectContactVC?.frame = self.pointBottom
            } completion: { _ in
                self.selectContactVC?.removeFromSuperview()
                self.selectContactVC = nil
            }
        }
    }

    private func deleteContact(_ contacts: [Contact]) {
        DispatchQueue.global().async(group: DispatchGroup(), qos: .background, flags: .noQoS) {
            let group = DispatchGroup()
            for ct in contacts {
                group.enter()
                SwiftyContacts.deleteContact(Contact: ct.contact.mutableCopy() as! CNMutableContact) { result in
                    group.leave()
                    switch result {
                    case .success(let bool):
                        if bool {
                            print("isDelete ")
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }

    @objc func handPanGesture(_ gesture: UIPanGestureRecognizer) {
        let fileView = gesture.view!
        let translate = gesture.translation(in: self.view)
        print("ended ", translate)

        if translate.y != 0 {
            scrollToTop = translate.y < 0
        }

        if self.selectContactVC!.frame.minY + translate.y > self.pointTop.minY {

            switch gesture.state {
            case .began, .changed:
                print("changed")
                fileView.center = CGPoint(x: fileView.center.x, y: fileView.center.y + translate.y)
                gesture.setTranslation(.zero, in: self.view)
            case .ended:
                print("ended ", translate)
                UIView.animate(withDuration: 0.3) {
                    if self.scrollToTop {
                        self.selectContactVC?.frame = self.pointTop
                    } else {
                        self.selectContactVC?.frame = self.pointBottom
                    }
                } completion: { _ in
                    if self.scrollToTop == false {
                        self.selectContactVC?.removeFromSuperview()
                        self.selectContactVC = nil
                    }
                }

            default:
                break
            }
        }
    }

    fileprivate func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    fileprivate func getCountSelect() -> Int {
        var count = 0
        for section in self.data {
            count += section.countCTSelect
        }
        return count
    }

    @discardableResult func doSearch(_ searchString: String?) -> Int {
        self.isSearching = (searchString?.count ?? 0) > 0

        var numberOfResult: Int = 0
        if let searchString = searchString?.lowercased().unsign(), searchString.count > 0 {
            let resultContacts = self.contacts.filter { (model) -> Bool in
                model.doSearch(searchString)
            }
            self.resultContacts = self.prepareGroupData(resultContacts)
            numberOfResult += resultContacts.count
        }

        self.reloadData()
        return numberOfResult
    }

    func showAlerYesNo(title: String, _ tileYes: String, _ titleNo: String, _ message: String?, yesHandler handlerYes: ((UIAlertAction) -> Void)?, noHandler handlerNo: ((UIAlertAction) -> Void)?, type: UIAlertController.Style = .alert) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: type)
        let yesAction = UIAlertAction(title: tileYes, style: .default, handler: handlerYes)
        let closeAction = UIAlertAction(title: titleNo, style: .default, handler: handlerNo)
        alert.addAction(closeAction)
        alert.addAction(yesAction)
        if type == .actionSheet {
            alert.addAction(UIAlertAction(title: "kCanncel".localized(), style: .cancel, handler: nil))
            if #available(iOS 13.0, *) {
                alert.isModalInPresentation = true
            } else {
                // Fallback on earlier versions
            }
        }
        self.homeVC?.present(alert, animated: true, completion: nil)
    }

    deinit {
        print("SecretContactView deinit")
        self.homeVC = nil
    }
}

extension SecretContactView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.doSearch(searchText)
    }
}

extension SecretContactView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let group = self.data[section]
        return group.duplicates.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ContactHeaderView()
        headerView.lbTitleHeader.text = self.data[section].name
        headerView.backgroundColor = UIColor.colorFromHex("D1D1D1")
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ContactTableViewCell.self, for: indexPath)
        let contact = self.data[indexPath.section].duplicates[indexPath.row]
        cell.setupData(contact)
        cell.radioImage.image = (contact.isSelect ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2))
        cell.viewRadio.isHidden = !self.isChooseSelect
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.isChooseSelect == false {
            let contact = self.data[indexPath.section].duplicates[indexPath.item].contact
            let contactVC = CNContactViewController(for: contact)
            contactVC.allowsEditing = true
            contactVC.allowsActions = true
            self.homeVC?.navigationController?.setNavigationBarHidden(false, animated: false)
            self.homeVC?.navigationController?.navigationBar.tintColor = .black
            self.homeVC?.navigationController?.pushViewController(contactVC, animated: true)
        } else {
            let section = self.data[indexPath.section]
            let contact = section.duplicates[indexPath.row]
            contact.isSelect = !contact.isSelect
            self.countSelect = self.getCountSelect()
            self.reloadData()
        }
    }

    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.data.map { (group) -> String? in
            group.name
        } as? [String]
    }
}

extension SecretContactView {
    func requestContactSecret() {
        self.contactServices.requestContact { [weak self] contacts in
            guard let self = self else { return }
            var listContactsSecret: [Contact] = []
            if let contactsSecretData = UserDefaults.listContact {
                do {
                    listContactsSecret = try CNContactVCardSerialization.contacts(with: contactsSecretData).map({ Contact(contact: $0) })
                } catch {
                    print(error.localizedDescription)
                }
            }
            self.contacts = listContactsSecret
            self.requestContact(contacts: listContactsSecret)
        }
    }

    func requestContact(contacts: [Contact]) {
        self.showButton(count: contacts.count)
        self.listGroup.removeAll()
        self.listGroup.append(contentsOf: self.prepareGroupData(contacts))
        self.totalContact = contacts.count
        self.reloadData()
    }

    func prepareGroupData(_ contacts: [Contact]) -> [ContactGroup] {
        let sortedContacts = contacts.sorted(by: { ($0.contactIndexName ?? "") < ($1.contactIndexName ?? "") })
        let groupedContacts = sortedContacts.reduce([[Contact]]()) {
            guard var last = $0.last else { return [[$1]] }
            var collection = $0
            if last.first!.contactIndexName == $1.contactIndexName {
                last += [$1]
                collection[collection.count - 1] = last
            } else {
                collection += [[$1]]
            }
            return collection
        }

        var groups = [ContactGroup]()

        for groupedContact in groupedContacts {
            let group = ContactGroup(name: groupedContact.first?.contactIndexName, ctMain: groupedContact[0].contact, duplicates: groupedContact)
            groups.append(group)
        }

        return groups.sorted { (group1, group2) -> Bool in
            (group1.name ?? "") < (group2.name ?? "")
        }
    }

    func showButton(count: Int) {
        if count == 0 {
            self.countSelect = 0
            self.btnSharedContacts.isHidden = count == 0
            self.btnDeleteContacts.isHidden = count == 0
        }

        self.tableView.isHidden = count == 0
        self.rightBarButton.isHidden = count == 0
    }
}
