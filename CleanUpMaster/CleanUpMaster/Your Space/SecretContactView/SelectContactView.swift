//
//  SecuredView.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 11/04/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class SelectContactView: CustomViewXib {
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            self.searchBar.delegate = self
        }
    }

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            self.tableView.sectionHeaderHeight = 30
            self.tableView.rowHeight = 50
            self.tableView.estimatedRowHeight = 50
            self.tableView.register(cellType: ContactTableViewCell.self)
        }
    }

    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "btnSecretContact".localized()
        }
    }

    @IBOutlet weak var leftBarButton: UIButton! {
        didSet {
            leftBarButton.setTitle(titleDone, for: .disabled)
        }
    }

    @IBOutlet weak var rightBarButton: UIButton! {
        didSet {
            rightBarButton.setTitle(titleSelect, for: .normal)
        }
    }
    
    @IBOutlet weak var lbNoData: UILabel! {
        didSet {
            lbNoData.text = "kNoSecrectContactTitle".localized()
        }
    }

    private var countSelect: Int = 0 {
        didSet {
            let contact = "kContact".localized()
            let selected = "kSelected".localized()
            let ks = "kS".localized()

            leftBarButton.setTitle(titleDone, for: countSelect > 0 ? .normal : .disabled)
            self.navTitle.text = "\(countSelect > 0 ? "\(countSelect) " : "")\(contact)\((countSelect > 1) ? "\(ks)" : "") \(selected)"
            self.isSelectAll = (countSelect == totalContact && countSelect != 0)
        }
    }

    private var isSelectAll = false {
        didSet {
            self.rightBarButton.setTitle(isSelectAll ? titleDeselectAll : titleSelectAll, for: .normal)
        }
    }

    private var isChooseSelect = false {
        didSet {
            self.tableView.reloadData()
            if isChooseSelect {
                isSelectAll = false
            } else {
                countSelect = 0
                rightBarButton.setTitle(titleSelect, for: .normal)
            }
        }
    }

    private var isShowButton: Bool {
        return self.data.filter({ $0.isShowButton }).count > 0
    }

    var isSearching: Bool = false

    private let titleDone = "kDone".localized()
    private let titleSelect = "kSelect".localized()
    private let titleSelectAll = "kSelectAll".localized()
    private let titleDeselectAll = "kDeselectAll".localized()
    var totalContact = 0
    let contactServices = ContactsManagerment()

    private var listGroup = [ContactGroup]()
    private var resultContacts = [ContactGroup]()
    private var contacts: [Contact] = []

    private var data: [ContactGroup] {
        if self.isSearching {
            return self.resultContacts
        }
        return self.listGroup
    }
    weak var homeVC: DashboardViewController?
    var didClose: (([Contact]) -> Void)?

    override func setupUI() {
        self.requestContact()
    }

    private func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    private func getCountSelect() -> Int {
        var count = 0
        for section in self.data {
            count += section.countCTSelect
        }
        return count
    }

    @IBAction func actionBack(_ sender: Any) {
//        if self.isChooseSelect == false {
        var contacts: [Contact] = []

        for section in self.data {
            contacts += section.contactsSelect
        }
        self.didClose?(contacts)
//        } else {
//            self.isChooseSelect = false
//        }
    }

    @IBAction func actionRight(_ sender: Any) {
        if self.isChooseSelect == false {
            self.isChooseSelect = true
        } else {
            for section in self.data {
                for item in section.duplicates {
                    item.isSelect = !self.isSelectAll
                }
            }
            self.countSelect = self.getCountSelect()
            self.reloadData()
        }
    }

    @discardableResult func doSearch(_ searchString: String?) -> Int {
        self.isSearching = (searchString?.count ?? 0) > 0

        var numberOfResult: Int = 0
        if let searchString = searchString?.lowercased().unsign(), searchString.count > 0 {
            let resultContacts = self.contacts.filter { (model) -> Bool in
                model.doSearch(searchString)
            }
            self.resultContacts = self.prepareGroupData(resultContacts)
            numberOfResult += resultContacts.count
        }

        self.reloadData()
        return numberOfResult
    }

    deinit {
        print("SelectContactView deinit")
        self.homeVC = nil
    }
}

extension SelectContactView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.doSearch(searchText)
    }
}

extension SelectContactView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let group = self.data[section]
        return group.duplicates.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ContactHeaderView()
        headerView.lbTitleHeader.text = self.data[section].name
        headerView.backgroundColor = UIColor.colorFromHex("D1D1D1")
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: ContactTableViewCell.self, for: indexPath)
        let contact = self.data[indexPath.section].duplicates[indexPath.row]
        cell.setupData(contact)
        cell.radioImage.image = (contact.isSelect ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2))
        cell.viewRadio.isHidden = !self.isChooseSelect
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.isChooseSelect == false {
            let contact = self.data[indexPath.section].duplicates[indexPath.item].contact
            let contactVC = CNContactViewController(for: contact)
            contactVC.allowsEditing = true
            contactVC.allowsActions = true
            self.homeVC?.navigationController?.setNavigationBarHidden(false, animated: false)
            self.homeVC?.navigationController?.navigationBar.tintColor = .black
            self.homeVC?.navigationController?.pushViewController(contactVC, animated: true)
        } else {
            let section = self.data[indexPath.section]
            let contact = section.duplicates[indexPath.row]
            contact.isSelect = !contact.isSelect
            self.countSelect = self.getCountSelect()
            self.tableView.reloadData()
        }
    }

    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.data.map { (group) -> String? in
            group.name
        } as? [String]
    }
}

extension SelectContactView {
    func requestContact() {
        self.contactServices.requestContact { [weak self] contacts in
            guard let self = self else { return }
            self.showButton(count: contacts.count)
            self.contacts = contacts
            self.listGroup.removeAll()
            self.listGroup.append(contentsOf: self.prepareGroupData(contacts))
            self.totalContact = contacts.count
            self.reloadData()
        }
    }

    func prepareGroupData(_ contacts: [Contact]) -> [ContactGroup] {
        let sortedContacts = contacts.sorted(by: { ($0.contactIndexName ?? "") < ($1.contactIndexName ?? "") })
        let groupedContacts = sortedContacts.reduce([[Contact]]()) {
            guard var last = $0.last else { return [[$1]] }
            var collection = $0
            if last.first!.contactIndexName == $1.contactIndexName {
                last += [$1]
                collection[collection.count - 1] = last
            } else {
                collection += [[$1]]
            }
            return collection
        }

        var groups = [ContactGroup]()

        for groupedContact in groupedContacts {
            let group = ContactGroup(name: groupedContact.first?.contactIndexName, ctMain: groupedContact[0].contact, duplicates: groupedContact)
            groups.append(group)
        }

        return groups.sorted { (group1, group2) -> Bool in
            (group1.name ?? "") < (group2.name ?? "")
        }
    }

    func showButton(count: Int) {
        self.tableView.isHidden = count == 0
        self.rightBarButton.isHidden = count == 0
    }
}

