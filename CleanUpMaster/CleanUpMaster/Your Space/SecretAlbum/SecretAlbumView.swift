//
//  SecuredView.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 11/04/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Photos
import Kingfisher

class SecretAlbumView: CustomViewXib, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "btnSecretAlbum".localized()
        }
    }

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(cellType: PhotoCollectionViewCell.self)
        }
    }

    @IBOutlet weak var leftBarButton: UIButton! {
        didSet {
            leftBarButton.setTitle(titleDone, for: .normal)
        }
    }

    @IBOutlet weak var btnRightNav: UIButton! {
        didSet {
            btnRightNav.setTitle(titleSelect, for: .normal)
        }
    }

    @IBOutlet weak var btnSharedContacts: UIButton! { didSet { btnSharedContacts.isHidden = true } }
    @IBOutlet weak var btnDeleteContacts: UIButton! { didSet { btnDeleteContacts.isHidden = true } }

    private var isShowBottomButton = false {
        didSet {
            self.btnSharedContacts.isHidden = !isShowBottomButton
            self.btnDeleteContacts.isHidden = !isShowBottomButton
        }
    }

    @IBOutlet weak var lbNoData: UILabel! {
        didSet {
            lbNoData.text = "kNoSecrectAlbumTitle".localized()
        }
    }
    
    @IBOutlet weak var lbNoDataContent: UILabel! {
        didSet {
            lbNoDataContent.text = "kNoSecrectAlbumContent".localized()
        }
    }
    
    private let titleDone = "kClose".localized()
    private let titleSelect = "kSelect".localized()
    private let titleSelectAll = "kSelectAll".localized()
    private let titleDeselectAll = "kDeselectAll".localized()
    private var footerTitle = "%@ \("kPhoto".localized())%@, %@ \("kVideo".localized())%@"
    weak var homeVC: DashboardViewController?
    private var selectAlbumVC: SelectAlbumView?

    private var pointTop: CGRect = .zero
    private var pointBottom: CGRect = .zero
    private var scrollToTop: Bool = true

    fileprivate var checkSelected: [Bool] = []
    private var isSelectAll = true {
        didSet {
            self.btnRightNav.setTitle(isSelectAll ? titleDeselectAll : titleSelectAll, for: .normal)
        }
    }

    private var countSelect: Int = 0 {
        didSet {
            let photo = "kPhoto".localized()
            let selected = "kSelected".localized()
            let ks = "kS".localized()
            
            self.navTitle.text = "\(countSelect > 0 ? "\(countSelect) " : "")\(photo)\((countSelect > 1) ? "\(ks)" : "") \(selected)"
            self.isSelectAll = (countSelect == photos.count && countSelect != 0)
            self.isShowBottomButton = countSelect > 0
        }
    }
    let group = DispatchGroup()
    var photos: [URL] = []

    var didClose: (() -> Void)?

    override func setupUI() {
        self.setupCollectionView()
        self.fetchAssets()
        PHPhotoLibrary.shared().register(self)
    }

    @IBAction func actionSharedImages(_ sender: UIButton) {
        var paths: [URL] = []

        for i in 0 ..< self.checkSelected.count {
            let item = self.photos[i]
            if self.checkSelected[i] == true {
                paths.append(item)
            }
        }

        self.homeVC?.sharedAppScan("\("kShare".localized()) \(paths.count) \("kImage".localized())\("kSs".localized()).", file: paths)
    }

    private func deleteImages(_ paths: [String]) {
        Loading.shared.startLoading()
        FileProvider.shared.deleteFile(paths) { [weak self] in
            Loading.shared.stopLoading()
            self?.fetchAssets()
        }
    }

    // Called when image save is complete (with error or not)
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        self.group.leave()
        if let error = error {
            print("ERROR: \(error)")
        }
        else {
            print("Save: ")
        }
    }

    @IBAction func actionDeleteImages(_ sender: UIButton) {
        var paths: [String] = []

        for i in 0 ..< self.checkSelected.count {
            let item = self.photos[i]
            if self.checkSelected[i] == true {
                paths.append(item.path)
            }
        }

        self.homeVC?.showAlerYesNo(title: "kChooseAaction".localized(), "kDeleteSelected".localized(), "kSave".localized(), "", yesHandler: { _ in
            self.deleteImages(paths)
        }, noHandler: { _ in
                for path in paths {
                    self.group.enter()
                    if let image = UIImage(contentsOfFile: path) {
                        UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_: didFinishSavingWithError: contextInfo:)), nil)
                    } else {
                        self.group.leave()
                    }
                }

                self.group.notify(queue: .main) {
                    FileProvider.shared.deleteFile(paths) { [weak self] in
                        self?.fetchAssets()
                    }
                }
            }, type: .actionSheet)
    }

    @IBAction func actionClose(_ sender: UIButton) {
        self.didClose?()
    }

    private func actionOpenAlbum() {
        self.selectAlbumVC = SelectAlbumView()
        guard let selectAlbumVC = self.selectAlbumVC else { return }
        self.addSubview(selectAlbumVC)
        selectAlbumVC.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handPanGesture(_:))))
        self.pointTop = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.pointBottom = CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: self.frame.height)
        selectAlbumVC.frame = self.pointBottom
        selectAlbumVC.homeVC = self.homeVC

        UIView.animate(withDuration: 0.3) {
            selectAlbumVC.frame = self.pointTop
        }

        selectAlbumVC.didClose = { [unowned self] images in
            UIView.animate(withDuration: 0.3) {
                self.selectAlbumVC?.frame = self.pointBottom
            } completion: { _ in
                self.selectAlbumVC?.removeFromSuperview()
                self.selectAlbumVC = nil
                if images.count > 0 {
                    
                    Loading.shared.startLoading()
                    let opera = OperationQueue.main
                    opera.name = "delete"
                    opera.maxConcurrentOperationCount = 1
                    opera.addOperation {
                        self.saveImages(assets: images)
                        for _ in 0 ..< images.count {
                            self.checkSelected.append(false)
                        }
                        self.reloadData()
                    }
                    opera.addOperation {
                        let assetIdentifiers = images.map({ $0.localIdentifier })
                        let assets = PHAsset.fetchAssets(withLocalIdentifiers: assetIdentifiers, options: nil)
                        if UserDefaults.isEnablePhotoVideo == true { // remove after import
                            PHPhotoLibrary.shared().performChanges({
                                PHAssetChangeRequest.deleteAssets(assets)
                            })
                        } else {
                            try? PHPhotoLibrary.shared().performChangesAndWait {
                                PHAssetChangeRequest.deleteAssets(assets)
                            }
                        }
                    }
                }
            }
        }
    }

    private func actionOpenCamera() {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        if status == .notDetermined {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] (granted: Bool) -> Void in
                DispatchQueue.main.async {
                    if granted == true {
                        self?.openCapture()
                    }
                }
            })
        } else if status == .authorized {
            self.openCapture()
        } else {

        }
    }

    private func openCapture() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = false
        vc.delegate = self
        self.homeVC?.present(vc, animated: true)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)

        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            print("No image found")
            return
        }

        if let url = FileProvider.documentsURL {
            let group = DispatchGroup()

            let urlImg = url.appendingPathComponent("newImage\(Date().toString(format: "mm:ss", timezone: .current)).png")
            group.enter()
            do {
                try image.toJPEGData?.write(to: urlImg)
                group.leave()
            } catch {
                group.leave()
                print("Catch error ", error.localizedDescription)
            }

            group.notify(queue: .main) {
                self.photos.append(urlImg)
                self.checkSelected.append(false)
                self.reloadData()
                self.showButton(count: self.photos.count)
            }
        }
    }

    @IBAction func actionAddContact(_ sender: UIButton) {
        self.homeVC?.showAlerYesNo(title: "kChooseAaction".localized(), "kOpenAlbum".localized(), "kOpenCamera".localized(), "", yesHandler: { _ in
            self.actionOpenAlbum()
        }, noHandler: { _ in
                self.actionOpenCamera()
            }, type: .actionSheet)
    }

    private func saveImages(assets: [PHAsset]) {
        FileProvider.shared.assetToImage(assets) { [weak self] (images) in
            guard let self = self else { return }

            if let url = FileProvider.documentsURL {
                let queue = OperationQueue()
                queue.name = "savevideo"
                queue.maxConcurrentOperationCount = 1

                for image in images {
                    queue.addOperation {
                        let urlImg = url.appendingPathComponent("\(Date().toString())-\(image.name ?? "")")
                        do {
                            try image.data?.write(to: urlImg)
                            self.photos.append(urlImg)
                        } catch {
                            print("Catch error ", error.localizedDescription)
                        }
                        print("saveImages -- - - -- - - - -> \n ", image.name ?? "")
                    }
                }

                queue.addOperation {
                    DispatchQueue.main.async {
                        Loading.shared.stopLoading()
                        print("all done")
                        self.showButton(count: self.photos.count)
                        self.reloadData()
                    }
                }
                queue.waitUntilAllOperationsAreFinished()
            }
        }
    }

    @IBAction func actionSelected(_ sender: Any) {
        self.isSelectAll = !self.isSelectAll
        self.checkSelected.removeAll()
        for _ in 0 ..< photos.count {
            self.checkSelected.append(isSelectAll)
        }
        self.countSelect = isSelectAll ? photos.count : 0

        self.reloadData()
    }

    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self

        if let flowLayout = self.collectionView.collectionViewLayout as? GridCollectionViewLayout {
            flowLayout.scrollDirection = .vertical
            flowLayout.sectionInset = UIEdgeInsets.init(top: -1, left: 1, bottom: 0, right: 1)
            flowLayout.interitemSpacing = 1
            flowLayout.lineSpacing = 1
            flowLayout.aspectRatio = 1
            flowLayout.footerReferenceLength = 30
            flowLayout.numberOfItemsPerLine = 4
        }
        collectionView.register(cellType: PhotoCollectionViewCell.self)
        collectionView.register(viewType: FooterView.self, forElementKind: UICollectionView.elementKindSectionFooter)

    }

    @objc func handPanGesture(_ gesture: UIPanGestureRecognizer) {
        let fileView = gesture.view!
        let translate = gesture.translation(in: self.view)
        print("ended ", translate)

        if translate.y != 0 {
            scrollToTop = translate.y < 0
        }

        if self.selectAlbumVC!.frame.minY + translate.y > self.pointTop.minY {

            switch gesture.state {
            case .began, .changed:
                print("changed")
                fileView.center = CGPoint(x: fileView.center.x, y: fileView.center.y + translate.y)
                gesture.setTranslation(.zero, in: self.view)
            case .ended:
                print("ended ", translate)
                UIView.animate(withDuration: 0.3) {
                    if self.scrollToTop {
                        self.selectAlbumVC?.frame = self.pointTop
                    } else {
                        self.selectAlbumVC?.frame = self.pointBottom
                    }
                } completion: { _ in
                    if self.scrollToTop == false {
                        self.selectAlbumVC?.removeFromSuperview()
                        self.selectAlbumVC = nil
                    }
                }

            default:
                break
            }
        }
    }

    fileprivate func reloadData() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

    private func fetchAssets() { // 1
        self.photos.removeAll()
        self.checkSelected.removeAll()
        FileProvider.shared.getAllDocuments { [weak self] urls in
            guard let self = self else { return }
            self.photos = urls
            
            self.showButton(count: self.photos.count)
            self.countSelect = 0
            for _ in 0 ..< self.photos.count {
                self.checkSelected.append(false)
            }
            self.reloadData()
        }
    }

    deinit {
        self.homeVC = nil
        print("homeVC", self.homeVC ?? "homeVC")
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    func showButton(count: Int) {
        if count == 0 {
            self.countSelect = 0
            self.btnSharedContacts.isHidden = count == 0
            self.btnDeleteContacts.isHidden = count == 0
        }

        self.collectionView.isHidden = count == 0
        self.btnRightNav.isHidden = count == 0
    }

    func createVideoThumbnail(from url: URL) -> UIImage? {

        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.maximumSize = self.frame.size

        let time = CMTimeMakeWithSeconds(0.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
}

extension SecretAlbumView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footerView = collectionView.dequeueReusableCell(with: FooterView.self, elementKind: UICollectionView.elementKindSectionFooter, for: indexPath)
        let totalImage = photos.filter { url in
            url.absoluteString.isImageType()
        }.count

        let totalVideo = photos.count - totalImage

        let footerTitle = String(format: self.footerTitle, totalImage.toString, totalImage > 0 ? "\("kS".localized())" : "", totalVideo.toString, totalVideo > 0 ? "\("kS".localized())" : "")
        
        footerView.lbTtitle.text = footerTitle
        return footerView
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PhotoCollectionViewCell.self, for: indexPath)
        let photo = self.photos[indexPath.item]
        cell.photoView.kf.indicatorType = .activity
        cell.btnRadio.isHidden = true
        cell.photoView.kf.setImage(with: photo, placeholder: self.createVideoThumbnail(from: photo)) { _ in
            cell.photoView.kf.indicatorType = .none
            cell.btnRadio.isHidden = false
        }
        cell.showImagePlay(isShow: !photo.absoluteString.isImageType())
        let check = self.checkSelected[indexPath.item]
        cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        cell.blurView.isHidden = !check

        cell.selectedPhoto = { [weak self] in
            guard let self = self else { return }

            self.checkSelected[indexPath.item] = !self.checkSelected[indexPath.item]
            let check = self.checkSelected[indexPath.item]
            cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
            self.countSelect = self.checkSelected.filter { $0 == true }.count
            self.isSelectAll = !self.checkSelected.contains(false)
        }
        return cell
    }
}

extension SecretAlbumView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let vc = PreviewPhotoViewController()
        vc.indexSelected = indexPath.item
        vc.photosURL = self.photos
        vc.checkSelected = self.checkSelected
        vc.delegate = self
        self.homeVC?.pushViewWithScreen(vc)
    }
}


extension SecretAlbumView: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        print("photoLibraryDidChange")
        self.reloadData()
    }
}

extension SecretAlbumView: PreviewPhotosDelegate {
    func didSelectedImage(index: Int) {
        self.checkSelected[index] = !self.checkSelected[index]
        self.countSelect = self.checkSelected.filter { $0 == true }.count
        self.isSelectAll = !self.checkSelected.contains(false)
        self.reloadData()
    }
}



extension Date {
    func toString(format: String = "MMM d", timezone: TimeZone = .current) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = timezone
        dateFormatter.locale = .current
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
