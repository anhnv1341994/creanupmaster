//
//  SecuredView.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 11/04/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Photos

class SelectAlbumView: CustomViewXib {
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(cellType: PhotoCollectionViewCell.self)
        }
    }

    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "btnSecretAlbum".localized()
        }
    }
    @IBOutlet weak var btnRightNav: UIButton!
    fileprivate var checkSelected: [Bool] = []
    private let titleSelectAll = "kSelectAll".localized()
    private let titleDeselectAll = "kDeselectAll".localized()

    private var isSelectAll = true {
        didSet {
            self.btnRightNav.setTitle(isSelectAll ? titleDeselectAll : titleSelectAll, for: .normal)
        }
    }

    private var countSelect: Int = 0 {
        didSet {
            let photo = "kPhoto".localized()
            let selected = "kSelected".localized()
            let ks = "kS".localized()
            
            self.navTitle.text = "\(countSelect > 0 ? "\(countSelect) " : "")\(photo)\((countSelect > 1) ? "\(ks)" : "") \(selected)"
            self.isSelectAll = (countSelect == photos.count && countSelect != 0)
        }
    }

    var photos: [PHAsset] = []
    var type: PhotosType = .all

    @IBOutlet weak var leftBarButton: UIButton! {
        didSet {
            leftBarButton.setTitle("kDone".localized(), for: .normal)
        }
    }
    
    @IBOutlet weak var lbNoData: UILabel! {
        didSet {
            lbNoData.text = "kNoFiles".localized()
        }
    }

    weak var homeVC: DashboardViewController?
    var didClose: (([PHAsset]) -> Void)?

    override func setupUI() {
        self.fetchAssets()
        self.setupCollectionView()
        PHPhotoLibrary.shared().register(self)
    }

    @IBAction func actionSelected(_ sender: Any) {
        self.isSelectAll = !self.isSelectAll
        self.checkSelected.removeAll()
        for _ in 0 ..< photos.count {
            self.checkSelected.append(isSelectAll)
        }
        self.countSelect = isSelectAll ? photos.count : 0

        self.reloadData()
    }

    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self

        if let flowLayout = self.collectionView.collectionViewLayout as? GridCollectionViewLayout {
            flowLayout.scrollDirection = .vertical
            flowLayout.sectionInset = UIEdgeInsets.init(top: -1, left: 1, bottom: 0, right: 1)
            flowLayout.interitemSpacing = 1
            flowLayout.lineSpacing = 1
            flowLayout.aspectRatio = 1
            flowLayout.headerReferenceLength = 1.0
            flowLayout.numberOfItemsPerLine = 4
        }
        collectionView.register(cellType: PhotoCollectionViewCell.self)
    }


    fileprivate func reloadData() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

    deinit {
        self.homeVC = nil
        print("homeVC", self.homeVC ?? "homeVC")
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    private func fetchAssets() { // 1
        self.countSelect = 0

        self.photos.removeAll()
        self.checkSelected.removeAll()

        let allPhotosOptions = PHFetchOptions()
        allPhotosOptions.sortDescriptors = [
            NSSortDescriptor(
                key: "creationDate",
                ascending: false)
        ]

        let group = DispatchGroup()
        PHAsset.fetchAssets(with: allPhotosOptions).enumerateObjects { object, _, _ in
            group.enter()
            self.checkSelected.append(false)
            self.photos.append(object)
            group.leave()
        }

        group.notify(queue: .main) {
            self.showButton(count: self.photos.count)
            self.reloadData()
        }
    }


    @IBAction func actionBack(_ sender: Any) {
        var imagesPhasset: [PHAsset] = []

        for i in 0 ..< self.checkSelected.count {
            let item = self.photos[i]
            if self.checkSelected[i] == true {
                imagesPhasset.append(item)
            }
        }

        self.didClose?(imagesPhasset)
    }

    func showButton(count: Int) {
        self.collectionView.isHidden = count == 0
        self.btnRightNav.isHidden = count == 0
    }
}

extension SelectAlbumView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PhotoCollectionViewCell.self, for: indexPath)
        let photo = self.photos[indexPath.item]
        cell.photoView.fetchImageAsset(photo, targetSize: cell.bounds.size) { _ in
//          cell.blurView.isHidden = !success
        }
        let check = self.checkSelected[indexPath.item]
        cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        cell.blurView.isHidden = !check

        cell.selectedPhoto = { [weak self] in
            guard let self = self else { return }

            self.checkSelected[indexPath.item] = !self.checkSelected[indexPath.item]
            let check = self.checkSelected[indexPath.item]
            cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
            self.countSelect = self.checkSelected.filter { $0 == true }.count
            self.isSelectAll = !self.checkSelected.contains(false)
        }
        return cell
    }
}

extension SelectAlbumView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoCollectionViewCell
        self.checkSelected[indexPath.item] = !self.checkSelected[indexPath.item]
        let check = self.checkSelected[indexPath.item]
        cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        self.countSelect = self.checkSelected.filter { $0 == true }.count
        self.isSelectAll = !self.checkSelected.contains(false)
    }
}

extension SelectAlbumView: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
//        DispatchQueue.main.sync {
//            if let changeDetails = changeInstance.changeDetails(for: photos) {
//                photos = changeDetails.fetchResultAfterChanges
//            }
//            self.collectionView.reloadData()
//        }
    }
}

extension SelectAlbumView: PreviewPhotosDelegate {
    func didSelectedImage(index: Int) {
        self.checkSelected[index] = !self.checkSelected[index]
        self.countSelect = self.checkSelected.filter { $0 == true }.count
        self.isSelectAll = !self.checkSelected.contains(false)
        self.reloadData()
    }
}
