//
//  SecuredView.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 11/04/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Photos

class PreviewAlbumView: CustomViewXib, PreviewPhotosDelegate {
    func showButton(count: Int) {
        
    }
    
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var btnRightNav: UIButton!
    @IBOutlet weak var contentViewXib: UIView!
    var checkSelected: [Bool] = []
    var indexSelect: Int = 0
    var photos: [URL] = []

    @IBOutlet weak var leftBarButton: UIButton!
    @IBOutlet weak var rightBarButton: UIButton!

    weak var homeVC: DashboardViewController?
    var didClose: (() -> Void)?

    override func setupUI() {
        let vc = PreviewPhotoViewController()
        self.contentViewXib.addSubview(vc.view)
        vc.view.frame = self.contentViewXib.bounds
        vc.indexSelected = indexSelect
        vc.photosURL = self.photos
        vc.checkSelected = self.checkSelected
        vc.delegate = self
    }

    deinit {
        self.homeVC = nil
    }

    func didSelectedImage(index: Int) {
        
    }
    

    @IBAction func actionBack(_ sender: Any) {
        self.didClose?()
    }
}
