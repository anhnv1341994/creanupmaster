//
//  SecuredView.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 11/04/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

enum SecretType {
    case album, contact
}

class SecuredView: CustomViewXib {
    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "kYourSpace".localized()
        }
    }
    @IBOutlet weak var btnSecretAlbum: UIButton! {
        didSet {
            btnSecretAlbum.setTitle("btnSecretAlbum".localized(), for: .normal)
        }
    }
    @IBOutlet weak var lbSecrectAlbum: UILabel! {
        didSet {
            lbSecrectAlbum.text = "lbSecrectAlbum".localized()
        }
    }
    @IBOutlet weak var btnSecretContact: UIButton! {
        didSet {
            btnSecretContact.setTitle("btnSecretContact".localized(), for: .normal)
        }
    }
    @IBOutlet weak var lbSecrectContact: UILabel! {
        didSet {
            lbSecrectContact.text = "lbSecrectContact".localized()
        }
    }

    @IBOutlet weak var stackHeader: UIStackView!
    private var secretContactVC: SecretContactView?
    private var secretAlbumView: SecretAlbumView?
    private var pointTop: CGRect = .zero
    private var pointBottom: CGRect = .zero
    private var scrollToTop: Bool = true
    weak var homeVC: DashboardViewController?

    override func setupUI() {

    }

    @IBAction func actionShowView(_ sender: UIButton) {
        self.homeVC?.actionShowViewSecured()
    }

    @IBAction func actionSecretAlbum(_ sender: UIButton) {
        self.secretAlbumView = SecretAlbumView()
        guard let securedView = self.secretAlbumView else { return }
        self.addSubview(securedView)
        securedView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handPanGesture(_:))))
        self.pointTop = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.pointBottom = CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: self.frame.height)
        securedView.frame = self.pointBottom
        securedView.homeVC = self.homeVC

        UIView.animate(withDuration: 0.3) {
            securedView.frame = self.pointTop
        }

        securedView.didClose = { [unowned self] in
            UIView.animate(withDuration: 0.3) {
                self.secretAlbumView?.frame = self.pointBottom
            } completion: { _ in
                self.secretAlbumView?.removeFromSuperview()
                self.secretAlbumView = nil
            }
        }
    }

    @IBAction func actionSecretContacts(_ sender: UIButton) {
        self.secretContactVC = SecretContactView()
        guard let securedView = self.secretContactVC else { return }
        self.addSubview(securedView)
        securedView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handPanGesture(_:))))
        self.pointTop = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.pointBottom = CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: self.frame.height)
        securedView.frame = self.pointBottom
        securedView.homeVC = self.homeVC

        UIView.animate(withDuration: 0.3) {
            securedView.frame = self.pointTop
        }

        securedView.didCloseView = { [unowned self] in
            UIView.animate(withDuration: 0.3) {
                self.secretContactVC?.frame = self.pointBottom
            } completion: { _ in
                self.secretContactVC?.removeFromSuperview()
                self.secretContactVC = nil
            }
        }
    }

    @objc func handPanGesture(_ gesture: UIPanGestureRecognizer) {
        let fileView = gesture.view!
        let translate = gesture.translation(in: self.view)
        print("ended ", translate)

        if translate.y != 0 {
            scrollToTop = translate.y < 0
        }
        var minY: CGFloat = 0
        if self.secretContactVC != nil {
            minY = self.secretContactVC?.frame.minY ?? 0
        } else {
            minY = self.secretAlbumView?.frame.minY ?? 0
        }

        if minY + translate.y > self.pointTop.minY {

            switch gesture.state {
            case .began, .changed:
                print("changed")
                fileView.center = CGPoint(x: fileView.center.x, y: fileView.center.y + translate.y)
                gesture.setTranslation(.zero, in: self.view)
            case .ended:
                print("ended ", translate)
                UIView.animate(withDuration: 0.3) {
                    if self.scrollToTop {
                        if self.secretContactVC != nil {
                            self.secretContactVC?.frame = self.pointTop
                        } else {
                            self.secretAlbumView?.frame = self.pointTop
                        }
                    } else {
                        if self.secretContactVC != nil {
                            self.secretContactVC?.frame = self.pointBottom
                        } else {
                            self.secretAlbumView?.frame = self.pointBottom
                        }
                    }
                } completion: { _ in
                    if self.scrollToTop == false {
                        if self.secretContactVC != nil {
                            self.secretContactVC?.removeFromSuperview()
                            self.secretContactVC = nil
                        } else {
                            self.secretAlbumView?.removeFromSuperview()
                            self.secretAlbumView = nil
                        }
                    }
                }

            default:
                break
            }
        }
    }

    deinit {

    }
}
