////
////  PurchaseApp.swift
////  CleanUpMaster
////
////  Created by Nguyen Van Anh on 01/07/2021.
////  Copyright © 2021 Nguyen Anh. All rights reserved.
////
//
//import Foundation
//import StoreKit
//import SwiftyStoreKit
//// https://developer.apple.com/documentation/storekit/in-app_purchase/validating_receipts_with_the_app_store
//enum IAPProduct: String {
//    case threeDay = "scanner.app.threeDayFree"
//    case week = "scanner.app.oneWeek"
//    case appMonth = "app.oneMonth"
//    case appYear = "scanner.app.oneYear"
//    case free3dayMonth = "app.oneMonth.trial.three.day"
//    case free3dayYear = "app.oneYear.trial.three.day"
//}
//
//class IPAService: NSObject {
//    static var shared = IPAService()
//
//    var products: Set<SKProduct> = []
//    var paymentQueue = SKPaymentQueue.default()
//
//    let sharedSecret = "2b7ad6c14e244b3b8d952b66282361dc"
//    let verifyType: AppleReceiptValidator.VerifyReceiptURLType = .production
//
//    override private init() {}
//
//    func checkTransactionState() {
//        // see notes below for the meaning of Atomic / Non-Atomic
//        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
//            for purchase in purchases {
//                switch purchase.transaction.transactionState {
//                case .purchased, .restored:
//                    if purchase.needsFinishTransaction {
//                        // Deliver content from server, then:
//                        SwiftyStoreKit.finishTransaction(purchase.transaction)
//                    }
//                    // Unlock content
//                case .failed, .purchasing, .deferred:
//                    break // do nothing
//                @unknown default:
//                    break
//                }
//            }
//        }
//    }
//
//    func retrieveProductsInfo(_ complete: @escaping (_ products: Set<SKProduct>?) -> Void) {
//        ScannerLoading.shared.startLoading()
//        SwiftyStoreKit.retrieveProductsInfo([IAPProduct.appMonth.rawValue, IAPProduct.appYear.rawValue]) { result in
//            ScannerLoading.shared.stopLoading()
//            if let product = result.retrievedProducts.first {
//                let priceString = product.localizedPrice
//                print("Product: \(product.localizedDescription), price: \(priceString)")
//                self.products = result.retrievedProducts
//                complete(result.retrievedProducts)
//            }
//            else if let invalidProductId = result.invalidProductIDs.first {
//                print("Invalid product identifier: \(invalidProductId)")
//                complete(nil)
//            }
//            else {
//                print("Error: \(String(describing: result.error))")
//                complete(nil)
//            }
//        }
//    }
//
//    func purchaseProduct(product: IAPProduct, _ complete: @escaping (_ isPurchase: Bool) -> Void) {
//        SwiftyStoreKit.purchaseProduct(product.rawValue, atomically: true) { [weak self] (result) in
//            switch result {
//            case .success(let purchase):
//                if purchase.needsFinishTransaction {
//                    SwiftyStoreKit.finishTransaction(purchase.transaction)
//                }
//                self?.verify(productId: product.rawValue, complete)
//
//            case .error(let error):
//                self?.setPurchasedProductId(nil)
//                complete(false)
//
//                switch error.code {
//                case .unknown: print("Unknown error. Please contact support")
//                case .clientInvalid: print("Not allowed to make the payment")
//                case .paymentCancelled: break
//                case .paymentInvalid: print("The purchase identifier was invalid")
//                case .paymentNotAllowed: print("The device is not allowed to make the payment")
//                case .storeProductNotAvailable: print("The product is not available in the current storefront")
//                case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
//                case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
//                case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
//                default: print((error as NSError).localizedDescription)
//                }
//            }
//        }
//    }
//
//    func restorePurchases (_ complete: @escaping (_ isPurchase: Bool) -> Void) {
//        SwiftyStoreKit.restorePurchases(atomically: true) { [weak self] (results) in
//            if results.restoreFailedPurchases.count > 0 {
//                print("Restore Failed: \(results.restoreFailedPurchases)")
//                complete(false)
//            }
//            else if results.restoredPurchases.count > 0 {
//                if let productId = results.restoredPurchases.first?.productId {
//                    self?.verify(productId: productId, complete)
//                }
//                else {
//                    print("Restore Fail")
//                    complete(false)
//                }
//            }
//            else {
//                print("Nothing to Restore")
//                complete(false)
//            }
//        }
//    }
//
//    func verify(productId: String, _ complete: @escaping (_ isPurchase: Bool) -> Void) {
//        let appleValidator = AppleReceiptValidator(service: self.verifyType, sharedSecret: self.sharedSecret)
//        SwiftyStoreKit.verifyReceipt(using: appleValidator) { [weak self] (result) in
//            if case .success(let receipt) = result {
//                let purchaseResult = SwiftyStoreKit.verifySubscription(ofType: .autoRenewable,
//                                                                       productId: productId,
//                                                                       inReceipt: receipt)
//                switch purchaseResult {
//                case .purchased(let expiryDate, let receiptItems):
//                    if let productId = receiptItems.first?.productId {
//                        self?.setPurchasedProductId(productId)
//                        print("Product \(productId) is valid until \(expiryDate)")
//                        complete(true)
//                        UserDefault.setIsPurchase(true)
//                    }
//                    else {
//                        self?.setPurchasedProductId(nil)
//                        print("receipt verification error")
//                        complete(false)
//                        UserDefault.setIsPurchase(false)
//                    }
//                case .expired(let expiryDate, _):
//                    self?.setPurchasedProductId(nil)
//                    print("Product is expired since \(expiryDate)")
//                    complete(false)
//                    UserDefault.setIsPurchase(false)
//                case .notPurchased:
//                    self?.setPurchasedProductId(nil)
//                    print("This product has never been purchased")
//                    complete(false)
//                    UserDefault.setIsPurchase(false)
//                }
//            }
//            else {
//                self?.setPurchasedProductId(nil)
//                print("receipt verification error")
//                complete(false)
//                UserDefault.setIsPurchase(false)
//            }
//        }
//    }
//
//    func handlePurchasesStarted() {
////        itms-services://?action=purchaseIntent&bundleId=com.example.app&productIdentifier=product_name
//        SwiftyStoreKit.shouldAddStorePaymentHandler = { payment, product in
//            // return true if the content can be delivered by your app
//            // return false otherwise
//            return false
//        }
//    }
//
//    func verifyPurchase(_ complete: ((_ isPurchase: Bool) -> Void)? = nil) {
//        guard let productId = self.getPurchasedProductId() else {
//            complete?(false)
//            return
//        }
//
//        self.verify(productId: productId) { (isPurchase) in
//            complete?(isPurchase)
//        }
//    }
//
//    // MARK: Save
//    static let kPurchasedProductIdKey = "PURCHASED_PRODUCT_ID"
//    func setPurchasedProductId(_ productId: String?) {
//        if let productId = productId {
//            UserDefaults.standard.setValue(productId, forKey: IPAService.kPurchasedProductIdKey)
//        }
//        else {
//            UserDefaults.standard.removeObject(forKey: IPAService.kPurchasedProductIdKey)
//        }
//        UserDefaults.standard.synchronize()
//    }
//    func getPurchasedProductId() -> String? {
//        return UserDefaults.standard.value(forKey: IPAService.kPurchasedProductIdKey) as? String
//    }
//
//}
//
//extension SKProduct {
//
//    fileprivate static var formatter: NumberFormatter {
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .currency
//        return formatter
//    }
//
//    var localizedPrice: String {
//        if self.price == 0.00 {
//            return "Get"
//        } else {
//            let formatter = SKProduct.formatter
//            formatter.locale = self.priceLocale
//
//            guard let formattedPrice = formatter.string(from: self.price) else {
//                return "Unknown Price"
//            }
//
//            return formattedPrice
//        }
//    }
//
//}
