//
//  CustomViewXib.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Contacts
import UIKit

class CustomViewXib: UIView {
    var view: UIView?
    var lastBounds: CGSize = CGSize.zero
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeView()
    }
    
    func initializeView() -> Void {
        let bundle: Bundle = Bundle(for: self.classForCoder)
        let className: String = String(describing: self.classForCoder)
        
        self.backgroundColor = UIColor.clear
        self.isOpaque = true
        
        if let customView: UIView = bundle.loadNibNamed(className, owner: self, options: [:])?.first as? UIView {
            customView.translatesAutoresizingMaskIntoConstraints = false;
            self.addSubview(customView)
            
            NSLayoutConstraint.activate([customView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                                         customView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                                         customView.topAnchor.constraint(equalTo: self.topAnchor),
                                         customView.bottomAnchor.constraint(equalTo: self.bottomAnchor)])
            
            self.view = customView
            lastBounds = self.bounds.size
            
            self.setupUI()
        }
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.lastBounds.equalTo(self.bounds.size) == false {
            lastBounds = self.bounds.size
            self.layoutChanged()
        }
    }
    
    public func setupUI() -> Void {
        self.view?.backgroundColor = UIColor.clear
        self.view?.isOpaque = false
    }
    
    public func layoutChanged() -> Void {}
    
    public func addTo(view: UIView) -> Void {
        self.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self)
        
        NSLayoutConstraint.activate([self.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     self.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     self.topAnchor.constraint(equalTo: view.topAnchor),
                                     self.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
}
