//
//  FileProvider.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 19/04/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import FilesProvider
import Photos
import Foundation

typealias CompleteBool = ((_ bool: Bool) -> Void)?

class FileProvider {
    static let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent(FileProvider.folder)
    static let folder = "cleanUp"

    static let shared = FileProvider()
    var documentsProvider: LocalFileProvider
    var timeDelay = 1.5

    private init() {
        documentsProvider = LocalFileProvider()
        documentsProvider.delegate = self
    }

    func createFolder(_ complete: @escaping (_ bool: Bool) -> Void) {
        if let urlStr = FileProvider.documentsURL?.absoluteString {
            documentsProvider.create(folder: FileProvider.folder, at: "/", completionHandler: { error in
                DispatchQueue.main.async {
                    if let error = error {
                        print("Error: \(error.localizedDescription)")
                        complete(false)
                    } else {
                        print("url: \(urlStr)")
                        complete(true)
                    }
                }
            })
        }
    }

    func getAllDocuments(completion: (([URL])->Void)?) {
        let files = FileManager.default.enumerator(atPath: FileProvider.documentsURL?.path ?? "")
        let allObjects = (files?.allObjects as? [String] ?? []).map({
            (FileProvider.documentsURL!.appendingPathComponent($0))
        })
        completion?(allObjects)
    }

    func createFile(_ path: String, data: Data?, _ complete: CompleteBool) {
        print("Name: ->>>> createFile")

        documentsProvider.writeContents(path: path, contents: data, atomically: true, overwrite: true) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                complete?(false)
            } else {
                print("Name: \(path)")
                complete?(true)
            }
        }
    }

    func deleteFile(_ paths: [String], _ complete: @escaping (() -> Void)) {
        let group = DispatchGroup()
        for path in paths {
            group.enter()
            do {
                try FileManager.default.removeItem(atPath: path)
                print("DeleteFile : \(path)")
                group.leave()
            } catch {
                group.leave()
                print("Error deleteFile : \(error.localizedDescription)")
            }
        }

        group.notify(queue: .main, execute: complete)
    }

    func copyFile(_ oldPath: String, newPath: String, _ complete: CompleteBool) {
        print("Name: ->>>> copyFile")

        documentsProvider.copyItem(path: oldPath, to: newPath, overwrite: false) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                complete?(false)
            } else {
                print("Name: ->>>>")
                complete?(true)
            }
        }
    }

    private func loadImage(url: URL) -> UIImage? {
        do {
            let imageData = try Data(contentsOf: url)
            return UIImage(data: imageData)?.toJPEG
        } catch {
            print("Error loading image : \(error)")
        }
        return nil
    }

    func assetToImage(_ assets: [PHAsset], completion: @escaping ([(name: String?, image: UIImage, data: Data?)]) -> Void) {
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        options.isNetworkAccessAllowed = true
        options.deliveryMode = .highQualityFormat
        options.resizeMode = .exact

        var images: [(name: String?, image: UIImage, data: Data?)] = []
        let queue = DispatchGroup()

        for asset in assets {
            let originalName = PHAssetResource.assetResources(for: asset).first?.originalFilename
            queue.enter()
            if asset.mediaType == .image {
                PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: options) { image, response in
                    if let image = image?.toJPEG {
                        images.append((name: originalName, image: image, image.toJPEGData))
                    } else if let response = response {
                        print(response)
                    }

                    queue.leave()
                }
            } else if asset.mediaType == .video {
                PHImageManager.default().requestAVAsset(forVideo: asset, options: nil) { (assetURL, audioMix, info) in
                    if let assetURL = assetURL as? AVURLAsset,
                        let data = try? Data(contentsOf: assetURL.url) {

                        PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: options) { image, response in
                            if let image = image?.toJPEG {
                                images.append((name: originalName, image: image, data))
                            } else if let response = response {
                                print(response)
                            }

                            queue.leave()
                        }
                    } else {
                        queue.leave()
                    }
                }
            }
        }

        queue.notify(queue: .main) {
            completion(images)
        }
    }
}

extension FileProvider: FileProviderDelegate {
    func fileproviderFailed(_ fileProvider: FileProviderOperations, operation: FileOperationType, error: Error) {
        switch operation {
        case .copy(source: let source, destination: _):
            print("copy of \(source) failed.")
        case .remove:
            print("file can't be deleted.")
        default:
            print("fileproviderFailed >>>>>>>>>>> \(operation.actionDescription) from \(operation.source) to \(operation.destination ?? "") failed")
        }
    }

    func fileproviderSucceed(_ fileProvider: FileProviderOperations, operation: FileOperationType) {
        switch operation {
        case .copy(source: let source, destination: let dest):
            print("\(source) copied to \(dest).")
        case .remove(path: let path):
            print("\(path) has been deleted.")
        case .create(path: let path):
            print("\(path) created success.")
        default:
            print("fileproviderSucceed >>>>>>>>>>> \(operation.actionDescription) from \(operation.source) to \(operation.destination ?? "") succeed")
        }
    }

    func fileproviderProgress(_ fileProvider: FileProviderOperations, operation: FileOperationType, progress: Float) {
        switch operation {
        case .copy(source: let source, destination: let dest):
            print("Copy\(source) to \(dest): \(progress * 100) completed.")
        case .create(path: let path):
            print("create\(path) to : \(progress * 100) completed.")
        default:
            break
        }
    }
}

extension URL {
    func checkFileExist() -> Bool {
        let path = self.path
        if (FileManager.default.fileExists(atPath: path)) {
            print("FILE AVAILABLE")
            return true
        } else {
            print("FILE NOT AVAILABLE")
            return false;
        }
    }
}
