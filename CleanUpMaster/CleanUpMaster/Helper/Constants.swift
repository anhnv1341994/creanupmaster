//
//  Constants.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Photos
import UIKit

struct AppConstants {
    static let appstoreURL = "https://apps.apple.com/app/id1535991253"
}

extension AppConstants {
    static let kSettingPhotoVideo = "kSettingPhotoVideo"
    static let kSettingContacts = "kSettingContacts"
    static let kSettingPasscode = "kSettingPasscode"
    static let kSettingPass = "kSettingPass"
    static let kSettingUseFaceID = "kSettingUseFaceID"
    static let kRecybin = "kRecybin"
    static let oldEvaluatedPolicy = "oldEvaluatedPolicy"
    static let listContact = "listContact"
    static let isPurchase = "PURCHASE"

}

extension UserDefaults {
    static var isPurchase: Bool? {
        get {
            return UserDefaults.standard.value(forKey: AppConstants.isPurchase) as? Bool
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppConstants.isPurchase)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var listContact: Data? {
        get {
            return UserDefaults.standard.value(forKey: AppConstants.listContact) as? Data
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppConstants.listContact)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    static var isEnablePhotoVideo: Bool? {
        get {
            return UserDefaults.standard.value(forKey: AppConstants.kSettingPhotoVideo) as? Bool
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppConstants.kSettingPhotoVideo)
            UserDefaults.standard.synchronize()
        }
    }

    static var isEnableContacts: Bool? {
        get {
            return UserDefaults.standard.value(forKey: AppConstants.kSettingContacts) as? Bool
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppConstants.kSettingContacts)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var passcode: String? {
        get {
            return UserDefaults.standard.value(forKey: AppConstants.kSettingPass) as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppConstants.kSettingPass)
            UserDefaults.standard.synchronize()
        }
    }

    static var isEnablePasscode: Bool? {
        get {
            return UserDefaults.standard.value(forKey: AppConstants.kSettingPasscode) as? Bool
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppConstants.kSettingPasscode)
            UserDefaults.standard.synchronize()
        }
    }

    static var isEnableUseFaceID: Bool? {
        get {
            return UserDefaults.standard.value(forKey: AppConstants.kSettingUseFaceID) as? Bool
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppConstants.kSettingUseFaceID)
            UserDefaults.standard.synchronize()
        }
    }

    static var listRecybin: [String]? {
        get {
            return UserDefaults.standard.value(forKey: AppConstants.kRecybin) as? [String]
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppConstants.kRecybin)
            UserDefaults.standard.synchronize()
        }
    }

    static func removeImageInRecybin(identifys: [String]) {
        guard let list = UserDefaults.listRecybin else { return }
        let assets = PHAsset.fetchAssets(withLocalIdentifiers: identifys, options: nil)
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.deleteAssets(assets)
        })
        var list1: Set = Set(list)
        let list2: Set = Set(identifys)
        list1.formSymmetricDifference(list2)
        UserDefaults.standard.set(list1.count > 0 ? list1 : nil, forKey: AppConstants.kRecybin)
        UserDefaults.standard.synchronize()
    }
    
    // MARK: - Improve recognition

    static var getOldEvaluatedPolicy: String? {
        return UserDefaults.standard.value(forKey: AppConstants.oldEvaluatedPolicy) as? String
    }

    static func setOldEvaluatedPolicy(_ str: String?) {
        UserDefaults.standard.set(str, forKey: AppConstants.oldEvaluatedPolicy)
    }
}

enum PhotosType {
    case all, simular, serial, blur, screen, dynamic
}


class AlertHelper {
    static func create(title: String? = nil,
                       titleFont: UIFont? = UIFont.semibold(17.0),
                       titleColor: UIColor? = UIColor.black,
                       message: String?,
                       messageFont: UIFont? = UIFont.regular(13.0),
                       messageColor: UIColor? = UIColor.black,
                       highlightMessage: String? = nil,
                       highlightMessageColor: UIColor? = UIColor.black,
                       highlighMessageFont: UIFont? = UIFont.medium(13.0),
                       type: UIAlertController.Style = .alert,
                       buttonTitle: String? = nil,
                       buttonColor: UIColor? = UIColor.colorBlue,
                       buttonHandler: (() -> Void)? = nil,
                       buttonTitle1: String? = nil,
                       buttonColor1: UIColor? = UIColor.colorBlue,
                       buttonHandler1: (() -> Void)? = nil,
                       buttonTitle2: String? = nil,
                       buttonColor2: UIColor? = UIColor.colorBlue,
                       buttonHandler2: (() -> Void)? = nil,
                       cancelButtonTitle: String? = nil,
                       cancelButtonColor: UIColor? = UIColor.colorBlue,
                       cancelHandler: (() -> Void)? = nil) -> UIAlertController {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: type)
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.lineSpacing = 4.0
        paragraph.paragraphSpacing = 4.0
        
        if let title = title, let titleFont = titleFont, let titleColor = titleColor {
            let customTitle = NSMutableAttributedString(string: title, attributes: [.font: titleFont, .foregroundColor: titleColor])
            alertController.setValue(customTitle, forKey: "attributedTitle")
        }
        
        if let message = message, let messageFont = messageFont, let messageColor = messageColor {
            let customMessage = NSMutableAttributedString(string: " \n", attributes: [.font: UIFont.regular(1.0)])
            
            if let highlightMessage = highlightMessage, let hightlightMessageColor = highlightMessageColor, let highlighMessageFont = highlighMessageFont {
                customMessage.append(NSAttributedString(string: String(format: message, highlightMessage), attributes: [.font: messageFont, .foregroundColor: messageColor, .paragraphStyle: paragraph]))
                
                let highlighRange = (customMessage.string as NSString).range(of: highlightMessage)
                customMessage.setAttributes([.font: highlighMessageFont, .foregroundColor: hightlightMessageColor, .paragraphStyle: paragraph], range: highlighRange)
                
                alertController.setValue(customMessage, forKey: "attributedMessage")
            } else {
                customMessage.append(NSAttributedString(string: message, attributes: [.font: messageFont, .foregroundColor: messageColor, .paragraphStyle: paragraph]))
            }
            alertController.setValue(customMessage, forKey: "attributedMessage")
        }
        
        if let cancelButtonTitle = cancelButtonTitle {
            let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) {
                _ in
                cancelHandler?()
            }
            
            if let cancelButtonColor = cancelButtonColor {
                cancelAction.setValue(cancelButtonColor, forKey: "titleTextColor")
            }
            
            alertController.addAction(cancelAction)
        }
        
        if let buttonTitle = buttonTitle {
            let okAction = UIAlertAction(title: buttonTitle, style: .default) {
                _ in
                buttonHandler?()
            }
            
            if let buttonColor = buttonColor {
                okAction.setValue(buttonColor, forKey: "titleTextColor")
            }
            
            alertController.addAction(okAction)
            alertController.preferredAction = okAction
        }
        
        if let buttonTitle = buttonTitle1 {
            let okAction = UIAlertAction(title: buttonTitle, style: .default) {
                _ in
                buttonHandler1?()
            }
            
            if let buttonColor = buttonColor1 {
                okAction.setValue(buttonColor, forKey: "titleTextColor")
            }
            
            alertController.addAction(okAction)
            alertController.preferredAction = okAction
        }
        
        if let buttonTitle = buttonTitle2 {
            let okAction = UIAlertAction(title: buttonTitle, style: .default) {
                _ in
                buttonHandler2?()
            }
            
            if let buttonColor = buttonColor2 {
                okAction.setValue(buttonColor, forKey: "titleTextColor")
            }
            
            alertController.addAction(okAction)
            alertController.preferredAction = okAction
        }
        
        if cancelButtonTitle == nil, buttonTitle == nil, buttonTitle1 == nil, buttonTitle2 == nil {
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) {
                _ in
                cancelHandler?()
            }
            
            if let cancelButtonColor = cancelButtonColor {
                cancelAction.setValue(cancelButtonColor, forKey: "titleTextColor")
            }
            
            alertController.addAction(cancelAction)
        }
        
        return alertController
    }
}
