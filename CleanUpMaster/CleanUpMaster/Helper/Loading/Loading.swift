//
//  ScannerLoading.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 08/06/2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import Foundation
import UIKit

class Loading: UIViewController, NVActivityIndicatorViewable {
    static let shared = Loading()
    var timer: Timer?
    
    func startLoading() {
        self.startAnimating(CGSize(width: 50, height: 50), type: .ballRotateChase)
        timer = Timer.scheduledTimer(timeInterval: 90, target: self, selector: #selector(stopLoading), userInfo: nil, repeats: true)
    }
    
    @objc func stopLoading() {
        self.stopAnimating(nil)
        self.stopTimer()
    }
    
    private func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
            self.stopAnimating(nil)
        }
    }
}
