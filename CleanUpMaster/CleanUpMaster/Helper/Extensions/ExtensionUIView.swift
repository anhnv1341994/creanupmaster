//
//  ExtensionUIView.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 07/20/2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable
    var cornerRadius1: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable
    var borderWidth1: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable
    var borderColor1: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            }
            else {
                layer.borderColor = nil
            }
        }
    }

    @IBInspectable
    var shadowRadius1: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }

    @IBInspectable
    var shadowOpacity1: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }

    @IBInspectable
    var shadowOffset1: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }

    @IBInspectable
    var shadowColor1: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            }
            else {
                layer.shadowColor = nil
            }
        }
    }
    
    @discardableResult
    func addSubviewAndFill(_ subview: UIView) -> [NSLayoutConstraint] {
        addSubview(subview)
        
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            subview.topAnchor.constraint(equalTo: topAnchor),
            trailingAnchor.constraint(equalTo: subview.trailingAnchor),
            bottomAnchor.constraint(equalTo: subview.bottomAnchor),
            subview.leadingAnchor.constraint(equalTo: leadingAnchor)
        ]
        constraints.forEach {
            $0.priority = UILayoutPriority(999)
            $0.isActive = true
        }
        
        return constraints
    }
    
    func viewFromOwnedNib(named nibName: String? = nil) -> UIView {
        let bundle = Bundle(for: self.classForCoder)
        return {
            if let nibName = nibName {
                return bundle.loadNibNamed(nibName, owner: self, options: nil)!.last as! UIView
            }
            return bundle.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)!.last as! UIView
        }()
    }
    
    func curvedView(desiredCurve: CGFloat?) {
        let offset: CGFloat = self.frame.width / desiredCurve!
        let bounds: CGRect = self.bounds
        
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        
        // Create the shape layer and set its path
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        
        // Set the newly created shape layer as the mask for the view's layer
        self.layer.mask = maskLayer
    }
    
    func fadeIn(_ duration: TimeInterval = 0.3, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = { (_: Bool) -> Void in }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(_ duration: TimeInterval = 0.3, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = { (_: Bool) -> Void in }) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
    func animationButton() {
        self.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIView.AnimationOptions.allowUserInteraction,
                       animations: {
                           self.transform = CGAffineTransform.identity
        }, completion: { _ in () })
    }
    
    /**
     Simply zooming in of a view: set view scale to 0 and zoom to Identity on 'duration' time interval.
     
     - parameter duration: animation duration
     */
    func animationZoomIn(_ duration: TimeInterval = 0.2) {
        self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = .identity
        }) { (_: Bool) -> Void in
        }
    }
    
    /**
     Simply zooming out of a view: set view scale to Identity and zoom out to 0 on 'duration' time interval.
     
     - parameter duration: animation duration
     */
    func animationZoomOut(_ duration: TimeInterval = 0.2) {
        self.transform = .identity
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        }) { (_: Bool) -> Void in
        }
    }
    
    /**
     Zoom in any view with specified offset magnification.
     
     - parameter duration:     animation duration.
     - parameter easingOffset: easing offset.
     */
    func animationZoomInWithEasing(_ duration: TimeInterval = 0.2, easingOffset: CGFloat = 0.2) {
        let easeScale = 1.0 + easingOffset
        let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
        let scalingDuration = duration - easingDuration
        UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
        }, completion: { (_: Bool) -> Void in
            UIView.animate(withDuration: easingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
                self.transform = .identity
            }, completion: { (_: Bool) -> Void in
            })
        })
    }
    
    /**
     Zoom out any view with specified offset magnification.
     
     - parameter duration:     animation duration.
     - parameter easingOffset: easing offset.
     */
    func animationZoomOutWithEasing(_ duration: TimeInterval = 0.2, easingOffset: CGFloat = 0.2) {
        let easeScale = 1.0 + easingOffset
        let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
        let scalingDuration = duration - easingDuration
        UIView.animate(withDuration: easingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
        }, completion: { (_: Bool) -> Void in
            UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
                self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            }, completion: { (_: Bool) -> Void in
            })
        })
    }
    
    func animationPulsate() { // to nhỏ
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.2
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: "pulse")
    }
    
    func animationFlash() { // nhấp nháy
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.2
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 3
        
        layer.add(flash, forKey: nil)
    }
    
    func animationShake() { // rung
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.05
        shake.repeatCount = 2
        shake.autoreverses = true
        
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        
        shake.fromValue = fromValue
        shake.toValue = toValue
        
        layer.add(shake, forKey: "position")
    }
    
    func animationButton1() {
        UIButton.animate(withDuration: 0.25,
                         animations: {
                             self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                         },
                         completion: { _ in
                             UIButton.animate(withDuration: 0.2, animations: {
                                 self.transform = CGAffineTransform.identity
                            })
        })
    }
    
    func animationNumberPad() {
        UIButton.animate(withDuration: 0.05,
                         animations: {
                             self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                         },
                         completion: { _ in
                             UIButton.animate(withDuration: 0.05, animations: {
                                 self.transform = CGAffineTransform.identity
                            })
        })
    }
}

extension UIView {
    var insetArea: UIEdgeInsets {
        let insets: UIEdgeInsets
        if #available(iOS 11.0, *) {
            insets = self.safeAreaInsets
        }
        else {
            insets = UIEdgeInsets.zero
        }
        return insets
    }
    
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    
    fileprivate typealias Action = (() -> Void)?
    
    // Set our computed property type to a closure
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    // This is the meat of the sauce, here we create the tap gesture recognizer and
    // store the closure the user passed to us in the associated object we declared above
    public func addTapGesture(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // Every time the user taps on the UIImageView, this function gets called,
    // which triggers the closure we stored
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        }
        else {}
    }
}

class RoundedView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            if self.cornerRadius != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var borderColorType: UIColor? {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var selectedBorderColor: UIColor? {
        let color: UIColor?
        if let colorType = self.borderColorType {
            color = colorType
        }
        else if let selectedColor = self.borderColor {
            color = selectedColor
        }
        else {
            color = nil
        }
        return color
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            if self.borderWidth != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var topLeft: Bool = false {
        didSet {
            if self.topLeft != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var topRight: Bool = false {
        didSet {
            if self.topRight != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var bottomLeft: Bool = false {
        didSet {
            if self.bottomLeft != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    @IBInspectable var bottomRight: Bool = false {
        didSet {
            if self.bottomRight != oldValue {
                self.setNeedsDisplay()
            }
        }
    }
    
    private var lastBounds: CGSize = CGSize.zero
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    func initialize() -> Void {
        self.isOpaque = false
        self.clipsToBounds = true
        self.lastBounds = self.bounds.size
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if let _ = self.superview {
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.lastBounds.equalTo(self.bounds.size) == false {
            self.lastBounds = self.bounds.size
            
            self.setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        var corners: UIRectCorner = []
        
        if self.topLeft {
            corners = [corners, .topLeft]
        }
        
        if self.topRight {
            corners = [corners, .topRight]
        }
        
        if self.bottomLeft {
            corners = [corners, .bottomLeft]
        }
        
        if self.bottomRight {
            corners = [corners, .bottomRight]
        }
        
        let color = (self.backgroundColor ?? .white)
        
        let roundedRect = self.bounds.insetBy(dx: self.borderWidth / 2.0, dy: self.borderWidth / 2.0)
        let cornerRadii = CGSize(width: self.cornerRadius, height: self.cornerRadius)
        let bezierPath = UIBezierPath(roundedRect: roundedRect,
                                      byRoundingCorners: corners,
                                      cornerRadii: cornerRadii)
        color.setFill()
        bezierPath.fill()
        
        if let color = self.selectedBorderColor {
            bezierPath.lineWidth = self.borderWidth
            color.setStroke()
            bezierPath.stroke()
        }
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = bezierPath.cgPath
        self.layer.mask = maskLayer
    }
}
