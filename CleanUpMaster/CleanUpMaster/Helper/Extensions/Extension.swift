//
//  Extension.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 07/20/2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import UIKit
import Contacts

extension Optional where Wrapped == Int {
    var int: Int {
        return self ?? 0
    }

    var string: String {
        return "\(self ?? 0)"
    }
}

extension Optional where Wrapped == String {
    var int: Int {
        return Int(self ?? "0") ?? 0
    }

    var string: String {
        return self ?? ""
    }

    var double: Double {
        return Double(self ?? "0.0") ?? 0.0
    }
}

extension Int {
    var toString: String {
        return String(describing: self)
    }
}

extension String {
    public func isImageType() -> Bool {
        // image formats which you want to check
        let imageFormats = ["JPG", "JPEG", "PNG", "GIF", "HEIC"]

        if URL(string: self) != nil {

            let extensi = (self as NSString).pathExtension
            print("-> isImageType -> ", extensi.uppercased())
            return imageFormats.contains(extensi.uppercased())
        }
        return false
    }
}

public extension Int {

    /// Returns a random Int point number between 0 and Int.max.
    static var random: Int {
        return Int.random(n: Int.max)
    }

    /// Random integer between 0 and n-1.
    ///
    /// - Parameter n:  Interval max
    /// - Returns:      Returns a random Int point number between 0 and n max
    static func random(n: Int) -> Int {
        return Int(arc4random_uniform(UInt32(n)))
    }

    ///  Random integer between min and max
    ///
    /// - Parameters:
    ///   - min:    Interval minimun
    ///   - max:    Interval max
    /// - Returns:  Returns a random Int point number between 0 and n max
    static func random(min: Int, max: Int) -> Int {
        return Int.random(n: max - min + 1) + min

    }
}

// MARK: Double Extension

public extension Double {

    /// Returns a random floating point number between 0.0 and 1.0, inclusive.
    static var random: Double {
        return Double(arc4random()) / 0xFFFFFFFF
    }

    /// Random double between 0 and n-1.
    ///
    /// - Parameter n:  Interval max
    /// - Returns:      Returns a random double point number between 0 and n max
    static func random(min: Double, max: Double) -> Double {
        return Double.random * (max - min) + min
    }
}

// MARK: Float Extension

public extension Float {

    /// Returns a random floating point number between 0.0 and 1.0, inclusive.
    static var random: Float {
        return Float(arc4random()) / 0xFFFFFFFF
    }

    /// Random float between 0 and n-1.
    ///
    /// - Parameter n:  Interval max
    /// - Returns:      Returns a random float point number between 0 and n max
    static func random(min: Float, max: Float) -> Float {
        return Float.random * (max - min) + min
    }
}

// MARK: CGFloat Extension

public extension CGFloat {

    /// Randomly returns either 1.0 or -1.0.
    static var randomSign: CGFloat {
        return (arc4random_uniform(2) == 0) ? 1.0 : -1.0
    }

    /// Returns a random floating point number between 0.0 and 1.0, inclusive.
    static var random: CGFloat {
        return CGFloat(Float.random)
    }

    /// Random CGFloat between 0 and n-1.
    ///
    /// - Parameter n:  Interval max
    /// - Returns:      Returns a random CGFloat point number between 0 and n max
    static func random(min: CGFloat, max: CGFloat) -> CGFloat {
        return CGFloat.random * (max - min) + min
    }
}

extension UInt64 {
    var toMegabyte: String {
        let size: CGFloat = CGFloat(self) / 1024.0 / 1024.0
        let str = String(format: "%.2f", size)
        return str
    }
}

extension UICollectionView {
    func register<T: UICollectionReusableView>(viewType: T.Type, forElementKind elementKind: String, bundle: Bundle? = nil) {
        let nib = UINib(nibName: String(describing: T.self), bundle: bundle)
        self.register(nib, forSupplementaryViewOfKind: elementKind, withReuseIdentifier: T.self.description())
    }

    func dequeueReusableCell<T: UICollectionReusableView>(with type: T.Type, elementKind: String, for indexPath: IndexPath) -> T {
        return self.dequeueReusableSupplementaryView(ofKind: elementKind, withReuseIdentifier: T.self.description(), for: indexPath) as! T
    }
}

extension UIApplication {
    func open(url: URL, _ completion: ((Bool) -> Void)? = nil) {
        if #available(iOS 10.0, *) {
            self.open(url, options: [:], completionHandler: completion)
        } else {
            if let completion = completion {
                completion(self.openURL(url))
            } else {
                self.openURL(url)
            }
        }
    }

    func open(urlString: String?, _ completion: ((Bool) -> Void)? = nil) {
        guard let urlString = urlString, let url = URL(string: urlString) else {
            if let completion = completion {
                completion(false)
            }
            return
        }
        self.open(url: url, completion)
    }

    func openAppSettings() {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            self.open(url: url)
        }
    }

    func call(number: String) {
        if let url = URL(string: "tel://\(number)") {
            self.open(url: url)
        }
    }

    var hasTopNotch: Bool {
        if #available(iOS 11.0, *) {
            return self.delegate?.window??.safeAreaInsets.top ?? 0.0 > 20.0
        }
        return false
    }

    var statusBarHeight: CGFloat {
        let statusBarSize = self.statusBarFrame.size
        return fmin(statusBarSize.width, statusBarSize.height)
    }
}

extension UIViewController {
    @objc func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }

    func dismissViewController(_ dismisAll: Bool = false) {
        if dismisAll {
            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)

        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    func openURL(_ urlStr: String) {
        guard let url = URL(string: urlStr) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:]) { _ in }
        } else {
            UIApplication.shared.openURL(url)
        }
    }

    func sharedContact(contacts: [CNContact]) {
        var fileLocations: [URL] = []
        let fileManager = FileManager.default
        let cacheDirectory = try! fileManager.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)


        let queue = OperationQueue()
        queue.name = "saveurlcontact"
        queue.maxConcurrentOperationCount = 1

        for contact in contacts {
            queue.addOperation {
                let fileLocation = cacheDirectory.appendingPathComponent("\(CNContactFormatter().string(from: contact)!).vcf")
                let contactData = try! CNContactVCardSerialization.data(with: [contact])
                do {
                    try contactData.write(to: fileLocation.absoluteURL, options: .atomicWrite)
                    fileLocations.append(fileLocation)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }


        queue.addOperation {
            DispatchQueue.main.async {

                let avc = UIActivityViewController(activityItems: fileLocations, applicationActivities: nil)
                // Apps to exclude sharing to
                avc.excludedActivityTypes = [
                        .airDrop, .print, .saveToCameraRoll, .addToReadingList, .mail, .message,
                        .postToFacebook, .postToFlickr, .postToTwitter, .postToTencentWeibo
                ]

                if UIDevice.current.userInterfaceIdiom == .pad {
                    avc.popoverPresentationController?.sourceView = self.view
                    avc.popoverPresentationController?.permittedArrowDirections = .down
                    avc.popoverPresentationController?.sourceRect = CGRect(x: 30, y: self.view.frame.height - 130, width: 60, height: 60)
                }
                if #available(iOS 13.0, *) {
                    avc.isModalInPresentation = true
                } else {
                    // Fallback on earlier versions
                }
                self.present(avc, animated: true, completion: nil)
            }
        }
        
        queue.waitUntilAllOperationsAreFinished()
    }

    func sharedAppScan(_ text: Any?, file: Any?, rootVC: UIViewController? = nil, buttonArrow left: Bool = true) {
        var items: [Any] = []
        if let text = text as? String {
            items.append(text)
        }

        if let images = file as? [UIImage] {
            for image in images {
                items.append(image)
            }
        }

        if let urls = file as? [URL] {
            for url in urls {
                items.append(url)
            }
        }

        if let url = file as? URL {
            items.append(url)
        }

        if let image = file as? UIImage {
            items.append(image)
        }

        let avc = UIActivityViewController(activityItems: items, applicationActivities: nil)
        // Apps to exclude sharing to
        avc.excludedActivityTypes = [
                .airDrop, .print, .saveToCameraRoll, .addToReadingList, .mail, .message,
                .postToFacebook, .postToFlickr, .postToTwitter, .postToTencentWeibo
        ]
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        // If user on iPad
        if UIDevice.current.userInterfaceIdiom == .pad {
            avc.popoverPresentationController?.sourceView = view
            avc.popoverPresentationController?.permittedArrowDirections = .down
            if left {
                avc.popoverPresentationController?.sourceRect = CGRect(x: 30, y: view.frame.height - 130, width: 60, height: 60)
            } else {
                avc.popoverPresentationController?.sourceRect = CGRect(x: view.frame.width - 30, y: view.frame.height - 130, width: 60, height: 60)
            }
        }
        if rootVC != nil {
            rootVC?.present(avc, animated: true, completion: nil)
        } else {
            appDelegate?.window?.rootViewController?.present(avc, animated: true, completion: nil)
        }

        avc.completionWithItemsHandler = { (_: UIActivity.ActivityType?, completed:
                Bool, _: [Any]?, error: Error?) in
            if completed {
                print("share completed")
                avc.dismiss(animated: true, completion: nil)

                return
            } else {
                print("cancel")
            }
            if let shareError = error {
                print("error while sharing: \(shareError.localizedDescription)")
            }
        }
    }

    func printApp(imageURLs: [NSURL]?, images: [URL]?) {
        let printInfo = UIPrintInfo(dictionary: nil)
        if let imageURLs = imageURLs {
            printInfo.jobName = imageURLs.first?.lastPathComponent ?? "print"
        }
        printInfo.outputType = .general

        let printController = UIPrintInteractionController.shared
        printController.printInfo = printInfo
        printController.showsNumberOfCopies = false
        printController.printingItems = imageURLs != nil ? imageURLs : images
        printController.present(animated: true, completionHandler: nil)
    }

    func removePDF(_ path: URL) {
        let fileManager = FileManager.default
        let documentsUrl = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first! as NSURL
        let documentsPath = documentsUrl.path

        do {
            if let documentPath = documentsPath {
                let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                print("all files in cache: \(fileNames)")
                for fileName in fileNames {
                    if fileName.hasSuffix(".png") {
                        let filePathName = "\(documentPath)/\(fileName)"
                        try fileManager.removeItem(atPath: filePathName)
                    }
                }

                let files = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                print("all files in cache after deleting images: \(files)")
            }

        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
}

extension UITextField {
    func addDashedLine(strokeColor: UIColor, lineWidth: CGFloat) {
        backgroundColor = .clear
        let widthTF = UIScreen.main.bounds.width - 210
        let string = (text ?? "") as NSString
        let stringSize: CGSize = string.size(withAttributes:
                [NSAttributedString.Key.font: UIFont.regular(14)])
        let center = self.center
        if stringSize.width < widthTF {
            layer.sublayers = nil
            let shapeLayer = CAShapeLayer()
            shapeLayer.name = "DashedBottomLine"
            shapeLayer.bounds = bounds
            shapeLayer.position = CGPoint(x: center.x - stringSize.width / 2 + 5, y: 60)
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = strokeColor.cgColor
            shapeLayer.lineWidth = lineWidth
            shapeLayer.lineJoin = CAShapeLayerLineJoin.round
            shapeLayer.lineDashPattern = [4, 4]

            let path = CGMutablePath()
            path.move(to: CGPoint.zero)
            path.addLine(to: CGPoint(x: stringSize.width + 15, y: 0))
            shapeLayer.path = path
            layer.addSublayer(shapeLayer)
        }
    }

    func addDashedLine1(strokeColor: UIColor, lineWidth: CGFloat) {
        backgroundColor = .clear
        let widthTF = UIScreen.main.bounds.width - 124
        let string = (text ?? "") as NSString
        let stringSize: CGSize = string.size(withAttributes:
                [NSAttributedString.Key.font: UIFont.regular(14)])
        let center = self.center
        if stringSize.width < widthTF {
            layer.sublayers = nil
            let shapeLayer = CAShapeLayer()
            shapeLayer.name = "DashedBottomLine"
            shapeLayer.bounds = bounds
            shapeLayer.position = CGPoint(x: center.x - stringSize.width / 2 + 5, y: 60)
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = strokeColor.cgColor
            shapeLayer.lineWidth = lineWidth
            shapeLayer.lineJoin = CAShapeLayerLineJoin.round
            shapeLayer.lineDashPattern = [4, 4]

            let path = CGMutablePath()
            path.move(to: CGPoint.zero)
            path.addLine(to: CGPoint(x: center.x, y: 0))
            shapeLayer.path = path
            layer.addSublayer(shapeLayer)
        }
    }
}

extension UIImage {
    var toJPEGData: Data? {
        return self.jpegData(compressionQuality: 0.5)
    }

    var toJPEG: UIImage? {
        guard let data = self.toJPEGData else { return nil }
        return UIImage(data: data)
    }

    class func resize(images: [UIImage], targetSize: CGSize) -> [UIImage] {
        var listImageResize: [UIImage] = []
        for image in images {
            let size = image.size

            let widthRatio = targetSize.width / image.size.width
            let heightRatio = targetSize.height / image.size.height

            var newSize: CGSize
            if widthRatio > heightRatio {
                newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
            } else {
                newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
            }

            let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

            UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
            image.draw(in: rect)
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            listImageResize.append(newImage!)
        }
        return listImageResize
    }

    class func scale(images: [UIImage], by scale: CGFloat) -> [UIImage] {
        var listImageScale: [UIImage] = []
        for image in images {
            let size = image.size
            let scaledSize = CGSize(width: size.width * scale, height: size.height * scale)
            let widthRatio = scaledSize.width / image.size.width
            let heightRatio = scaledSize.height / image.size.height

            var newSize: CGSize
            if widthRatio > heightRatio {
                newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
            } else {
                newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
            }

            let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

            UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
            image.draw(in: rect)
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            listImageScale.append(newImage!)
        }
        return listImageScale
    }

    func rotate(radians: CGFloat) -> UIImage {
        let rotatedSize = CGRect(origin: .zero, size: self.size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)

        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            self.draw(in: CGRect(x: -origin.y, y: -origin.x,
                width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return rotatedImage ?? self
        }
        return self
    }

    enum JPEGQuality: CGFloat {
        case lowest = 0
        case low = 0.25
        case medium = 0.5
        case high = 0.75
        case highest = 1
    }

    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func dataQuality(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

extension String {
    func image(withAttributes attributes: [NSAttributedString.Key: Any]? = nil, size: CGSize? = nil) -> UIImage? {
        let size = size ?? (self as NSString).size(withAttributes: attributes)
        return UIGraphicsImageRenderer(size: size).image { _ in
            (self as NSString).draw(in: CGRect(origin: .zero, size: size),
                withAttributes: attributes)
        }
    }
}

extension Bundle {
    var appVersion: String? {
        self.infoDictionary?["CFBundleShortVersionString"] as? String
    }

    static var mainAppVersion: String? {
        Bundle.main.appVersion
    }
}

extension UITextField {
    func setClearButton(with image: UIImage) {
        if let button = self.value(forKey: "clearButton") as? UIButton {
            button.setImage(image, for: .normal)
        }
    }
}

extension UIViewController {
    func present(_ owner: UIViewController, completion: (() -> Void)? = nil) {
        owner.present(self, animated: true, completion: completion)
    }

    func presentFullScreen(_ vc: UIViewController, completion: (() -> Void)? = nil) {
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .coverVertical
        self.present(vc, animated: true, completion: completion)
    }

    func pushViewWithScreen(_ vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func setLeftNavigationButton(_ imageName: String) {
        self.navigationItem.leftBarButtonItems = nil
        self.navigationItem.hidesBackButton = true
        let button = UIBarButtonItem(image: UIImage(named: imageName), style: .plain, target: self, action: #selector(actionLeftBar))
        self.navigationItem.leftBarButtonItem = button
    }

    func setRightNavigationButton(_ imageName: String) {
        self.navigationItem.rightBarButtonItems = nil
        let button = UIBarButtonItem(image: UIImage(named: imageName), style: .plain, target: self, action: #selector(actionRightBar))
        self.navigationItem.rightBarButtonItem = button
    }

    @objc func actionLeftBar() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func actionRightBar() { }
}

public protocol ClassNameProtocol {
    static var className: String { get }
    var className: String { get }
}

public extension ClassNameProtocol {
    static var className: String {
        return String(describing: self)
    }

    var className: String {
        return type(of: self).className
    }
}

extension NSObject: ClassNameProtocol { }

public extension UICollectionView {
    func register<T: UICollectionViewCell>(cellType: T.Type, bundle: Bundle? = nil) {
        let className = cellType.className
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: className)
    }

    func dequeueReusableCell<T: UICollectionViewCell>(with type: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: type.className, for: indexPath) as! T
    }
}

public extension UITableView {
    func register<T: UITableViewCell>(cellType: T.Type, bundle: Bundle? = nil) {
        let className = cellType.className
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forCellReuseIdentifier: className)
    }

    func register<T: UITableViewCell>(cellTypes: [T.Type], bundle: Bundle? = nil) {
        cellTypes.forEach {
            register(cellType: $0, bundle: bundle)
        }
    }

    func dequeueReusableCell<T: UITableViewCell>(with type: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: type.className, for: indexPath) as! T
    }

    func register<T: UITableViewHeaderFooterView>(headerFooterType: T.Type, bundle: Bundle? = nil) {
        let className = headerFooterType.className
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forHeaderFooterViewReuseIdentifier: className)
    }

    func register<T: UITableViewHeaderFooterView>(headerFooterTypes: [T.Type], bundle: Bundle? = nil) {
        headerFooterTypes.forEach {
            register(headerFooterType: $0, bundle: bundle)
        }
    }

    func dequeueReusableHeaderFooter<T: UITableViewHeaderFooterView>(with type: T.Type) -> T {
        return self.dequeueReusableHeaderFooterView(withIdentifier: type.className) as! T
    }
}

extension String {
    func index(at position: Int, from start: Index? = nil) -> Index? {
        let startingIndex = start ?? startIndex
        return index(startingIndex, offsetBy: position, limitedBy: endIndex)
    }

    func character(at position: Int) -> Character? {
        guard position >= 0, let indexPosition = index(at: position) else {
            return nil
        }
        return self[indexPosition]
    }

    func TPInitials() -> String {
        let substrings = self.split(separator: " ")
        var returnString = ""
        if let f = substrings.first?.first {
            returnString.append(f)
        }
        if let l = substrings.last?.first {
            returnString.append(l)
        }
        return returnString.uppercased()
    }

    func firstLetter() -> String {
        guard let firstChar = self.first else {
            return ""
        }
        return String(firstChar)
    }

    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }

    func substring(from range: NSRange) -> String? {
        if let range = Range(range, in: self) {
            return String(self[range])
        }
        return nil
    }

    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+(\\.[A-Za-z]{2,64})+"
        let emailPred = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }

    // width
    func width(withFont font: UIFont) -> CGFloat {
        let attributedString = NSAttributedString(string: self, attributes: [.font: font])
        return ceil(attributedString.size().width)
    }

    func attributedString(withFont font: UIFont, textColor: UIColor, lineSpacing: CGFloat, alignment: NSTextAlignment = .natural, lineBreakMode: NSLineBreakMode = .byWordWrapping) -> NSAttributedString {
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = alignment
        paragraph.lineSpacing = lineSpacing
        paragraph.lineBreakMode = lineBreakMode

        return self.attributes([.font: font, .foregroundColor: textColor, .paragraphStyle: paragraph])
    }

    func attributes(_ attributes: [NSAttributedString.Key: Any]?) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: attributes)
    }

    func unsign() -> String {
        var text = self.folding(options: .diacriticInsensitive, locale: Locale(identifier: "en_US"))
        text = text.replacingOccurrences(of: "đ", with: "d")
        text = text.replacingOccurrences(of: "Đ", with: "D")

        return text
    }

    func trimming() -> String {
        var text = self.trimmingCharacters(in: .whitespacesAndNewlines)
        text = text.replacingOccurrences(of: "\n", with: "")
        return text
    }

    func trimmingBreakLine() -> String {
        var text = self.trimmingCharacters(in: .whitespacesAndNewlines)
        text = text.replacingOccurrences(of: "\n", with: " ")
        return text
    }

    func removeSpecial() -> String {
        let special: Set<Character> = Set("!@#$%^&*<>_?{|}~[/]")
        return String(self.filter { !special.contains($0) }).replacingOccurrences(of: "\\", with: "")
    }

    func isPinCode(digit: Int) -> Bool {
        if self.count == digit, self.isOnlyNumeric() {
            return true
        }
        return false
    }

    func isOnlyNumeric() -> Bool {
        return self.isOnly(.decimalDigits)
    }

    func isOnly(_ characterSet: CharacterSet) -> Bool {
        return self.trimmingCharacters(in: characterSet).count <= 0
    }

    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    func normalized() -> String {
        return self.replacingOccurrences(of: "\\p{Cf}", with: "", options: .regularExpression)
    }

//    var emojilessStringWithSubstitution: String {
//        let emojiPatterns = [UnicodeScalar(0x1F600)!...UnicodeScalar(0x1F64F)!,
//            UnicodeScalar(0x1F300)!...UnicodeScalar(0x1F5FF)!,
//            UnicodeScalar(0x1F680)!...UnicodeScalar(0x1F6FF)!,
//            UnicodeScalar(0x2600)!...UnicodeScalar(0x26FF)!,
//            UnicodeScalar(0x2700)!...UnicodeScalar(0x27BF)!,
//            UnicodeScalar(0xFE00)!...UnicodeScalar(0xFE0F)!]
//
//        return self.unicodeScalars
//            .map { ucScalar in
//            emojiPatterns.contains { $0 ~= ucScalar } ? UnicodeScalar(32) : ucScalar
//        }
//            .reduce("") { $0 + String($1) }
//            .condenseWhitespace()
//    }

    func condenseWhitespace() -> String {
        let components = self.components(separatedBy: .whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }

    var isAlphaNumbericSpace: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9 ]", options: .regularExpression) == nil
    }

    var isAlphaNumberic: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }

    var isNumberic: Bool {
        return !isEmpty && range(of: "[^0-9]", options: .regularExpression) == nil
    }

    var isDecimalNumber: Bool {
        return !isEmpty && range(of: "[^0-9.]", options: .regularExpression) == nil
    }

    var isAlpha: Bool {
        return !isEmpty && range(of: "[^a-zA-Z]", options: .regularExpression) == nil
    }

    func isAcceptedCharacters(_ characters: String? = nil) -> Bool {
        if let characters = characters {
            if self.lowercased().rangeOfCharacter(from: CharacterSet(charactersIn: characters)) != nil {
                return false
            }
        }

        if self.lowercased().rangeOfCharacter(from: CharacterSet.illegalCharacters) != nil {
            return false
        }

        if self.lowercased().rangeOfCharacter(from: CharacterSet.symbols) != nil {
            return false
        }

        return true
    }

    var isBackspace: Bool {
        if let char = self.cString(using: String.Encoding.utf8) {
            if char.elementsEqual([0]) {
                return true
            }
        }
        return false
    }

    func isAcceptedCharactersChangeName(_ characters: String? = nil) -> Bool {
        if let characters = characters {
            if self.lowercased().rangeOfCharacter(from: CharacterSet(charactersIn: characters)) != nil {
                return false
            }
        }

        return true
    }
}

extension UIImage {
    func tintColor(_ color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()

        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)

        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
}

extension UIDevice {
    func MBFormatter(_ bytes: Int64) -> String {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = ByteCountFormatter.Units.useMB
        formatter.countStyle = ByteCountFormatter.CountStyle.decimal
        formatter.includesUnit = false
        return formatter.string(fromByteCount: bytes) as String
    }

    //MARK: Get String Value
    var totalDiskSpaceInGB: String {
        return ByteCountFormatter.string(fromByteCount: totalDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }

    var freeDiskSpaceInGB: String {
        return ByteCountFormatter.string(fromByteCount: freeDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }

    var usedDiskSpaceInGB: String {
        return ByteCountFormatter.string(fromByteCount: usedDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.decimal)
    }

    var totalDiskSpaceInMB: String {
        return MBFormatter(totalDiskSpaceInBytes)
    }

    var freeDiskSpaceInMB: String {
        return MBFormatter(freeDiskSpaceInBytes)
    }

    var usedDiskSpaceInMB: String {
        return MBFormatter(usedDiskSpaceInBytes)
    }

    //MARK: Get raw value
    var totalDiskSpaceInBytes: Int64 {
        guard let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
            let space = (systemAttributes[FileAttributeKey.systemSize] as? NSNumber)?.int64Value else { return 0 }
        return space
    }

    /*
     Total available capacity in bytes for "Important" resources, including space expected to be cleared by purging non-essential and cached resources. "Important" means something that the user or application clearly expects to be present on the local system, but is ultimately replaceable. This would include items that the user has explicitly requested via the UI, and resources that an application requires in order to provide functionality.
     Examples: A video that the user has explicitly requested to watch but has not yet finished watching or an audio file that the user has requested to download.
     This value should not be used in determining if there is room for an irreplaceable resource. In the case of irreplaceable resources, always attempt to save the resource regardless of available capacity and handle failure as gracefully as possible.
     */
    var freeDiskSpaceInBytes: Int64 {
        if #available(iOS 11.0, *) {
            if let space = try? URL(fileURLWithPath: NSHomeDirectory() as String).resourceValues(forKeys: [URLResourceKey.volumeAvailableCapacityForImportantUsageKey]).volumeAvailableCapacityForImportantUsage {
                return space
            } else {
                return 0
            }
        } else {
            if let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
                let freeSpace = (systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber)?.int64Value {
                return freeSpace
            } else {
                return 0
            }
        }
    }

    var usedDiskSpaceInBytes: Int64 {
        return totalDiskSpaceInBytes - freeDiskSpaceInBytes
    }

}
