//
//  ExtensionFonts.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 07/20/2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import UIKit

extension UIFont {
    static func regular(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: "SFUIText-Regular", size: size) {
            return font
        }
        else {
            return UIFont.systemFont(ofSize: size, weight: .regular)
        }
    }
    
    static func light(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: "SFUIText-Light", size: size) {
            return font
        }
        else {
            return UIFont.systemFont(ofSize: size, weight: .light)
        }
    }
    
    static func semibold(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: "SFUIText-Semibold", size: size) {
            return font
        }
        else {
            return UIFont.systemFont(ofSize: size, weight: .semibold)
        }
    }
    
    static func medium(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: "SFUIText-Medium", size: size) {
            return font
        }
        else {
            return UIFont.systemFont(ofSize: size, weight: .medium)
        }
    }
}
