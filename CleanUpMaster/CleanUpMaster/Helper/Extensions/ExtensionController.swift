//
//  ExtensionController.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 09/01/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Photos
import Contacts
import AVKit

extension UIViewController {
    func getPermissionPhotos(completionHandler: @escaping (Bool) -> Void) {
        // 1
        guard PHPhotoLibrary.authorizationStatus() != .authorized else {
            completionHandler(true)
            return
        }
        // 2
        PHPhotoLibrary.requestAuthorization { status in
            completionHandler(status == .authorized ? true : false)
        }
    }

    func getPermissionContact(completionHandler: @escaping (Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)

        switch authorizationStatus {
        case .authorized:
            completionHandler(true)

        case .notDetermined:
            CNContactStore().requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    completionHandler(false)
                }
            })

        default:
            completionHandler(false)
        }
    }

    func playVideo (asset: PHAsset) {
        PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil) { (asset, audiomix, info) in
            let asset = asset as! AVURLAsset

            DispatchQueue.main.async {
                let player = AVPlayer(url: asset.url)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
    }

    func playVideo (url: URL) {
        DispatchQueue.main.async {
            let player = AVPlayer(url: url)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    func createVideoThumbnail(from url: URL) -> UIImage? {

        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.maximumSize = self.view.frame.size

        let time = CMTimeMakeWithSeconds(0.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        }
        catch {
            print(error.localizedDescription)
            return nil
        }

    }
}

extension UIViewController {
    func showAlerClose(_ title: String, _ message: String?, closeHandler handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "kClose".localized(), style: .cancel, handler: handler)
        alert.addAction(closeAction)
        self.present(alert, animated: true, completion: nil)
    }

    func showAlerNotification(_ title: String? = "kNotification".localized(), message: String?, closeHandler handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "kClose".localized(), style: .cancel, handler: handler)
        alert.addAction(closeAction)
        self.present(alert, animated: true, completion: nil)
    }

    func showAlerYesNo(title: String, _ tileYes: String, _ titleNo: String, _ message: String?, yesHandler handlerYes: ((UIAlertAction) -> Void)?, noHandler handlerNo: ((UIAlertAction) -> Void)?, type: UIAlertController.Style = .alert) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: type)
        let yesAction = UIAlertAction(title: tileYes, style: .default, handler: handlerYes)
        let closeAction = UIAlertAction(title: titleNo, style: .default, handler: handlerNo)
        alert.addAction(closeAction)
        alert.addAction(yesAction)
        if type == .actionSheet {
            alert.addAction(UIAlertAction(title: "kCanncel".localized(), style: .cancel, handler: nil))
            if UIDevice.current.userInterfaceIdiom == .pad {
                alert.popoverPresentationController?.sourceView = view
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.width / 2, y: self.view.bounds.height - 100, width: 0, height: 0)
                alert.popoverPresentationController?.permittedArrowDirections = .down
            }
        }
        self.present(alert, animated: true, completion: nil)
    }
}
