//
//  Biometric.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 07/27/2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import Foundation
import LocalAuthentication
import MessageUI

enum BiometricType {
    case none
    case touchID
    case faceID
}

class BiometricIDAuth: NSObject {
    let context = LAContext()
    
    func biometricType() -> BiometricType {
        return self.context.biometricType
    }
    
    func biometricInstruction() -> String {
        switch self.biometricType() {
        case .none:
            return ""
        case .touchID:
            return "TouchID"
        case .faceID:
            return "FaceID"
        }
    }
    
    func canEvaluatePolicy() -> Bool {
        var error: NSError?
        if self.context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            return true
        } else {
            return false
        }
    }
    
    func isBiometryReady() -> Bool {
        return self.biometricType() != .none
    }
}

// MARK: Check biometrics settings

extension BiometricIDAuth {
    func isBiometricsSettingsChanged() -> Bool {
        self.context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        
        if let oldEvaluatedPolicy = UserDefaults.getOldEvaluatedPolicy,
            oldEvaluatedPolicy.count > 0, let domainState = context.evaluatedPolicyDomainState,
            oldEvaluatedPolicy != String(data: domainState.base64EncodedData(), encoding: .utf8) {
            return true
        }
        
        return false
    }
    
    func saveBiometricsState() {
        self.context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        if let domainState = self.context.evaluatedPolicyDomainState {
            UserDefaults.setOldEvaluatedPolicy(String(data: domainState.base64EncodedData(), encoding: .utf8))
        }
    }
    
    func isBiometryLocked() -> Bool {
        var error: NSError?
        if self.context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            return false
        }
        if error?.code == -8 {
            return true
        }
        
        return false
    }
}

extension LAContext {
    var biometricType: BiometricType {
        return self.biometricType(with: .deviceOwnerAuthenticationWithBiometrics)
    }
    
    func biometricType(with policy: LAPolicy) -> BiometricType {
        var error: NSError?
        guard self.canEvaluatePolicy(policy, error: &error) else {
            return .none
        }
        
        if #available(iOS 11.0, *) {
            switch self.biometryType {
            case .none:
                return .none
            case .touchID:
                return .touchID
            case .faceID:
                return .faceID
            @unknown default:
                fatalError()
            }
        } else {
            return .touchID
        }
    }
}
