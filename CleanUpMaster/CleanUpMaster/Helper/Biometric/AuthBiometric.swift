//
//  AuthBiometric.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 07/27/2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import LocalAuthentication
import UIKit

class AuthByBiometrics: NSObject {
    var fallBackTitle = ""
    var biometricsType = BiometricType.none
    var userDidCancel: (() -> Void)?
    func type() -> BiometricType {
        let context = LAContext()
        return context.biometricType
    }

    func typeWithName() -> String {
        let context = type()
        switch context {
        case .faceID:
            return "FaceID"
        case .touchID:
            return "TouchID"
        default:
            return ""
        }
    }

    func canUse() -> Bool {
        let context = LAContext()
        let type = context.biometricType(with: .deviceOwnerAuthenticationWithBiometrics)

        if type == .none {
            return false
        }
        else {
            self.biometricsType = type
            return true
        }
    }

    func auth(completion: @escaping (_ success: Bool, _ type: BiometricType) -> Void) {
        self.auth(type: self.biometricsType, completion: completion)
    }

    private func auth(type: BiometricType, completion: @escaping (_ success: Bool, _ type: BiometricType) -> Void) {
        let context = LAContext()
        context.localizedFallbackTitle = self.fallBackTitle
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Vui lòng di chuyển khuôn mặt hoặc quét vân tay để thực hiện mở khoá.") { success, evaluateError in
            DispatchQueue.main.async {
                if success {
                    if let domainState = context.evaluatedPolicyDomainState {
                        UserDefaults.setOldEvaluatedPolicy(String(data: domainState.base64EncodedData(), encoding: .utf8))
                    }
                    completion(true, type)
                }
                else {
                    if #available(iOS 11.0, *) {
                        if let error = evaluateError {
                            let errorCode = error._code
                            if errorCode == LAError.userCancel.rawValue {
                                self.userDidCancel?()
                                return
                            }
                            else if errorCode == LAError.systemCancel.rawValue {
                                completion(false, type)
                                return
                            }
                            else if errorCode == LAError.appCancel.rawValue {
                                completion(false, type)
                                return
                            }
                            else if errorCode == LAError.authenticationFailed.rawValue {
                                completion(false, type)
                                return
                            }
                            else {
                                completion(false, type)
                            }
                        }
                    }
                    completion(false, type)
                }
            }
        }
    }
}
