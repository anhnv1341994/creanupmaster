//
//  DashboardViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Photos
import SwiftyContacts
import UIKit
import ContactsUI

// https://www.raywenderlich.com/11764166-getting-started-with-photokit
class DashboardViewController: UIViewController {

    @IBOutlet weak var progressView: CircularProgressBar! {
        didSet {
            progressView.setProgress(to: 0, withAnimation: false)
            progressView.labelSize = 35
            progressView.safePercent = 100
            progressView.transform = CGAffineTransform(scaleX: 1.02, y: 1.02)
        }
    }
    @IBOutlet weak var btnRecybin: UIButton! {
        didSet {
            btnRecybin.setImage(UIImage(named: "trash")?.tintColor(.white), for: .normal)
        }
    }
    @IBOutlet weak var btnIntelligentCleaning: UIButton! {
        didSet {
            self.btnIntelligentCleaning.setTitle("kBtnIntelligentCleaning".localized(), for: .normal)
        }
    }
    @IBOutlet weak var viewAction: UIView! {
        didSet {
            self.viewAction.isUserInteractionEnabled = false
            self.viewAction.alpha = 0.5
        }
    }

    @IBOutlet weak var lbUsedStorage: UILabel! {
        didSet {
            lbUsedStorage.text = String(format: "kFreeStorage".localized(), UIDevice.current.freeDiskSpaceInGB)
        }
    }

    @IBOutlet weak var lbStorage: UILabel! {
        didSet {
            lbStorage.text = "\(String(format: "kUsedStorage".localized(), UIDevice.current.usedDiskSpaceInGB))\n\(String(format: "kTotalStorage".localized(), UIDevice.current.totalDiskSpaceInGB))"
        }
    }

    @IBOutlet weak var lbImageCleaning: UILabel! {
        didSet {
            lbImageCleaning.text = "lbImageCleaning".localized()
        }
    }
    @IBOutlet weak var lbOptimizeAddressBook: UILabel! {
        didSet {
            lbOptimizeAddressBook.text = "lbOptimizeAddressBook".localized()
        }
    }
    @IBOutlet weak var lbVideoCleaning: UILabel! {
        didSet {
            lbVideoCleaning.text = "lbVideoCleaning".localized()
        }
    }
    lazy var getContactQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "getAllContactsSmart"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    lazy var callAllApi: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "callAllApi"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    var contactGroupUnique: [ContactGroup] = []
    var contacts: [CNContact] = []


    private var recybinPhotos: [PHAsset] = []
    var securedView: SecuredView?

    var progress: Double = 0
    var timer: Timer?
    private var pointTop: CGRect = .zero
    private var pointBottom: CGRect = .zero
    private var scrollToTop: Bool = true

    /// scanner master
    var listImages: [PHAsset] = []
    var totalSizeListImage: Double = 0.0
    var totalSizeVideos: Double = 0.0
    var videos: [PHAsset] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        requestAccess { [weak self] _ in
            self?.getPermissionPhotos { _ in
                print("PermissionPhotos")
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let photos = UserDefaults.listRecybin ?? []
        self.btnRecybin.isHidden = photos.count == 0
//        NotificationCenter.default.addObserver(self, selector: #selector(self.firstCheckApp), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkFetchContact), name: NSNotification.Name.CNContactStoreDidChange, object: nil)
        if self.contacts.count == 0 {
            self.getAllContacts()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.CNContactStoreDidChange, object: nil)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if self.securedView == nil {
            self.securedView = SecuredView()
            guard let securedView = self.securedView else { return }
            self.view.addSubview(securedView)
            securedView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handPanGesture(_:))))
            self.pointTop = CGRect(x: 0, y: self.view.insetArea.top + 15, width: self.view.frame.width, height: self.view.frame.height - self.view.insetArea.top - 15)
            self.pointBottom = CGRect(x: 0, y: self.view.frame.height - 65, width: self.view.frame.width, height: self.view.frame.height - self.view.insetArea.top - 15)
            securedView.frame = self.pointBottom
            securedView.homeVC = self
        }
    }

    @objc func checkFetchContact() {
        print("checkFetchContact")
        self.getAllContacts()
    }

    func getAllContacts() {
        let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactMiddleNameKey, CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactViewController.descriptorForRequiredKeys()] as [Any]
        SwiftyContacts.fetchContacts(keysToFetch: keys as! [CNKeyDescriptor], order: .userDefault) { result in
            switch result {
            case .success(let contacts):
                self.contacts = contacts
            case .failure(let error):
                print(error)
            }
        }
    }

    @objc func handPanGesture(_ gesture: UIPanGestureRecognizer) {
        let fileView = gesture.view!
        let translate = gesture.translation(in: self.view)
        print("ended ", translate)

        if translate.y != 0 {
            scrollToTop = translate.y < 0
        }

        if self.securedView!.frame.minY + translate.y > self.pointTop.minY {

            switch gesture.state {
            case .began, .changed:
                print("changed")
                fileView.center = CGPoint(x: fileView.center.x, y: fileView.center.y + translate.y)
                gesture.setTranslation(.zero, in: self.view)
            case .ended:
                print("ended ", translate)
                UIView.animate(withDuration: 0.3, animations: {
                    if self.scrollToTop {
                        self.securedView?.frame = self.pointTop
                        self.firstCheckApp()
                    } else {
                        self.securedView?.frame = self.pointBottom
                    }
                })
            default:
                break
            }
        }
    }

    func actionShowViewSecured() {
        UIView.animate(withDuration: 0.3) {
            if self.securedView!.frame.minY == self.pointTop.minY {
                self.securedView?.frame = self.pointBottom
            } else {
                self.securedView?.frame = self.pointTop
                self.firstCheckApp()
            }
        }
    }

    @IBAction func actionSettings(_ sender: Any) {
        let settingsVC = SettingsViewController()
        self.pushViewWithScreen(settingsVC)
    }

    @IBAction func actionRecybin(_ sender: Any) {
        getPermissionPhotos { granted in
            guard granted else { return }
            self.fetchAssets()
        }
    }

    @IBAction func actionIntelligentCleaning(_ sender: Any) {
        self.updateStatusAction(false)
        progress = 0
        progressView.setProgress(to: progress, withAnimation: true)
        self.fetchAll()
    }

    func fetchAll() {
        self.checkDuplicateImage { [weak self] in
            guard let self = self else { return }
            print(self.listImages.count)

            self.progress = Double.random(min: 0.3, max: 0.5)
            self.progressView.setProgress(to: self.progress, withAnimation: true)

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.checkDuplicateContact { [weak self] in
                    guard let self = self else { return }
                    self.progress = Double.random(min: self.progress, max: self.progress + 0.3)

                    self.progressView.setProgress(to: self.progress, withAnimation: true)
                    print(self.contactGroupUnique.count)

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.checkDuplicateVideo { [weak self] in
                            guard let self = self else { return }
                            print(self.contactGroupUnique.count)
                            self.updateStatusAction(true)

                            self.timer = Timer.scheduledTimer(timeInterval: Double.random(min: 0.02, max: 0.04), target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
                        }
                    }
                }
            }
        }
    }

    @objc func updateProgress() {
        progressView.setProgress(to: progress, withAnimation: true)
        progress += 0.01
        if progress > 1.01 {
            self.timer?.invalidate()
            self.timer = nil
            let vc = CleanAllViewController()
            vc.totalImage = String(format: "%.2f", self.totalSizeListImage) + "MB"
            vc.totalVideo = String(format: "%.2f", self.totalSizeVideos) + "MB"
            vc.totalAddressBook = "\(self.contactGroupUnique.count)"
            vc.listImages = self.listImages
            vc.videos = self.videos
            vc.contactGroupUnique = self.contactGroupUnique
            vc.dashboardVC = self
            vc.didGotoPurchase = { [weak self] in
                self?.gotoPurchase()
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    ///Goto screen purchase
    func gotoPurchase() {
        let purchaseVC = InAppPurchaseViewController()
        let presenter = InAppPurchasePresenterView(purchaseVC)
        purchaseVC.inAppPurchasePresenterView = presenter
        purchaseVC.modalPresentationStyle = .fullScreen
        self.present(purchaseVC, animated: true, completion: nil)
        purchaseVC.gotoPolicyView = { [weak self] (_ type: WebType) in
            let vc = PrivacyPolicyViewController()
            vc.type = type
            self?.pushViewWithScreen(vc)
        }
    }

    private func updateStatusAction(_ status: Bool) {
        self.btnIntelligentCleaning.isUserInteractionEnabled = status
        self.btnIntelligentCleaning.alpha = status ? 1 : 0.5
        self.viewAction.isUserInteractionEnabled = status
        self.viewAction.alpha = status ? 1 : 0.5
    }

    @IBAction func actionImageCleaning(_ sender: Any) {
        let yourGalleryVC = YourGalleryViewController()
        self.pushViewWithScreen(yourGalleryVC)
    }

    @IBAction func actionOptimizeAddressBook(_ sender: Any) {
        let addressBookVC = AddressBookViewController()
        self.pushViewWithScreen(addressBookVC)
    }

    @IBAction func actionVideoCleaning(_ sender: Any) {
        let videoVC = VideosViewController()
        self.pushViewWithScreen(videoVC)
    }

    func fetchAssets() { // 1
        let allPhotosOptions = PHFetchOptions()
        allPhotosOptions.sortDescriptors = [
            NSSortDescriptor(
                key: "creationDate",
                ascending: false)
        ]
        let listRecybin = UserDefaults.listRecybin ?? []
        var allPhotos: [PHAsset] = []
        PHAsset.fetchAssets(with: allPhotosOptions).enumerateObjects { object, _, _ in
            if listRecybin.contains(object.localIdentifier) {
                allPhotos.append(object)
            }
        }

        let recybinVC = RecybinPhotosViewController()
        recybinVC.photos = allPhotos
        self.pushViewWithScreen(recybinVC)
    }

    @objc func firstCheckApp() {
        if let _ = UserDefaults.passcode, UserDefaults.isEnablePasscode == true {
            let vc = AppPinPadViewController()
            vc.pinPadType = .Open
            self.present(vc, animated: true, completion: nil)
        } else {
//            self.openCameraIfNeeded()
        }
    }
}

//MARK: - CHECK DUPLICATE FILE
extension DashboardViewController {
    func checkDuplicateImage(_ completion: @escaping(() -> Void)) {
        defer {
            completion()
        }
        self.totalSizeListImage = 0.0
        self.listImages.removeAll()
        let listRecybin = UserDefaults.listRecybin ?? []

        let blurPicture = PHAssetCollection.fetchAssetCollections(
            with: .smartAlbum,
            subtype: .smartAlbumDepthEffect,
            options: nil)
        PHAsset.fetchAssets(in: blurPicture[0], options: nil).enumerateObjects { object, _, _ in
            if !listRecybin.contains(object.localIdentifier), object.mediaType == .image {
                self.totalSizeListImage += object.fileSize
                self.listImages.append(object)
            }
        }

        let screenCapture = PHAssetCollection.fetchAssetCollections(
            with: .smartAlbum,
            subtype: .smartAlbumScreenshots,
            options: nil)
        PHAsset.fetchAssets(in: screenCapture[0], options: nil).enumerateObjects { object, _, _ in
            if !listRecybin.contains(object.localIdentifier), object.mediaType == .image {
                self.totalSizeListImage += object.fileSize
                self.listImages.append(object)
            }
        }
    }

    func checkDuplicateContact(_ completion: @escaping (() -> Void)) {
        let group = DispatchGroup()
        group.enter()
        self.fetchContactsRepeatFullname { [weak self] in
            guard let self = self else { return }

            self.contactGroupUnique = self.contactGroupUnique.sorted { ct1, ct2 -> Bool in
                ct1.ctMain.fullName ?? "" > ct2.ctMain.fullName ?? ""
            }
            group.leave()
        }

        group.notify(queue: .main, execute: completion)
    }

    func checkDuplicateVideo(_ completion: @escaping (() -> Void)) {
        self.totalSizeVideos = 0.0
        self.videos.removeAll()
        let listRecybin = UserDefaults.listRecybin ?? []

        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [
            NSSortDescriptor(
                key: "creationDate",
                ascending: false)
        ]

        fetchOptions.predicate = NSPredicate(format: "mediaType = %d ", PHAssetMediaType.video.rawValue)

        let group = DispatchGroup()

        let allVideo = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        allVideo.enumerateObjects { (asset, index, bool) in
            group.enter()
            if !listRecybin.contains(asset.localIdentifier) {
                self.totalSizeVideos += asset.fileSize
                self.videos.append(asset)
                group.leave()
            } else {
                group.leave()
            }
        }

        group.notify(queue: .main, execute: completion)
    }

    //MARK: - check repeat fullname contact
    private func fetchContactsRepeatFullname(_ completion: (() -> Void)?) {
        self.getContactQueue.addOperation {
            if self.contacts.count == 0 {
                self.getAllContacts()
            }

            let fullNames = self.contacts.map { CNContactFormatter.string(from: $0, style: .fullName) }
            let uniqueArray = Array(Set(fullNames.filter { name in fullNames.filter({ $0 == name }).count > 1 }))
            self.contactGroupUnique.removeAll()

            for fullName in uniqueArray {
                let group = self.contacts.filter {
                    CNContactFormatter.string(from: $0, style: .fullName) == fullName
                }
                if group.count > 1 {
                    let contactMain = group[0]
                    let duplicates = group.filter { $0.identifier != contactMain.identifier }.map { Contact(contact: $0) }
                    let ctUnique = ContactGroup(ctMain: contactMain, duplicates: duplicates)
                    self.contactGroupUnique.append(ctUnique)
                }
            }

            OperationQueue.main.addOperation {
                completion?()
            }
        }
    }
}
