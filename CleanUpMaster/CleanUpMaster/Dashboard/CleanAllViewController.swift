//
//  CleanAllViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 28/05/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Photos
import SwiftyContacts

class CleanAllViewController: UIViewController {

    @IBOutlet weak var lbImageCleaning: UILabel! {
        didSet {
            lbImageCleaning.text = "lbImageCleaning".localized()
        }
    }
    @IBOutlet weak var lbOptimizeAddressBook: UILabel! {
        didSet {
            lbOptimizeAddressBook.text = "lbOptimizeAddressBook".localized()
        }
    }
    @IBOutlet weak var lbVideoCleaning: UILabel! {
        didSet {
            lbVideoCleaning.text = "lbVideoCleaning".localized()
        }
    }
    
    @IBOutlet weak var btnClean: UIButton! {
        didSet {
            self.btnClean.setTitle("kCleanAll".localized(), for: .normal)
        }
    }
    
    @IBOutlet weak var lbImageCleaningValue: UILabel!
    @IBOutlet weak var lbOptimizeAddressBookValue: UILabel!
    @IBOutlet weak var lbVideoCleaningValue: UILabel!
    @IBOutlet weak var viewContainer: UIView!

    var totalImage: String?
    var totalAddressBook: String?
    var totalVideo: String?

    ///List scanner
    lazy var getContactQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "mergeAllContacts"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    let contactServices = ContactsManagerment()
    var listImages: [PHAsset] = []
    var videos: [PHAsset] = []
    var contactGroupUnique: [ContactGroup] = []
    var didGotoPurchase: (() -> Void)?
    /// end list scanner

    weak var dashboardVC: DashboardViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbImageCleaningValue.text = self.totalImage
        self.lbOptimizeAddressBookValue.text = self.totalAddressBook
        self.lbVideoCleaningValue.text = self.totalVideo
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.show(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hide(animated)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.commonInit()
    }

    func commonInit() {
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
    }

    @IBAction func actionDismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func actionCleaning(_ sender: Any) {
        if self.listImages.count != 0 || self.videos.count != 0 || self.contactGroupUnique.count != 0 {
            self.dismiss(animated: true) {
                if(UserDefaults.isPurchase ?? false) {
                    var totalAsset: [PHAsset] = []
                    totalAsset += self.listImages
                    totalAsset += self.videos
                    if totalAsset.count > 0 {
                        let assetIdentifiers = totalAsset.map({ $0.localIdentifier })
                        let assets = PHAsset.fetchAssets(withLocalIdentifiers: assetIdentifiers, options: nil)

                        PHPhotoLibrary.shared().performChanges({
                            PHAssetChangeRequest.deleteAssets(assets)
                        })
                    }
                    self.mergeContactsFullname()
                } else {
                    self.didGotoPurchase?()
                }
            }
        } else {
            dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func actionImageCleaning(_ sender: Any) {
        self.dismiss(animated: true) {
            self.dashboardVC?.actionImageCleaning(sender)
        }
    }

    @IBAction func actionOptimizeAddressBook(_ sender: Any) {
        self.dismiss(animated: true) {
            self.dashboardVC?.actionOptimizeAddressBook(sender)
        }
    }

    @IBAction func actionVideoCleaning(_ sender: Any) {
        self.dismiss(animated: true) {
            self.dashboardVC?.actionVideoCleaning(sender)
        }
    }

    // MARK: Animation
    func show(_ animated: Bool) {
        if !animated {
            return
        }

        guard let contentView = self.viewContainer else {
            return
        }

        self.view.layoutIfNeeded()
        contentView.transform = CGAffineTransform.init(translationX: 0.0, y: contentView.frame.maxY)

        UIView.animate(withDuration: 0.4) {
            contentView.transform = CGAffineTransform.identity
        }
    }

    func hide(_ animated: Bool) {
        if !animated {
            return
        }

        guard let contentView = self.viewContainer else {
            return
        }

        self.view.layoutIfNeeded()
        contentView.transform = CGAffineTransform.identity

        UIView.animate(withDuration: 0.4) {
            contentView.transform = CGAffineTransform.init(translationX: 0.0, y: contentView.frame.maxY)
        }
    }

    private func mergeContactsFullname() {
        let block = BlockOperation()
        block.addExecutionBlock {

            let contactGroupUnique = self.contactGroupUnique
            for contact in contactGroupUnique {
                var listDup: [CNContact] = []
                let contactDup = contact.duplicates
                listDup = contactDup.map({ $0.contact })
                listDup.insert(contact.ctMain, at: 0)

                let newContact = self.contactServices.mergeAllDuplicatesFullName(duplicates: listDup)

                addContact(Contact: newContact.mutableCopy() as! CNMutableContact) { result in
                    switch result {
                    case .success(let bool):
                        print("-----mergeAllDuplicatesFullName-----", newContact.fullName ?? "")
                        if bool {
                            for ct in listDup {
                                deleteContact(Contact: ct.mutableCopy() as! CNMutableContact) { result in
                                    switch result {
                                    case .success(let bool):
                                        if bool {
                                            print("isDelete ", ct.fullName ?? "")
                                        }
                                    case .failure(let error):
                                        print(error.localizedDescription)
                                        Loading.shared.stopLoading()
                                    }
                                }
                            }
                        }
                    case .failure(let error):
                        Loading.shared.stopLoading()
                        print(error.localizedDescription)
                    }
                }
            }
        }

        block.completionBlock = {
            self.dashboardVC?.getAllContacts()
        }

        self.getContactQueue.addOperation(block)
    }
}

extension PHAsset {
    var fileSize: Double {
        get {
            let resource = PHAssetResource.assetResources(for: self)
            let imageSizeByte = resource.first?.value(forKey: "fileSize") as? Double ?? 0
            let imageSizeMB = imageSizeByte / (1024.0 * 1024.0)
            print("Size \(imageSizeMB) MB")
            return imageSizeMB
        }
    }
}
