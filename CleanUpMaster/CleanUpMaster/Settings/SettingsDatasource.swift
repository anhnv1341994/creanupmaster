//
//  SettingsDatasource.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Foundation

struct SettingsDatasource {
    static func listData() -> [SettingsHeader] {
        let items1 = [
//            SettingsItem(image: "image1", title: "Photo & Video", isEnable: UserDefaults.isEnablePhotoVideo ?? false, type: .PhotoVideo),
            SettingsItem(image: "phone", title: "kSettingContacts".localized(), isEnable: UserDefaults.isEnableContacts ?? false, type: .Contact),
        ]
        
        var items2 = [
            SettingsItem(image: "question", title: "kSettingUsePasscode".localized(), isEnable: UserDefaults.isEnablePasscode ?? false, type: .Passcode),
            SettingsItem(image: "screenCapture", title: "kSettingFaceID".localized(), isEnable: UserDefaults.isEnableUseFaceID ?? false, type: .FaceId),
        ]

        if UserDefaults.isEnablePasscode == true {
            let item = SettingsItem(image: "question", title: "kSettingChangePass".localized(), isEnable: nil, type: .ChangePasscode)
            items2.insert(item, at: 1)
        }

        let items3 = [
//            SettingsItem(image: nil, title: "FAQ", isEnable: nil, type: .FAQ),
            SettingsItem(image: nil, title: "kSettingPolicy".localized(), isEnable: nil, type: .PrivacyPolicy),
            SettingsItem(image: nil, title: "kSettingTerm".localized(), isEnable: nil, type: .TermsOfUse),
//            SettingsItem(image: nil, title: "Contact Us", isEnable: nil, type: .ContactUs),
        ]

        var items4 = [
            SettingsItem(image: nil, title: "kSettingRestorePurchase".localized(), isEnable: nil, type: .RestorePurchase),
        ]
        
        if (UserDefaults.isPurchase ?? false) == false {
            items4.insert(SettingsItem(image: nil, title: "kPurchase".localized(), isEnable: nil, type: .PurchaseProduct), at: 0)
        }
        
        let appVersion = Bundle.mainAppVersion

        return [
            SettingsHeader(titleHeader: "kSettingRemoveAfterImport".localized(), note: "Automatically remove contacts from the address book, photos and videos from Photos after import to Your Space.", items: items1),
            SettingsHeader(titleHeader: "kSettingYourSpace".localized(), note: "Protect your persional data stored in Your Space", items: items2),
            SettingsHeader(titleHeader: nil, note: nil, items: items3),
            SettingsHeader(titleHeader: nil, note: nil, items: items4),
            SettingsHeader(titleHeader: "\("kSettingVersion".localized()) \(appVersion ?? "1.0")", note: nil, items: [])
        ]
    }
}

enum SettingsType {
    case PhotoVideo
    case Contact
    case Passcode
    case ChangePasscode
    case FaceId
    case FAQ
    case PrivacyPolicy
    case TermsOfUse
    case ContactUs
    case RestorePurchase
    case PurchaseProduct
}

struct SettingsHeader {
    var titleHeader: String?
    var note: String?
    var items: [SettingsItem]
}

struct SettingsItem {
    var image: String?
    var title: String?
    var isEnable: Bool?
    var type: SettingsType
}
