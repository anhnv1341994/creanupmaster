//
//  PrivacyPolicyViewController.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 2507//2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import UIKit
import WebKit

enum WebType {
    case term, policy
    var url: URL? {
        switch self {
        case .term:
            return URL(string: "https://nvananh.github.io/CleanStorage-cleanMaster-term.io/")
        case .policy:
            return URL(string: "https://nvananh.github.io/CleanStorage-cleanMaster-policy.github.io/")
        }
    }
    
    var navTitle: String {
        switch self {
        case .term:
            return "kSettingTerm".localized()
        case .policy:
            return "kSettingPolicy".localized()
        }
    }
}

class PrivacyPolicyViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var navTitle: UILabel!

    var webView: WKWebView!
    var type: WebType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navTitle.text = self.type?.navTitle
        setupWebKit()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    private func setupWebKit() {
        webView = WKWebView(frame: containerView.frame)
        containerView.addSubviewAndFill(webView)
        webView.backgroundColor = .clear
        requestWebKit()
    }

    private func requestWebKit() {
        webView.navigationDelegate = self as WKNavigationDelegate
        if let url = self.type?.url {
            let urlRequest = URLRequest(url: url)
            DispatchQueue.main.async {
                self.webView.load(urlRequest)
            }
        }
    }
    
    deinit {
        self.webView = nil
        print("->>>>>>>>>>>>>>>>>>>>>>>>>>>>> Deinit \(self.className)")
    }
}

extension PrivacyPolicyViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loading.stopAnimating()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loading.stopAnimating()
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        loading.stopAnimating()
    }
}
