//
//  SettingsTableViewCell.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageSettings: UIImageView!
    @IBOutlet weak var viewImageSettings: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var swSwitch: UISwitch!
    @IBOutlet weak var imageArrow: UIImageView!
    private var settingsItem: SettingsItem?
    var completionReloadData: (() -> Void)?
    weak var settingsVC: SettingsViewController?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func checkHiddenView(_ item: SettingsItem) {
        self.settingsItem = item
        viewImageSettings.isHidden = item.image == nil
        swSwitch.isHidden = item.isEnable == nil
        imageArrow.isHidden = item.isEnable != nil
    }

    func setDataForRow(item: SettingsItem) {
        self.checkHiddenView(item)
        self.lbTitle.text = item.title
        if let isEnable = item.isEnable {
            self.swSwitch.isOn = isEnable
        }
        if let image = item.image {
            self.imageSettings.image = UIImage(named: image)
        }

        if item.type == .FaceId {
            self.lbTitle.textColor = UserDefaults.isEnablePasscode == true ? .black : .lightGray
            self.swSwitch.isUserInteractionEnabled = UserDefaults.isEnablePasscode == true
        } else {
            self.lbTitle.textColor = .black
        }
    }

    @IBAction func actionSwitch(_ sender: UISwitch) {
        guard let item = self.settingsItem else { return }
        let isOn = sender.isOn
        switch item.type {
        case .PhotoVideo:
            if isOn {
                self.settingsVC?.getPermissionPhotos(completionHandler: { granted in
                    guard granted else { return }
                    UserDefaults.isEnablePhotoVideo = isOn
                })
            } else {
                UserDefaults.isEnablePhotoVideo = isOn
            }
        case .Contact:
            if isOn {
                self.settingsVC?.getPermissionContact(completionHandler: { granted in
                    guard granted else { return }
                    UserDefaults.isEnableContacts = isOn
                })
            } else {
                UserDefaults.isEnableContacts = isOn
            }
        case .Passcode:
            self.settingsVC?.setupPasscode()
        case .FaceId:
            UserDefaults.isEnableUseFaceID = isOn
        default: break
        }
        self.completionReloadData?()
    }
}
