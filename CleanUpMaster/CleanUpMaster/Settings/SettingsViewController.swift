//
//  SettingsViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "kSettings".localized()
        }
    }

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.rowHeight = 50
            tableView.estimatedRowHeight = 50
            tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
            tableView.register(cellType: SettingsTableViewCell.self)
        }
    }

    private var settings = SettingsDatasource.listData()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func setupPasscode() {
        let vc = AppPinPadViewController()
        vc.pinPadType = (UserDefaults.isEnablePasscode ?? false) ? PinPadType.TurnOff : PinPadType.Create
        vc.appPinPadAction = { [weak self] success in
            guard let self = self else { return }
            if success {
                UserDefaults.isEnablePasscode = !(UserDefaults.isEnablePasscode ?? false)
                if false == (UserDefaults.isEnablePasscode ?? false) {
                    UserDefaults.isEnableUseFaceID = false
                }
                self.settings = SettingsDatasource.listData()
            }
            self.tableView.reloadData()
        }
        self.present(vc, animated: true, completion: nil)
    }

    private func setupChangedPasscode() {
        let vc = AppPinPadViewController()
        vc.pinPadType = .Change
        self.present(vc, animated: true, completion: nil)
    }

    private func restorePurchase() {
        Loading.shared.startLoading()
        IPAService.shared.restorePurchases { [weak self] isPurchase in
            guard let self = self else { return }
            Loading.shared.stopLoading()
            if isPurchase {
                print("isPurchase")
                UserDefaults.isPurchase = true
                
                let msgPurchaseFaild = "kNotificationRestorePurchaseSuccess".localized()
                let alert = AlertHelper.create(title: "kNotificationRestorePurchase".localized(), message: msgPurchaseFaild, buttonTitle: "kAlertOK".localized())
                self.present(alert, animated: true, completion: nil)

                self.tableView.reloadData()
            } else {
                print("False isPurchase")
                let msgPurchaseFaild = "kNotificationRestorePurchaseFaild".localized()
                let alert = AlertHelper.create(title: "kNotificationRestorePurchase".localized(), message: msgPurchaseFaild, buttonTitle: "kAlertOK".localized())
                self.present(alert, animated: true, completion: nil)
            }
        }
        print("actionRestore")
    }
    
    func gotoPurchase() {
        let purchaseVC = InAppPurchaseViewController()
        let presenter = InAppPurchasePresenterView(purchaseVC)
        purchaseVC.inAppPurchasePresenterView = presenter
        purchaseVC.modalPresentationStyle = .fullScreen
        guard let window = UIApplication.shared.windows.first?.rootViewController else { return }
        purchaseVC.completionPurchase = {[weak self] in
            self?.settings = SettingsDatasource.listData()
            self?.tableView.reloadData()
        }
        purchaseVC.gotoPolicyView = { [weak self] type in
            let vc = PrivacyPolicyViewController()
            vc.type = type
            self?.pushViewWithScreen(vc)
        }

        window.present(purchaseVC, animated: true, completion: nil)
    }

    deinit {
        print(String(describing: self.classForCoder))
    }
}

extension SettingsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.settings.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.settings[section].items.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = SettingsHeaderView()
        let settingSection = self.settings[section]
        header.lbTitleHeader.text = settingSection.titleHeader
        if section == (self.settings.count - 1) {
            header.lbTitleHeader.textAlignment = .center
        } else {
            header.lbTitleHeader.textAlignment = .left
        }
        return header
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == -1 {
            return CGFloat.leastNormalMagnitude
        }
        let section = self.settings[section]
        if section.titleHeader != nil {
            return 40
        }
        return 30
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.settings[indexPath.section].items[indexPath.item]
        let cell = tableView.dequeueReusableCell(with: SettingsTableViewCell.self, for: indexPath)
        cell.setDataForRow(item: item)
        cell.settingsVC = self

        cell.completionReloadData = { [weak self] in
            guard let self = self else { return }
            self.settings = SettingsDatasource.listData()
            self.tableView.reloadData()
        }
        return cell
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.settings[indexPath.section].items[indexPath.item]
        switch item.type {
        case .ChangePasscode:
            self.setupChangedPasscode()
        case .TermsOfUse, .PrivacyPolicy:
            let vc = PrivacyPolicyViewController()
            vc.type = (item.type == .TermsOfUse) ? .term : .policy
            self.pushViewWithScreen(vc)
        case .RestorePurchase:
            self.restorePurchase()
        case .PurchaseProduct:
            self.gotoPurchase()
        default:
            break
        }
    }
}
