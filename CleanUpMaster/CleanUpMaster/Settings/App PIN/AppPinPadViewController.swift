//
//  AppPinPadViewController.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 07/23/2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import UIKit

enum PinPadType {
    case Create, TurnOff, Change, Open
}

class AppPinPadViewController: UIViewController {
    var appPinPadAction: ((_ isOn: Bool) -> Void)?
    @IBOutlet weak var pincodeView: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var lbCancel: UILabel! {
        didSet {
            lbCancel.text = "kHomeCancel".localized()
        }
    }
    var pinPadType: PinPadType = .Create
    var pinCode: String = "" {
        didSet {
            lbCancel.text = pinCode.count > 0 ? "kDelete".localized() : "kHomeCancel".localized()
        }
    }

    let pinCodeInputView: PinCodeInputView<PasswordItemView> = .init(
        digit: 4,
        itemSpacing: 15,
        itemFactory: {
            PasswordItemView()
        }
    )

    var pinCode1: String = ""
    var pinCode2: String = ""
    var step: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        switch pinPadType {
        case .Create:
            lbTitle.text = "kCreatePin".localized()
        case .TurnOff:
            lbTitle.text = "kEnterPinToOff".localized()
        case .Change:
            lbTitle.text = "kEnterOldPin".localized()
        case .Open:
            lbTitle.text = "kEnterPin".localized()
        }
        
        if self.pinPadType == .Open {
            self.viewCancel.alpha = 0
            
            if let isEnable = UserDefaults.isEnableUseFaceID, isEnable {
                let biometricAuth = AuthByBiometrics()
                biometricAuth.auth { [weak self] success, _ in
                    if success {
                        print("ok")
                        self?.dismisView(true)
                    } else {
                        print("faild")
                    }
                }
            }
        }
        
        
        setupBackButton()

        pincodeView.addSubview(pinCodeInputView)
        view.addTapGesture {
            self.pinCodeInputView.resignFirstResponder()
        }
        pinCodeInputView.frame = pincodeView.bounds

        pinCodeInputView.set(changeTextHandler: { text in
            self.pinCode = text
            if text.count == 4 {
                if self.pinPadType == .Change {
                    if let pinCode = UserDefaults.passcode, pinCode == self.pinCode, self.step == 0 {
                        print("old code is same")
                        self.step = 1
                        self.resetPinCode("kCreateNewPin".localized())
                    } else {
                        if self.step == 1 {
                            self.step = 2
                            self.pinCode1 = text
                            self.resetPinCode("kConfirmPin".localized())
                        } else if self.step == 2 {
                            self.pinCode2 = text
                            if self.pinCode1 == self.pinCode2, self.pinCode1.count == 4, self.pinCode2.count == 4 {
                                UserDefaults.passcode = text
                                self.dismisView(true)
                            }else {
                                self.pincodeView.animationShake()
                            }
                        } else {
                            self.pincodeView.animationShake()
                            self.resetPinCode()
                        }
                    }
                } else if self.pinPadType == .TurnOff {
                    if let pinCode = UserDefaults.passcode, pinCode == text {
                        UserDefaults.passcode = nil
                        self.dismisView(true)
                    } else {
                        self.pincodeView.animationShake()
                        self.resetPinCode()
                        print("pin code off error")
                    }
                } else if self.pinPadType == .Open {
                    if let pinCode = UserDefaults.passcode, pinCode == text {
                        print("pin code open app")
                        self.dismisView(true)
                    } else {
                        self.pincodeView.animationShake()
                        self.resetPinCode()
                        print("pin code error")
                    }
                } else { // create pin code
                    if self.pinCode1.count == 0 {
                        self.pinCode1 = text
                        self.resetPinCode("kConfirmPin".localized())
                    } else {
                        if self.pinCode1 == text {
                            UserDefaults.passcode = text
                            self.dismisView(true)
                        } else {
                            self.pincodeView.animationShake()
                            self.resetPinCode()
                        }
                    }
                }
            }else if self.pinPadType == .Open {
                self.viewCancel.alpha = text != "" ? 1 : 0
            }
            print(text)
            })
        pinCodeInputView.set(
            appearance: .init(
                itemSize: CGSize(width: 18, height: 16),
                font: .semibold(28),
                textColor: .white,
                backgroundColor: UIColor.white.withAlphaComponent(0),
                cursorColor: .color1,
                cornerRadius: 8,
                borderColor: .color1
            )
        )

        NotificationCenter
            .default
            .addObserver(
                self,
                selector: #selector(didBecameActive),
                name: UIApplication.didBecomeActiveNotification,
                object: nil
            )
    }
    func setupBackButton() {
        let button = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.backAction))
        self.navigationItem.leftBarButtonItem = button
        self.navigationItem.hidesBackButton = true
    }
    
    @objc func backAction() {
        self.popViewController()
    }
    
    private func resetPinCode(_ title: String? = nil) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let title = title {
                self.lbTitle.text = title
            }
            self.pinCode = ""
            self.pinCodeInputView.reset()
        }
    }

    @objc func didBecameActive() {
        if let string = UIPasteboard.general.string {
            pinCodeInputView.set(text: string)
        }
    }

    @IBAction func cancelAction(_ sender: Any) {
        dismisView(false)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .coverVertical
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func dismisView(_ isOn: Bool) {
        dismiss(animated: true, completion: {
            self.appPinPadAction?(isOn)
        })
    }

    @IBAction func actionNumberPad(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 1, 2, 3, 4, 5, 6, 7, 8, 9, 0:
            pinCodeInputView.insertText(tag.toString)
        case 10:
            if pinCode.count == 0 {
                dismisView(false)
            } else {
                pinCodeInputView.deleteBackward()
            }
        default:
            break
        }
    }
    
    deinit {
        print("->>>>>>>>>>>>>>>>>>>>>>>>>>>>> Deinit \(self.className)")
    }
}
