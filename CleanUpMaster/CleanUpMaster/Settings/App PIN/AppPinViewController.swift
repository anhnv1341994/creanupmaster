//
//  AppPinViewController.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 07/23/2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import UIKit

class AppPinViewController: BaseViewController {
    @IBOutlet var tableView: UITableView!
    var status: String?
    var completion: ((_ status: String?) -> Void)?
    var list_app_pin: [SettingsHeader] = []
    let kOn = "kOn".localized()
    let kOff = "kOff".localized()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "kSettingsAppPin".localized()
        setupBackButton()
        setupTableView()
        usePinIsOn(isOn: status ?? "" == self.kOn)
    }

    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: SettingsTableViewCell.self)
        tableView.tableFooterView = UIView()
    }

    private func usePinIsOn(isOn: Bool) {
        let biometricAuth = AuthByBiometrics()

        list_app_pin = [
            SettingsHeader(name: "", items: [
                SettingsItem(name: "kUsePin".localized(), isEnableSwitch: true, type: .AppPin)
            ]),
            SettingsHeader(name: "kSettingsAppPinNote".localized())
        ]
        if biometricAuth.canUse() {
            list_app_pin.append(
                SettingsHeader(name: "", items: [
                    SettingsItem(name: biometricAuth.typeWithName(), isEnableSwitch: true, type: .TouchID)
                ])
            )
        }
        if isOn {
            list_app_pin.append(
                SettingsHeader(name: "", items: [
                    SettingsItem(name: "kChangePin".localized(), type: .ChangePIN)
                ])
            )
        }
        tableView.reloadData()
    }
    
    deinit {
        print("->>>>>>>>>>>>>>>>>>>>>>>>>>>>> Deinit \(self.className)")
    }
}

extension AppPinViewController: UITableViewDelegate, UITableViewDataSource {
    func heightHeader(_ section: Int) -> CGFloat {
        if section == 1 {
            return 24
        } else if section == 2 {
            return 12
        } else {
            return 30
        }
    }

    func heightCell(_ indexPath: IndexPath) -> CGFloat {
        return SettingsHelper.heightCellSettings
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return list_app_pin.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return heightHeader(section)
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return heightHeader(section)
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightCell(indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightCell(indexPath)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = Bundle.main.loadNibNamed("SettingsHeaderCell", owner: self, options: nil)?.first as! SettingsHeaderCell
        header.lbName.text = list_app_pin[section].name
        header.lbName.textAlignment = .left
        header.lbName.textColor = .lightGray
        return header
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list_app_pin[section].items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: SettingsTableViewCell.self, for: indexPath)
        let item = list_app_pin[indexPath.section].items[indexPath.item]
        cell.setupData(item)
        cell.selectionStyle = .none
        if item.type! == .ChangePIN {
            cell.lbName.textColor = .textColorBlue
        }
        if item.type! == .TouchID {
            cell.swSwitch.isOn = UserDefault.getTouchID ?? false
        } else {
            cell.swSwitch.isOn = UserDefault.getAppPin ?? AppPinStatus.Off.rawValue == AppPinStatus.On.rawValue
        }

        cell.switchComplete = {[weak self] isOn in
            guard let self = self else { return }
            if item.type! == .AppPin {
                let vc = AppPinPadViewController()
                vc.pinPadType = isOn ? PinPadType.Create : PinPadType.TurnOff
                vc.appPinPadAction = {[weak self] success in
                    guard let self = self else { return }
                    if success {
                        cell.swSwitch.isOn = isOn
                        UserDefault.setAppPin(cell.swSwitch.isOn ? AppPinStatus.On.rawValue : AppPinStatus.Off.rawValue)
                        self.usePinIsOn(isOn: isOn)
                        self.completion?(cell.swSwitch.isOn ? self.kOn : self.kOff)
                    } else {
                        cell.swSwitch.isOn = !isOn
                        self.usePinIsOn(isOn: !isOn)
                    }
                }
                self.present(vc, animated: true, completion: nil)
            } else {
                cell.swSwitch.isOn = isOn
                UserDefault.setTouchID(isOn)
                print(isOn)
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = list_app_pin[indexPath.section].items[indexPath.item]
        if item.type! == .ChangePIN {
            let vc = AppPinPadViewController()
            vc.pinPadType = PinPadType.Change
            present(vc, animated: true, completion: nil)
        }
    }
}
