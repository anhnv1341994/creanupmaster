//
//  DetailsAllPhotosViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/11/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Photos
import UIKit

class DetailsAllPhotosViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    private let btnDelete = ButtonDeleteView()
    @IBOutlet weak var roundedView: RoundedView!

    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var btnRightNav: UIButton!
    fileprivate var checkSelected: [Bool] = []

    private var isSelectAll = true {
        didSet {
            self.btnRightNav.setTitle(isSelectAll ? "kDeselectAll".localized() : "kSelectAll".localized(), for: .normal)
        }
    }

    private var countSelect: Int = 0 {
        didSet {
            self.navTitle.text = "\(countSelect > 0 ? "\(countSelect) " : "")\("kPhoto".localized())\((countSelect > 1) ? "kS".localized() : "") \("kSelected".localized())"

            self.isSelectAll = (countSelect == photos.count && countSelect != 0)

            self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: countSelect > 0)
            self.showButton(count: self.photos.count)
        }
    }
    
    @IBOutlet weak var lbNoData: UILabel! {
        didSet {
            lbNoData.text = "kNoPhotos".localized()
        }
    }

    var photos: [PHAsset] = []
    var type: PhotosType = .all

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupButtonDelete()
        self.setupCollectionView()
        self.fetchAssets()
        self.reloadData()
        PHPhotoLibrary.shared().register(self)
    }

    @IBAction func actionSelected(_ sender: Any) {
        self.isSelectAll = !self.isSelectAll
        self.checkSelected.removeAll()
        for _ in 0 ..< photos.count {
            self.checkSelected.append(isSelectAll)
        }
        self.countSelect = isSelectAll ? photos.count : 0

        self.reloadData()
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self

        if let flowLayout = self.collectionView.collectionViewLayout as? GridCollectionViewLayout {
            flowLayout.scrollDirection = .vertical
            flowLayout.sectionInset = UIEdgeInsets.init(top: -1, left: 1, bottom: 0, right: 1)
            flowLayout.interitemSpacing = 1
            flowLayout.lineSpacing = 1
            flowLayout.aspectRatio = 1
            flowLayout.headerReferenceLength = 1.0
            flowLayout.numberOfItemsPerLine = 4
        }
        collectionView.register(cellType: PhotoCollectionViewCell.self)
    }

    fileprivate func reloadData() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    private func fetchAssets() { // 1
        self.countSelect = 0
        for _ in 0 ..< photos.count {
            self.checkSelected.append(false)
            print("checkSelected")
        }
        self.reloadData()
    }

    private func setupButtonDelete() {
        self.btnDelete.btnDelete.setTitle("kDeleteSelected".localized(), for: .normal)
        self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: false, alpha: 0)
        self.btnDelete.gotoPolicyView = { [weak self] type in
            let vc = PrivacyPolicyViewController()
            vc.type = type
            self?.pushViewWithScreen(vc)
        }

        self.btnDelete.didDelete = { [weak self] in
            guard let self = self else { return }
            var listRecybin: [String] = []
            if let currentRecybin = UserDefaults.listRecybin {
                listRecybin = currentRecybin
            }
            let group = DispatchGroup()
            var photosDraft: [PHAsset] = []
            var checkSelectedDraft: [Bool] = []

            for (index, isCheck) in self.checkSelected.enumerated() {
                group.enter()

                if isCheck {
                    listRecybin.append(self.photos[index].localIdentifier)
                    group.leave()
                } else {
                    checkSelectedDraft.append(self.checkSelected[index])
                    photosDraft.append(self.photos[index])
                    group.leave()
                }
            }

            group.notify(queue: .main) {
                self.checkSelected = checkSelectedDraft
                self.photos = photosDraft
                self.countSelect = self.checkSelected.filter { $0 == true }.count
                UserDefaults.listRecybin = listRecybin
                self.reloadData()
            }
        }
    }
}

extension DetailsAllPhotosViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PhotoCollectionViewCell.self, for: indexPath)
        let photo = self.photos[indexPath.item]
        cell.photoView.fetchImageAsset(photo, targetSize: cell.bounds.size) { _ in
//          cell.blurView.isHidden = !success
        }
        let check = self.checkSelected[indexPath.item]
        cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        cell.blurView.isHidden = !check

        cell.selectedPhoto = { [weak self] in
            guard let self = self else { return }

            self.checkSelected[indexPath.item] = !self.checkSelected[indexPath.item]
            let check = self.checkSelected[indexPath.item]
            cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
            self.countSelect = self.checkSelected.filter { $0 == true }.count
            self.isSelectAll = !self.checkSelected.contains(false)
        }
        return cell
    }
}

extension DetailsAllPhotosViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let vc = PreviewPhotoViewController()
        vc.indexSelected = indexPath.item
        vc.photos = self.photos
        vc.checkSelected = self.checkSelected
        vc.delegate = self
        self.pushViewWithScreen(vc)
    }
}

extension DetailsAllPhotosViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
//        DispatchQueue.main.sync {
//            if let changeDetails = changeInstance.changeDetails(for: photos) {
//                photos = changeDetails.fetchResultAfterChanges
//            }
//            self.collectionView.reloadData()
//        }
    }
}

extension DetailsAllPhotosViewController: PreviewPhotosDelegate {
    func didSelectedImage(index: Int) {
        self.checkSelected[index] = !self.checkSelected[index]
        self.countSelect = self.checkSelected.filter { $0 == true }.count
        self.isSelectAll = !self.checkSelected.contains(false)
        self.reloadData()
    }

    func showButton(count: Int) {
        if self.collectionView != nil, self.btnRightNav != nil {
            self.collectionView.isHidden = count == 0
            self.btnRightNav.alpha = count == 0 ? 0 : 1
            self.btnRightNav.isEnabled = count == 0 ? false : true
        }
    }
}
