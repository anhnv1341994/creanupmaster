//
//  PreviewPhotoViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/11/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Photos
import UIKit
import AVKit
import DTPhotoViewerController

class PreviewPhotoViewController: UIViewController {
    @IBOutlet weak var collectionView: FSPagerView! {
        didSet {
            collectionView.isHidden = true
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        }
    }

    @IBOutlet weak var collectionViewBottom: CollectionViewCustom!
    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    @IBOutlet weak var navTitle: UILabel!
    let marginOrigin = (UIApplication.shared.windows.first?.frame.width ?? 1.0) / 2 - 15
    var checkSelected: [Bool] = []
    var photos: [PHAsset] = []
    var photosURL: [URL] = []

    var countSelect: Int = 0 {
        didSet {
            let photo = "kPhoto".localized()
            let selected = "kSelected".localized()
            let ks = "kS".localized()
            let of = "kOf".localized()
            if countSelect > 0 {
                self.navTitle.text = "\(countSelect) \(photo)\((countSelect > 1) ? "\(ks)" : "") \(selected)"
            } else {
                self.navTitle.text = "\(indexSelected + 1) \(of) \(self.photos.count > 0 ? photos.count : photosURL.count)"
            }
        }
    }

    var indexSelected: Int = 0 {
        didSet {
            if countSelect == 0, self.navTitle != nil {
                self.navTitle.text = "\(indexSelected + 1) of \(self.photos.count > 0 ? photos.count : photosURL.count)"
            }
        }
    }
    weak var delegate: PreviewPhotosDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.countSelect = self.getCountSelected()
        self.collectionViewBottom.previewVC = self
        self.collectionViewBottom.setupData(photos: self.photos, photosURL: self.photosURL, checkSelected: self.checkSelected)
        self.collectionViewBottom.didSelect = { [weak self] index in
            guard let self = self else { return }
            self.indexSelected = index
            self.collectionView.scrollToItem(at: self.indexSelected, animated: true)
        }
        self.defauSetupPagerView()
        self.reloadData()
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.collectionView.scrollToItem(at: self.indexSelected, animated: false)
            self.indicator.stopAnimating()
            self.collectionView.isHidden = false
        }
    }

    func defauSetupPagerView() {
        self.collectionView.scrollDirection = .horizontal
        let spacing: CGFloat = 0.0
        self.collectionView.itemSize = self.roundedView.frame.size
        self.collectionView.interitemSpacing = spacing
        self.collectionView.transformer = FSPagerViewTransformer(type: .linear)
        //        self.pagerView.decelerationDistance = FSPagerView.automaticDistance
    }

    private func reloadData() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

    private func getCountSelected() -> Int {
        return self.checkSelected.filter { $0 }.count
    }

    deinit {
        print(String(describing: self.classForCoder))
    }
}

extension PreviewPhotoViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count > 0 ? self.photos.count : self.photosURL.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PhotoCollectionViewCell.self, for: indexPath)
        if self.photos.count > 0 {
            let photo = self.photos[indexPath.item]
            cell.photoView.kf.indicatorType = .activity
            photo.getURL { (url) in
                cell.photoView.kf.setImage(with: url) { _ in
                    cell.photoView.kf.indicatorType = .none
                }
                
                cell.showImagePlay(isShow: !(url?.absoluteString.isImageType() ?? false))
            }
        } else {
            let photo = self.photosURL[indexPath.item]
            cell.photoView.kf.indicatorType = .activity
            cell.photoView.kf.setImage(with: photo.absoluteString.isImageType() ? photo : nil, placeholder: self.createVideoThumbnail(from: photo)) { _ in
                cell.photoView.kf.indicatorType = .none
            }
            
            cell.showImagePlay(isShow: !photo.absoluteString.isImageType())
        }
        
        let check = self.checkSelected[indexPath.item]
        cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        cell.blurView.isHidden = !check
        cell.contraintTop.constant = 15
        cell.contraintRight.constant = 15
        cell.selectedPhoto = { [weak self] in
            guard let self = self else { return }

            self.checkSelected[indexPath.item] = !self.checkSelected[indexPath.item]
            self.collectionViewBottom.checkSelected[indexPath.item] = !self.collectionViewBottom.checkSelected[indexPath.item]
            self.collectionViewBottom.reloadData()
            let check = self.checkSelected[indexPath.item]
            self.countSelect = self.getCountSelected()
            self.delegate?.didSelectedImage(index: indexPath.item)
            cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        }
        return cell
    }
}

extension PreviewPhotoViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) { }
}

extension PreviewPhotoViewController: FSPagerViewDataSource, FSPagerViewDelegate {
    // MARK:- FSPagerView DataSource
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.photos.count > 0 ? self.photos.count : self.photosURL.count
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! PhotoCollectionViewCell
        if self.photos.count > 0 {
            let photo = self.photos[index]
            cell.photoView.kf.indicatorType = .activity
            cell.photoView.fetchImageAsset(photo, targetSize: cell.bounds.size) { _ in
                cell.photoView.kf.indicatorType = .none
            }
            photo.getURL { (url) in
                cell.showImagePlay(isShow: !(url?.absoluteString.isImageType() ?? false))
            }

        } else {
            let photo = self.photosURL[index]
            cell.photoView.kf.indicatorType = .activity
            cell.photoView.kf.setImage(with: photo, placeholder: self.createVideoThumbnail(from: photo)) { _ in
                cell.photoView.kf.indicatorType = .none
            }
            cell.showImagePlay(isShow: !photo.absoluteString.isImageType())
        }
        let check = self.checkSelected[index]
        cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        cell.blurView.isHidden = !check
        cell.contraintTop.constant = 90
        cell.contraintRight.constant = 30
        cell.selectedPhoto = { [weak self] in
            guard let self = self else { return }

            self.checkSelected[index] = !self.checkSelected[index]
            self.collectionViewBottom.checkSelected[index] = !self.collectionViewBottom.checkSelected[index]
            self.collectionViewBottom.reloadData()
            let check = self.checkSelected[index]
            self.countSelect = self.getCountSelected()
            self.delegate?.didSelectedImage(index: index)
            cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        }
        return cell
    }

    // MARK:- FSPagerView Delegate
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)

        if self.photos.count > 0 {
            let photo = self.photos[index]

            if photo.mediaType == .image {
                if let cell = pagerView.cellForItem(at: index) as? PhotoCollectionViewCell {
                    let viewController = SimplePhotoViewerController(referencedView: cell.photoView, image: cell.photoView.image)
                    viewController.dataSource = self
                    viewController.delegate = self
                    present(viewController, animated: true, completion: nil)
                }
            } else if photo.mediaType == .video {
                self.playVideo(asset: photo)
            }

        } else {
            let photo = self.photosURL[index]

            if photo.absoluteString.isImageType() {
                if let cell = pagerView.cellForItem(at: index) as? PhotoCollectionViewCell {
                    let viewController = SimplePhotoViewerController(referencedView: cell.photoView, image: cell.photoView.image)
                    viewController.dataSource = self
                    viewController.delegate = self
                    present(viewController, animated: true, completion: nil)
                }
            } else {
                self.playVideo(url: photo)
            }
        }
    }

    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.indexSelected = targetIndex
    }

    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {

    }
}

//MARK: DTPhotoViewerControllerDataSource
extension PreviewPhotoViewController: DTPhotoViewerControllerDataSource {
    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configureCell cell: DTPhotoCollectionViewCell, forPhotoAt index: Int) {
        // Set text for each item
        if let cell = cell as? CustomPhotoCollectionViewCell {
            cell.extraLabel.text = "\(index + 1)"
        }
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, referencedViewForPhotoAt index: Int) -> UIView? {
        if let cell = collectionView?.cellForItem(at: index) as? PhotoCollectionViewCell {
            return cell.imageView
        }

        return nil
    }

    func numberOfItems(in photoViewerController: DTPhotoViewerController) -> Int {
        return self.photos.count > 0 ? self.photos.count : self.photosURL.count
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, configurePhotoAt index: Int, withImageView imageView: UIImageView) {
        if self.photos.count > 0 {
            let photo = self.photos[index]
            imageView.kf.indicatorType = .activity
            imageView.fetchImageAsset(photo, targetSize: view.bounds.size) { _ in
                imageView.kf.indicatorType = .none
            }
        } else {
            let photo = self.photosURL[index]
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: photo) { _ in
                imageView.kf.indicatorType = .none
            }
        }
    }
}

//MARK: DTPhotoViewerControllerDelegate
extension PreviewPhotoViewController: SimplePhotoViewerControllerDelegate {
    func photoViewerControllerDidEndPresentingAnimation(_ photoViewerController: DTPhotoViewerController) {
        photoViewerController.scrollToPhoto(at: indexSelected, animated: false)
    }

    func photoViewerController(_ photoViewerController: DTPhotoViewerController, didScrollToPhotoAt index: Int) {
        indexSelected = index
        if let collectionView = collectionView {
            let indexPath = IndexPath(item: indexSelected, section: 0)

            // If cell for selected index path is not visible
            if !collectionView.collectionView.indexPathsForVisibleItems.contains(indexPath) {
                // Scroll to make cell visible
                self.collectionView.scrollToItem(at: index, animated: false)
            }
        }
    }

    func simplePhotoViewerController(_ viewController: SimplePhotoViewerController, savePhotoAt index: Int) {
//        UIImageWriteToSavedPhotosAlbum(UIImage(data: try! Data(contentsOf: URL(string: self.list_slide[index].image!)!))!, nil, nil, nil)
    }
}

protocol PreviewPhotosDelegate: class {
    func didSelectedImage(index: Int)
    func showButton(count: Int)
}
