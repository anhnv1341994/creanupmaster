//
//  SettingsHeaderView.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Photos
import UIKit

class CollectionViewCustom: CustomViewXib {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var photos: [PHAsset] = []
    var photosURL: [URL] = []
    var checkSelected: [Bool] = []
    
    func setupData(photos: [PHAsset], photosURL: [URL], checkSelected: [Bool]) {
        self.photos = photos
        self.photosURL = photosURL
        self.checkSelected = checkSelected
        self.setupCollectionView()
        self.reloadData()
    }
    
    weak var previewVC: PreviewPhotoViewController?
    var didSelect: ((_ index: Int) -> Void)?
    
    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self

        let width = 60
        let height = 46
        
        if let layout = self.collectionView.collectionViewLayout as? LNZInfiniteCollectionViewLayout {
            layout.focusChangeDelegate = self
            layout.itemSize = CGSize(width: width, height: height)
            layout.interitemSpacing = 0.5
        }
        
        if let currentFocused = (self.collectionView.collectionViewLayout as? FocusedContaining)?.currentInFocus {
            let indexPath = IndexPath(item: currentFocused, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }
        
        collectionView.register(cellType: PhotoCollectionViewCell.self)
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func createVideoThumbnail(from url: URL) -> UIImage? {

        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.maximumSize = CGSize(width: 200, height: 200)

        let time = CMTimeMakeWithSeconds(0.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
}

extension CollectionViewCustom: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count > 0 ? self.photos.count : self.photosURL.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PhotoCollectionViewCell.self, for: indexPath)
        if self.photos.count > 0 {
            let photo = self.photos[indexPath.item]
            cell.photoView.kf.indicatorType = .activity
            cell.photoView.fetchImageAsset(photo, targetSize: cell.bounds.size) { _ in
                cell.photoView.kf.indicatorType = .none
            }
        } else {
            let photo = self.photosURL[indexPath.item]
            cell.photoView.kf.indicatorType = .activity
            cell.photoView.kf.setImage(with: photo, placeholder: self.createVideoThumbnail(from: photo)) { _ in
                cell.photoView.kf.indicatorType = .none
            }
        }
        let check = self.checkSelected[indexPath.item]
        cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "tick") : nil, for: .normal)
        cell.blurView.isHidden = !check
        cell.contraintTop.constant = 6
        cell.contraintRight.constant = 13
        cell.btnRadio.isUserInteractionEnabled = false
        cell.selectedPhoto = { [weak self] in
            guard let self = self else { return }
            
            self.checkSelected[indexPath.item] = !self.checkSelected[indexPath.item]
            let check = self.checkSelected[indexPath.item]
            cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "tick") : nil, for: .normal)
            cell.blurView.isHidden = check
        }
        return cell
    }
}

extension CollectionViewCustom: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}

extension CollectionViewCustom: FocusChangeDelegate {
    func focusContainer(_ container: FocusedContaining, willChangeElement inFocus: Int, to newInFocus: Int) {
        print("00")
    }
    
    func focusContainer(_ container: FocusedContaining, didChangeElement inFocus: Int) {
        print("0->", inFocus)
        self.didSelect?(inFocus)
    }
}
