//
//  PhotoCollectionViewCell.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/11/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: FSPagerViewCell {
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var contraintTop: NSLayoutConstraint!
    @IBOutlet weak var contraintRight: NSLayoutConstraint!
    
    var selectedPhoto: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.blurView.isHidden = true
    }

    @IBAction func actionRadioSelect(_ sender: UIButton) {
        self.selectedPhoto?()
    }
    
    func showImagePlay(isShow: Bool = true) {
        DispatchQueue.main.async {
            if isShow {
                self.imgPlay.image = UIImage(named: "play-button-arrowhead")?.tintColor(UIColor.white.withAlphaComponent(0.5))
            }else {
                self.imgPlay.image = nil
            }
        }
    }
}
