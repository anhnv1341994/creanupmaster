//
//  DetailsAllPhotosViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/11/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Photos
import UIKit

class RecybinPhotosViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!

    private let btnDelete = ButtonDeleteView()
    @IBOutlet weak var roundedView: RoundedView!

    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var btnRightNav: UIButton!
    @IBOutlet weak var lbNoData: UILabel! {
        didSet {
            lbNoData.text = "kNoFiles".localized()
        }
    }
    fileprivate var checkSelected: [Bool] = []

    private var isSelectAll = true {
        didSet {
            self.btnRightNav.setTitle(isSelectAll ? "kDeselectAll".localized() : "kSelectAll".localized(), for: .normal)
        }
    }

    private var countSelect: Int = 0 {
        didSet {
            self.navTitle.text = "\(countSelect > 0 ? "\(countSelect) " : "")\("kPhoto".localized())\((countSelect > 1) ? "kS".localized() : "") \("kSelected".localized())"
            self.isSelectAll = (countSelect == photos.count && countSelect != 0)

            self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: countSelect > 0)

            self.showButton(count: self.photos.count)
        }
    }

    var photos: [PHAsset] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupButtonDelete()
        self.setupCollectionView()
        self.fetchAssets()
        self.reloadData()
        PHPhotoLibrary.shared().register(self)
    }

    @IBAction func actionSelected(_ sender: Any) {
        self.isSelectAll = !self.isSelectAll
        self.checkSelected.removeAll()
        for _ in 0 ..< photos.count {
            self.checkSelected.append(isSelectAll)
        }
        self.countSelect = isSelectAll ? photos.count : 0

        self.reloadData()
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self

        if let flowLayout = self.collectionView.collectionViewLayout as? GridCollectionViewLayout {
            flowLayout.scrollDirection = .vertical
            flowLayout.sectionInset = UIEdgeInsets.init(top: -1, left: 1, bottom: 0, right: 1)
            flowLayout.interitemSpacing = 1
            flowLayout.lineSpacing = 1
            flowLayout.aspectRatio = 1
            flowLayout.headerReferenceLength = 1.0
            flowLayout.numberOfItemsPerLine = 4
        }
        collectionView.register(cellType: PhotoCollectionViewCell.self)
    }

    fileprivate func reloadData() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    private func fetchAssets() { // 1
        self.countSelect = 0
        for _ in 0 ..< photos.count {
            self.checkSelected.append(false)
        }
        self.reloadData()
    }

    private func setupButtonDelete() {
        self.btnDelete.btnDelete.setTitle("kDeleteSelected".localized(), for: .normal)
        self.btnDelete.btnRecover.setTitle("kRecoverSelected".localized(), for: .normal)
        self.btnDelete.isShowRecover = true
        self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: false, alpha: 0)
        self.btnDelete.gotoPolicyView = { [weak self] type in
            let vc = PrivacyPolicyViewController()
            vc.type = type
            self?.pushViewWithScreen(vc)
        }
        self.btnDelete.didDelete = { [weak self] in
            guard let self = self else { return }
            let group = DispatchGroup()
            var photosDraft: [PHAsset] = []
            var checkSelectedDraft: [Bool] = []
            var listIdentify: [String] = []

            for (index, check) in self.checkSelected.enumerated() {
                group.enter()
                if check {
                    let identify = self.photos[index].localIdentifier
                    listIdentify.append(identify)
                    group.leave()
                } else {
                    checkSelectedDraft.append(self.checkSelected[index])
                    photosDraft.append(self.photos[index])
                    group.leave()
                }
            }

            group.notify(queue: .main) {
                self.checkSelected = checkSelectedDraft
                self.photos = photosDraft
                UserDefaults.removeImageInRecybin(identifys: listIdentify)
                self.countSelect = self.checkSelected.filter { $0 == true }.count
                self.reloadData()
            }


            self.reloadData()
        }

        self.btnDelete.didRecover = { [weak self] in
            guard let self = self else { return }
            var listRecybin: [String] = []
            if let currentRecybin = UserDefaults.listRecybin {
                listRecybin = currentRecybin
            }
            let group = DispatchGroup()
            var photosDraft: [PHAsset] = []
            var checkSelectedDraft: [Bool] = []

            for (index, check) in self.checkSelected.enumerated() {
                group.enter()
                if check {
                    let identify = self.photos[index].localIdentifier
                    if let index = listRecybin.firstIndex(of: identify) {
                        listRecybin.remove(at: index)
                    }
                    group.leave()
                } else {
                    checkSelectedDraft.append(self.checkSelected[index])
                    photosDraft.append(self.photos[index])
                    group.leave()
                }
            }

            group.notify(queue: .main) {
                self.checkSelected = checkSelectedDraft
                self.photos = photosDraft
                UserDefaults.listRecybin = listRecybin
                self.countSelect = self.checkSelected.filter { $0 == true }.count
                self.reloadData()
            }
        }
    }
}

extension RecybinPhotosViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PhotoCollectionViewCell.self, for: indexPath)
        let photo = self.photos[indexPath.item]
        cell.photoView.fetchImageAsset(photo, targetSize: cell.bounds.size) { _ in
//          cell.blurView.isHidden = !success
        }
        let check = self.checkSelected[indexPath.item]
        cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        cell.blurView.isHidden = !check

        cell.selectedPhoto = { [weak self] in
            guard let self = self else { return }

            self.checkSelected[indexPath.item] = !self.checkSelected[indexPath.item]
            let check = self.checkSelected[indexPath.item]
            cell.btnRadio.setImage(check ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
            self.countSelect = self.checkSelected.filter { $0 == true }.count
            self.isSelectAll = !self.checkSelected.contains(false)
        }
        return cell
    }
}

extension RecybinPhotosViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let vc = PreviewPhotoViewController()
        vc.indexSelected = indexPath.item
        vc.photos = self.photos
        vc.checkSelected = self.checkSelected
        vc.delegate = self
        self.pushViewWithScreen(vc)
    }
}

extension RecybinPhotosViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        print("photoLibraryDidChange")
    }
}

extension RecybinPhotosViewController: PreviewPhotosDelegate {
    func didSelectedImage(index: Int) {
        self.checkSelected[index] = !self.checkSelected[index]
        self.countSelect = self.checkSelected.filter { $0 == true }.count
        self.isSelectAll = !self.checkSelected.contains(false)
        self.reloadData()
    }

    func showButton(count: Int) {
        if self.collectionView != nil, self.btnRightNav != nil {
            self.collectionView.isHidden = count == 0
            self.btnRightNav.alpha = count == 0 ? 0 : 1
            self.btnRightNav.isEnabled = count == 0 ? false : true
        }
    }
}
