//
//  YourGalleryViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Photos
import UIKit

class YourGalleryViewController: UIViewController {
    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "kYourGallery".localized()
        }
    }
    @IBOutlet weak var lbAllPhotos: UILabel! {
        didSet {
            lbAllPhotos.text = "kGalleryCalulation".localized()
        }
    }
    @IBOutlet weak var lbSimularPicture: UILabel! {
        didSet {
            lbSimularPicture.text = "kGalleryCalulation".localized()
        }
    }
    @IBOutlet weak var lbSerialPhotos: UILabel! {
        didSet {
            lbSerialPhotos.text = "kGalleryCalulation".localized()
        }
    }
    @IBOutlet weak var lbBlurPicture: UILabel! {
        didSet {
            lbBlurPicture.text = "kGalleryCalulation".localized()
        }
    }
    @IBOutlet weak var lbScreenCapture: UILabel! {
        didSet {
            lbScreenCapture.text = "kGalleryCalulation".localized()
        }
    }
    @IBOutlet weak var lbSimularDynamicGraph: UILabel! {
        didSet {
            lbSimularDynamicGraph.text = "kGalleryCalulation".localized()
        }
    }

    @IBOutlet weak var btnAllPhotos: UIButton! {
        didSet {
            btnAllPhotos.setTitle("btnAllPhotos".localized(), for: .normal)
        }
    }
    @IBOutlet weak var btnSimularPicture: UIButton! {
        didSet {
            btnSimularPicture.setTitle(" " + "btnSimularPicture".localized(), for: .normal)
        }
    }
    @IBOutlet weak var btnSerialPhotos: UIButton! {
        didSet {
            btnSerialPhotos.setTitle(" " + "btnSerialPhotos".localized(), for: .normal)
        }
    }
    @IBOutlet weak var btnBlurPicture: UIButton! {
        didSet {
            btnBlurPicture.setTitle(" " + "btnBlurPicture".localized(), for: .normal)
        }
    }
    @IBOutlet weak var btnScreenCapture: UIButton! {
        didSet {
            btnScreenCapture.setTitle(" " + "btnScreenCapture".localized(), for: .normal)
        }
    }
    @IBOutlet weak var btnSimularDynamicGraph: UIButton! {
        didSet {
            btnSimularDynamicGraph.setTitle( "btnSimularDynamicGraph".localized(), for: .normal)
        }
    }

    var allPhotos: [PHAsset] = []
    var simularPicture: [PHAsset] = []
    var serialPhotos: [PHAsset] = []
    var blurPicture: [PHAsset] = []
    var screenCapture: [PHAsset] = []
    var simularDynamicGraph: [PHAsset] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        PHPhotoLibrary.shared().register(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getPermissionPhotos { granted in
            guard granted else { return }
            self.fetchAssets()
        }
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionViewAllPhotos(_ sender: Any) {
        self.gotoDetails(type: .all)
    }

    @IBAction func actionViewSimularPicture(_ sender: Any) {
        self.gotoDetails(type: .simular)
    }

    @IBAction func actionViewSerialPhotos(_ sender: Any) {
        self.gotoDetails(type: .serial)
    }

    @IBAction func actionViewBlurPhotos(_ sender: Any) {
        self.gotoDetails(type: .blur)
    }

    @IBAction func actionViewScreenShort(_ sender: Any) {
        self.gotoDetails(type: .screen)
    }

    @IBAction func actionViewDynamicGraph(_ sender: Any) {
        self.gotoDetails(type: .dynamic)
    }

    private func gotoDetails(type: PhotosType) {
        let details = DetailsAllPhotosViewController()
        details.type = type
        switch type {
        case .all:
            details.photos = self.allPhotos
        case .simular:
            details.photos = self.simularPicture.sorted(by: { $0.creationDate! > $1.creationDate! })
        case .serial:
            details.photos = self.serialPhotos.sorted(by: { $0.creationDate! > $1.creationDate! })
        case .blur:
            details.photos = self.blurPicture.sorted(by: { $0.creationDate! > $1.creationDate! })
        case .screen:
            details.photos = self.screenCapture.sorted(by: { $0.creationDate! > $1.creationDate! })
        case .dynamic:
            details.photos = self.simularDynamicGraph.sorted(by: { $0.creationDate! > $1.creationDate! })
        }

        self.pushViewWithScreen(details)
    }

    deinit {
        print(String(describing: self.classForCoder))
    }
}

extension YourGalleryViewController {
    func fetchAssets() { // 1
        self.allPhotos.removeAll()
        self.simularPicture.removeAll()
        self.serialPhotos.removeAll()
        self.blurPicture.removeAll()
        self.screenCapture.removeAll()
        self.simularDynamicGraph.removeAll()

        let allPhotosOptions = PHFetchOptions()
        allPhotosOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        allPhotosOptions.sortDescriptors = [
            NSSortDescriptor(
                key: "creationDate",
                ascending: false)
        ]
        let listRecybin = UserDefaults.listRecybin ?? []

        PHAsset.fetchAssets(with: allPhotosOptions).enumerateObjects { object, _, _ in
            if !listRecybin.contains(object.localIdentifier) {
                self.allPhotos.append(object)
            }
        }

        let simularPicture = PHAssetCollection.fetchAssetCollections(
            with: .smartAlbum,
            subtype: .albumRegular,
            options: nil)

        PHAsset.fetchAssets(in: simularPicture[0], options: nil).enumerateObjects { object, _, _ in
            if !listRecybin.contains(object.localIdentifier), object.mediaType == .image {
                self.simularPicture.append(object)
            }
        }

        let serialPhotos = PHAssetCollection.fetchAssetCollections(
            with: .smartAlbum,
            subtype: .smartAlbumUserLibrary,
            options: nil)
        PHAsset.fetchAssets(in: serialPhotos[0], options: nil).enumerateObjects { object, _, _ in
            if !listRecybin.contains(object.localIdentifier), object.mediaType == .image {
                self.serialPhotos.append(object)
            }
        }

        let blurPicture = PHAssetCollection.fetchAssetCollections(
            with: .smartAlbum,
            subtype: .smartAlbumDepthEffect,
            options: nil)
        PHAsset.fetchAssets(in: blurPicture[0], options: nil).enumerateObjects { object, _, _ in
            if !listRecybin.contains(object.localIdentifier), object.mediaType == .image {
                self.blurPicture.append(object)
            }
        }

        let screenCapture = PHAssetCollection.fetchAssetCollections(
            with: .smartAlbum,
            subtype: .smartAlbumScreenshots,
            options: nil)
        PHAsset.fetchAssets(in: screenCapture[0], options: nil).enumerateObjects { object, _, _ in
            if !listRecybin.contains(object.localIdentifier), object.mediaType == .image {
                self.screenCapture.append(object)
            }
        }

        let simularDynamicGraph = PHAssetCollection.fetchAssetCollections(
            with: .smartAlbum,
            subtype: .smartAlbumLivePhotos,
            options: nil)
        PHAsset.fetchAssets(in: simularDynamicGraph[0], options: nil).enumerateObjects { object, _, _ in
            if !listRecybin.contains(object.localIdentifier), object.mediaType == .image {
                self.simularDynamicGraph.append(object)
            }
        }

        DispatchQueue.main.async {
            self.lbAllPhotos.text = self.calculationPhoto(count: self.allPhotos.count)
            self.lbSimularPicture.text = self.calculationPhoto(count: self.simularPicture.count)
            self.lbSerialPhotos.text = self.calculationPhoto(count: self.serialPhotos.count)
            self.lbBlurPicture.text = self.calculationPhoto(count: self.blurPicture.count)
            self.lbScreenCapture.text = self.calculationPhoto(count: self.screenCapture.count)
            self.lbSimularDynamicGraph.text = self.calculationPhoto(count: self.simularDynamicGraph.count)
        }
    }

    private func calculationPhoto(count: Int) -> String? {
        let photo = "kPhoto".localized().lowercased()
        let ks = "kS".localized()
        return "\(count) \(photo)\(count > 1 ? "\(ks)" : "")"
    }
}

extension YourGalleryViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
//        DispatchQueue.main.sync {
//            if let changeDetails = changeInstance.changeDetails(for: allPhotos) {
//                allPhotos = changeDetails.fetchResultAfterChanges
//            }
//        }
    }
}
