//
//  InAppPurchasePresenter.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 09/05/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import StoreKit

protocol InAppPurchaseView: class {
    func setDataProducts(_ products: Set<SKProduct>)
    func setDataPurchaseProduct(success: Bool)
}

protocol InAppPurchasePresenter: class {
    func retrieveProductsInfo()
    func purchaseProduct(_ productType: IAPProduct)
}

class InAppPurchasePresenterView: InAppPurchasePresenter {
    weak var inAppPurchaseView: InAppPurchaseView?
    var products: Set<SKProduct> = []
    lazy var operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "purchase product"
        queue.maxConcurrentOperationCount = 1
        queue.qualityOfService = .background
        return queue
    }()

    init(_ inAppPurchaseView: InAppPurchaseView?) {
        self.inAppPurchaseView = inAppPurchaseView
    }

    func retrieveProductsInfo() {
        Loading.shared.startLoading()

        operationQueue.addOperation {
            IPAService.shared.retrieveProductsInfo { [weak self] products in
                guard let self = self else { return }
                Loading.shared.stopLoading()

                guard let products = products else { return }
                self.products = products
                self.inAppPurchaseView?.setDataProducts(products)
            }
        }
    }

    func purchaseProduct(_ productType: IAPProduct) {
        Loading.shared.startLoading()
        IPAService.shared.purchaseProduct(productType) { [weak self] isPurchase in
            Loading.shared.stopLoading()
            guard let self = self else { return }
            self.inAppPurchaseView?.setDataPurchaseProduct(success: isPurchase)
        }
    }
}
