//
//  InAppPurchaseViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Anh on 06/01/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import StoreKit

class InAppPurchaseViewController: UIViewController {
    var inAppPurchasePresenterView: InAppPurchasePresenterView?

    @IBOutlet weak var lbHeaderTitle: UILabel! {
        didSet {
            lbHeaderTitle.text = "kPurchaseHeaderTitle".localized()
        }
    }
    @IBOutlet weak var lbContentTitle1: UILabel! {
        didSet {
            lbContentTitle1.text = "kPurchaseContentTitle1".localized()
        }
    }
    @IBOutlet weak var lbContentTitle2: UILabel! {
        didSet {
            lbContentTitle2.text = "kPurchaseContentTitle2".localized()
        }
    }
    @IBOutlet weak var lbContentTitle3: UILabel! {
        didSet {
            lbContentTitle3.text = "kPurchaseContentTitle3".localized()
        }
    }

    @IBOutlet weak var btnYear: UIButton! {
        didSet { btnYear.tag = 1 }
    }

    @IBOutlet weak var btnWeek: UIButton! {
        didSet { btnWeek.tag = 2 }
    }
    
    @IBOutlet weak var btnStartTrial: UIButton! {
        didSet {
            btnStartTrial.setTitle("kPurchaseStartTrial".localized(), for: .normal)
        }
    }
    
    @IBOutlet weak var btnPolicy: UIButton! {
        didSet {
            btnPolicy.setTitle("kSettingPolicy".localized(), for: .normal)
        }
    }
    
    @IBOutlet weak var btnTerm: UIButton! {
        didSet {
            btnTerm.setTitle("kSettingTerm".localized(), for: .normal)
        }
    }

    var completionPurchase: (() -> Void)?
    var gotoPolicyView: ((_ type: WebType) -> Void)?

    private var selectPackage: IAPProduct = .appYear

    let desPriceTitle = "kTitleButton".localized()
    let week = "k1Week".localized()
    let year = "k1Year".localized()

    lazy var operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "purchase product"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .red
        self.inAppPurchasePresenterView?.retrieveProductsInfo()
    }

    @IBAction func actionClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func actionSelectPackagePurchase(_ sender: UIButton) {
        let tag = sender.tag
        self.selectPackage = tag == 1 ? .appYear : .appWeek
        self.btnYear.setBackgroundImage(tag == 1 ? #imageLiteral(resourceName: "rectangleSelected"): #imageLiteral(resourceName: "rectangleUnSelected"), for: .normal)
        self.btnWeek.setBackgroundImage(tag == 1 ? #imageLiteral(resourceName: "rectangleUnSelected"): #imageLiteral(resourceName: "rectangleSelected"), for: .normal)
    }

    @IBAction func actionConfirmed(_ sender: Any) {
        if DeviceManagement.shared().hasInternetConnection {
            self.operationQueue.addOperation {
                self.inAppPurchasePresenterView?.purchaseProduct(self.selectPackage)
            }
        } else {
            self.alertNotConnectInternet()
        }
    }

    @IBAction func actionPrivacyPolicy(_ sender: Any) {
        self.dismiss(animated: true) { [weak self] in
            self?.gotoPolicyView?(.policy)
        }
    }

    @IBAction func actionTermCondition(_ sender: Any) {
        self.dismiss(animated: true) { [weak self] in
            self?.gotoPolicyView?(.term)
        }
    }

    deinit {
        self.operationQueue.cancelAllOperations()
        print(String(describing: self.classForCoder))
    }
}

extension InAppPurchaseViewController: InAppPurchaseView {
    func setDataProducts(_ products: Set<SKProduct>) {

        products.forEach { product in
            let price = product.localizedPrice

            if product.localizedTitle == "One Week" {
                let title = String(format: self.desPriceTitle, price, week.lowercased())
                self.btnWeek.setTitle(title, for: .normal)
            } else if product.localizedTitle == "One Year" {
                let title = String(format: self.desPriceTitle, price, year.lowercased())
                self.btnYear.setTitle(title, for: .normal)
            }
        }
    }

    func setDataPurchaseProduct(success: Bool) {
        if success {
            print("isPurchase")
            UserDefaults.isPurchase = success
            self.dismiss(animated: true, completion: self.completionPurchase)

        } else {
            var namePackage = ""
            if self.selectPackage == .appWeek {
                namePackage = "1 \(week)"
            } else if selectPackage == .appYear {
                namePackage = "1 \(year)"
            }

            let msgPurchaseFaild = "kNotificationPurchaseFaild".localized()
            let message = String(format: msgPurchaseFaild, namePackage)
            let alert = AlertHelper.create(title: nil, message: message, buttonTitle: "kAlertOK".localized())
            self.present(alert, animated: true, completion: nil)
            print("False isPurchase")
        }
    }

    func alertNotConnectInternet() {
        let alert = AlertHelper.create(title: "kNoInternetTitle".localized(), message: "kNoInternetContent".localized(), buttonTitle: "kAlertOK".localized())
        self.present(alert, animated: true, completion: nil)
    }
}
