//
//  BaseViewController.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 2407//2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var countFileFree: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setupBackButton() {
        let button = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.backAction))
        self.navigationItem.leftBarButtonItem = button
        self.navigationItem.hidesBackButton = true
    }
    
    func setupLeftButton(_ image: UIImage?, text: String?) {
        let button: UIBarButtonItem
        if let image = image {
            button = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.leftAction))
        } else {
            button = UIBarButtonItem(title: text, style: .plain, target: self, action: #selector(self.leftAction))
        }
        self.navigationItem.leftBarButtonItem = button
        self.navigationItem.hidesBackButton = true
    }
    
    func setupRightButton(_ image: UIImage?, text: String?) {
        let button: UIBarButtonItem
        if let image = image {
            button = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.rightAction))
        } else {
            button = UIBarButtonItem(title: text, style: .plain, target: self, action: #selector(self.rightAction))
        }
        self.navigationItem.rightBarButtonItem = button
    }
    
    @objc func backAction() {
        self.popViewController()
    }
    
    @objc func leftAction() {}
    
    @objc func rightAction() {}
}

import CFNetwork

struct DeviceInfo: Codable {
    let username: String
    let cifNumber: String
    let deviceManagementId: String
}

protocol NetworConnectionkState: AnyObject {
    func hasConnection(hasConnection: Bool)
}

class DeviceManagement: NSObject {

    static let isEnableLog = false
    static let chunkedSize = 200
    
    var didCheckVersion = false

    
    static let sharedInstance = DeviceManagement()
    override init() {
        super.init()
    }
    
    deinit {
        
    }

    class func shared() -> DeviceManagement {
        return sharedInstance
    }
    
    // MARK: NetworkConnection
    var hasInternetConnection: Bool {
        if let connection = self.reachability?.connection {
            return (connection != .none)
        }
        return false
    }
    
    private lazy var reachability: Reachability? = {
        let reachability: Reachability?
        if let host = URL(string: IPAService.shared.verifyType.rawValue)?.host {
            reachability = Reachability(hostname: host)
        }
        else {
            reachability = Reachability()
        }

        
        reachability?.whenReachable = { [weak self] (reachability) in
            switch reachability.connection {
            case .wifi:
                print("Wifi connected")
            case .cellular:
                print("Cellular connected")
            default:
                break
            }
            self?.notifyNetworkConnectionObservers(hasConnection: true)
        }
        
        reachability?.whenUnreachable = { [weak self] (_) in
            print("No network connected")
            self?.notifyNetworkConnectionObservers(hasConnection: false)
        }
        
        return reachability
    }()
    
    func addNetworkNotify(_ add: Bool) {
        if add {
            do {
                try self.reachability?.startNotifier()
            } catch {
                print(error)
            }
        }
        else {
            self.reachability?.stopNotifier()
        }
    }
    
    private var observers: NSHashTable<AnyObject> = NSHashTable.weakObjects()
    func registerNetworkObserver<Observer: NetworConnectionkState>(observer: Observer) {
        observer.hasConnection(hasConnection: self.hasInternetConnection)
        self.observers.add(observer)
    }
    func unregisterNetworkObserver<Observer: NetworConnectionkState>(observer: Observer) {
        self.observers.remove(observer)
    }
    fileprivate func notifyNetworkConnectionObservers(hasConnection: Bool) {
        DispatchQueue.main.async {
            self.observers.allObjects.compactMap({ $0 as? NetworConnectionkState }).forEach({ $0.hasConnection(hasConnection: hasConnection) })
        }
    }
    
    
    // MARK: CheckActiveApp
    let kAutoLogOutTime: TimeInterval = 180.0
    let kRefreshTokenTime: TimeInterval = 160.0
    var needCheckActiveApp: Bool = false
}
