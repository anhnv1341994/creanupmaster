//
//  UserDefaults.swift
//  ScannerApp
//
//  Created by Nguyen Van Anh on 2307//2020.
//  Copyright © 2020 Nguyen Van Anh. All rights reserved.
//

import UIKit

struct UserDefault {
    static let oldEvaluatedPolicy = "oldEvaluatedPolicy"
    static let isPurchase = "PURCHASE"
    static let appPin = "APPPIN"
    static let appPinCode = "APPPINCODE"
    static let touchID = "TOUCHID"
    static let startAppWith = "STARTAPPWITH"
    static let emailAD = "EMAILADDRESS"
    static let pictureQuality = "PICTUREQUALITY"
    static let wifiTransfer = "WIFITRANSFER"
    static let improveRecognition = "IMPROVERECOGNITION"
    static let nameIsCreateLast = "nameIsCreateLast"

    fileprivate static func setUserDefaults(_ value: Any?, forKey: String) {
        UserDefaults.standard.setValue(value, forKey: forKey)
        UserDefaults.standard.synchronize()
    }

    fileprivate static func getUserDefaults(_ forKey: String) -> Any? {
        return UserDefaults.standard.value(forKey: forKey)
    }

    fileprivate static func removeUserDefaults(_ forKey: String) {
        UserDefaults.standard.removeObject(forKey: forKey)
        UserDefaults.standard.synchronize()
    }
}

extension UserDefault {
    static var getNameLastCreate: String? {
        return UserDefault.getUserDefaults(UserDefault.nameIsCreateLast) as? String
    }

    static func setNameLastCreate(_ name: String?) {
        UserDefault.setUserDefaults(name, forKey: UserDefault.nameIsCreateLast)
    }
    
    // MARK: - Purchase App

    static var getIsPurchase: Bool? {
        return UserDefault.getUserDefaults(UserDefault.isPurchase) as? Bool
    }

    static func setIsPurchase(_ isOn: Bool?) {
        UserDefault.setUserDefaults(isOn, forKey: UserDefault.isPurchase)
    }

    // MARK: - App pin

    static var getAppPin: String? {
        return UserDefault.getUserDefaults(UserDefault.appPin) as? String
    }

    static func setAppPin(_ str: String?) {
        UserDefault.setUserDefaults(str, forKey: UserDefault.appPin)
    }

    // MARK: - App pin Code

    static var getAppPinCode: String? {
        return UserDefault.getUserDefaults(UserDefault.appPinCode) as? String
    }

    static func setAppPinCode(_ pinCode: String?) {
        UserDefault.setUserDefaults(pinCode, forKey: UserDefault.appPinCode)
    }

    // MARK: - TouchID

    static var getTouchID: Bool? {
        return UserDefault.getUserDefaults(UserDefault.touchID) as? Bool
    }

    static func setTouchID(_ isOn: Bool?) {
        UserDefault.setUserDefaults(isOn, forKey: UserDefault.touchID)
    }

    // MARK: - Start app with

    static var getStartAppWith: String? {
        return UserDefault.getUserDefaults(UserDefault.startAppWith) as? String
    }

    static func setStartAppWith(_ str: String?) {
        UserDefault.setUserDefaults(str, forKey: UserDefault.startAppWith)
    }

    // MARK: - Wifi Transfer

    static var getWifiTransfer: Bool? {
        return UserDefault.getUserDefaults(UserDefault.wifiTransfer) as? Bool
    }

    static func setWifiTransfer(_ isOn: Bool?) {
        UserDefault.setUserDefaults(isOn, forKey: UserDefault.wifiTransfer)
    }

    // MARK: - Email

    static var getEmail: String? {
        return UserDefault.getUserDefaults(UserDefault.emailAD) as? String
    }

    static func setEmail(_ str: String?) {
        UserDefault.setUserDefaults(str, forKey: UserDefault.emailAD)
    }

    // MARK: - Picture Quality

    static var getPictureQuality: String? {
        return UserDefault.getUserDefaults(UserDefault.pictureQuality) as? String
    }

    static func setPictureQuality(_ str: String?) {
        UserDefault.setUserDefaults(str, forKey: UserDefault.pictureQuality)
    }

    // MARK: - Improve recognition

    static var getImproveRecognition: Bool? {
        return UserDefault.getUserDefaults(UserDefault.improveRecognition) as? Bool
    }

    static func setImproveRecognition(_ isOn: Bool?) {
        UserDefault.setUserDefaults(isOn, forKey: UserDefault.improveRecognition)
    }

    // MARK: - Improve recognition

    static var getOldEvaluatedPolicy: String? {
        return UserDefault.getUserDefaults(UserDefault.oldEvaluatedPolicy) as? String
    }

    static func setOldEvaluatedPolicy(_ str: String?) {
        UserDefault.setUserDefaults(str, forKey: UserDefault.oldEvaluatedPolicy)
    }
}

extension Encodable {
    func toObject() -> [String: Any] {
        do {
            let data = try JSONEncoder().encode(self)
            let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]

            if let dictionary = dictionary {
                return dictionary
            }
            return [:]
        }
        catch {
            return [:]
        }
    }
    
    func toJsonString() -> String? {
        do {
            let data = try JSONEncoder().encode(self)
            let jsonStr = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? String
            return jsonStr
        }
        catch {
            return ""
        }
    }

    func toData() -> Data? {
        do {
            let data = try JSONEncoder().encode(self)
            return data
        }
        catch {
            return nil
        }
    }

    func toDictionary() -> [String: Any] {
        do {
            let data = try JSONEncoder().encode(self)
            let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]

            if let dictionary = dictionary {
                return dictionary
            }
            return [:]
        }
        catch {
            return [:]
        }
    }
}

struct AppUtility {
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }

    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation: UIInterfaceOrientation) {
        self.lockOrientation(orientation)

        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
}
