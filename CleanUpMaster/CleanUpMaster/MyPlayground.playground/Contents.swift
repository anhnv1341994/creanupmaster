import UIKit


let printerOperation = BlockOperation()

printerOperation.addExecutionBlock { print("I") }
printerOperation.addExecutionBlock { print("am") }
printerOperation.addExecutionBlock { print("printing") }
printerOperation.addExecutionBlock { print("block") }
printerOperation.addExecutionBlock { print("operation") }

printerOperation.completionBlock = {
    print("I'm done printing")
}

let operationQueue = OperationQueue()
operationQueue.maxConcurrentOperationCount = 1
operationQueue.qualityOfService = .default
operationQueue.addOperation(printerOperation)
