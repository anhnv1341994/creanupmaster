//
//  ContactTableViewCell.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/14/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imgPersion: UIImageView!
    @IBOutlet weak var firstCharName: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var viewRadio: UIView! {
        didSet {
            viewRadio.isHidden = true
        }
    }
    @IBOutlet weak var radioImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupData(_ contact: Contact) {
        var name = ""
        if let nameCT = contact.contact.fullName, nameCT != "" {
            name = nameCT.localizedCapitalized
        }else {
            name = contact.contact.phones ?? ""
        }
        self.phoneNumber.text = name
        if let imgData = contact.contact.imageData {
            self.imgPersion.image = UIImage(data: imgData)
            self.imgPersion.isHidden = false
            self.firstCharName.isHidden = true
        }else {
            let nameStrings = (contact.contact.fullName ?? "").components(separatedBy: " ")
            var name = ""
            for (index, str) in nameStrings.enumerated() {
                if (index == 0 || index == 1), nameStrings.count > 1 {
                    if str.count > 0 {
                        name += str.character(at: 0)?.uppercased() ?? ""
                    }
                }
            }
            self.firstCharName.text = name
            self.imgPersion.isHidden = true
            self.firstCharName.isHidden = false
        }
    }
}
