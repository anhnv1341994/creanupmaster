//
//  SettingsHeaderView.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class RepeatedHeaderView: CustomViewXib {
    @IBOutlet weak var lbNameContact: UILabel!
    @IBOutlet weak var lbPhoneNumber: UILabel!
    @IBOutlet weak var btnSelectAll: UIButton!
    var isSelectAll = false
    var didSelectAll: ((Bool) -> Void)?
    var didSelect: (() -> Void)?
    
    @IBAction func actionSelectAll(_ sender: Any) {
        self.isSelectAll = !self.isSelectAll
        self.didSelectAll?(self.isSelectAll)
    }
    
    @IBAction func actionRadioSelect(_ sender: Any) {
        self.didSelect?()
    }
}
