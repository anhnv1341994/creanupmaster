//
//  RepeatedTableViewCell.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/18/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class NoNumberContactTableViewCell: UITableViewCell {
    @IBOutlet weak var lbNameContact: UILabel!
    @IBOutlet weak var btnRadio: UIButton!

    var didSelect: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupData(contact: Contact) {
        self.lbNameContact.text = contact.contact.fullName != "" ? contact.contact.fullName : (contact.contact.phones?.count != nil ? contact.contact.phones : "Unknow")
    }

    @IBAction func actionRadioSelect(_ sender: Any) {
        self.didSelect?()
    }
}
