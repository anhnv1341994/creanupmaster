//
//  SettingsHeaderView.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class ButtonDeleteView: CustomViewXib {
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnRecover: UIButton!
    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak var recoverView: UIStackView!

    var didDelete: (() -> Void)?
    var didRecover: (() -> Void)?
    var gotoPolicyView: ((_ type: WebType) -> Void)?
    var isShow = false
    var isShowRecover = false

    @IBAction func actionDelete(_ sender: Any) {
        if(UserDefaults.isPurchase ?? false) {
            self.didDelete?()
        } else {
            self.gotoPurchase()
        }
    }
    @IBAction func actionRecover(_ sender: Any) {
        if(UserDefaults.isPurchase ?? false) {
            self.didRecover?()
        } else {
            self.gotoPurchase()
        }
    }

    func showButtonDelete(sView: UIView, isShow: Bool, alpha: CGFloat? = nil) {
        if self.isShow != isShow || alpha != nil {
            self.isShow = isShow
            self.recoverView.isHidden = !self.isShowRecover
            self.removeFromSuperview()
            sView.addSubview(self)
            self.alpha = (alpha != nil) ? alpha! : (isShow ? 0 : 1)
            let width: CGFloat = sView.frame.width - (50.0)
            let height: CGFloat = self.isShowRecover ? 121.0 : 50.0
            let marginX: CGFloat = 25.0
            let marginY: CGFloat = sView.frame.height
            UIView.animate(withDuration: 0.5) {
                if isShow {
                    self.alpha = 1
                    self.frame = CGRect(x: marginX, y: marginY - (self.isShowRecover ? 136.0 : 65.0), width: width, height: height)
                } else {
                    self.alpha = 0
                    self.frame = CGRect(x: marginX, y: marginY, width: width, height: height)
                }
            }
        }
    }

    func gotoPurchase() {
        let purchaseVC = InAppPurchaseViewController()
        let presenter = InAppPurchasePresenterView(purchaseVC)
        purchaseVC.inAppPurchasePresenterView = presenter
        purchaseVC.modalPresentationStyle = .fullScreen
        guard let window = UIApplication.shared.windows.first?.rootViewController else { return }
        window.present(purchaseVC, animated: true, completion: nil)
        purchaseVC.gotoPolicyView = self.gotoPolicyView
    }
}
