//
//  SettingsTableViewCell.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit

class AddressBookTableViewCell: UITableViewCell {
    @IBOutlet weak var imageSettings: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imageArrow: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setDataForRow(item: AddressBookItem) {
        self.lbTitle.text = item.title
        if let image = item.image {
            self.imageSettings.image = UIImage(named: image)
        }
    }
}
