//
//  RepeatedTableViewCell.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/18/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Contacts
import UIKit

class RepeatedTableViewCell: UITableViewCell {
    @IBOutlet weak var lbNameContact: UILabel!
    @IBOutlet weak var lbPhoneNumber: UILabel!
    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak var btnRadio: UIButton!
    var didSelect: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupData(contact: CNContact) {
        self.lbNameContact.isHidden = contact.fullName == nil
        self.lbNameContact.text = contact.fullName
        self.lbPhoneNumber.text = contact.phones
    }

    func setupConstrainBottom(customBottom: Bool) {
        self.roundedView.bottomLeft = customBottom
        self.roundedView.bottomRight = customBottom
        self.roundedView.cornerRadius = customBottom ? 15 : 0
    }

    @IBAction func actionRadioSelect(_ sender: Any) {
        self.didSelect?()
    }
}

extension CNContact {
    var fullName: String? {
        return CNContactFormatter.string(from: self, style: .fullName)
    }

    var phones: String? {
        var phones = ""
        self.phoneNumbers.forEach { phones += $0.value.stringValue + " . " }
        return String(phones.dropLast(3))
    }

    var numbers: [String] {
        var array: [String] = []
        self.phoneNumbers.forEach { array.append($0.value.stringValue.replacingOccurrences(of: " ", with: "")) }
        return array
    }
}
