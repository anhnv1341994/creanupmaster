//
//  AddressBookRepeatedViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/18/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import ContactsUI
import SwiftyContacts
import UIKit

class AddressBookRepeatedViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            self.tableView.sectionHeaderHeight = 155
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
            self.tableView.register(cellType: RepeatedTableViewCell.self)
            self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        }
    }

    @IBOutlet weak var roundedView: RoundedView!

    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "kContacts".localized()
        }
    }

    @IBOutlet weak var lbNoData: UILabel! {
        didSet {
            lbNoData.text = "kNoContacts".localized()
        }
    }

    @IBOutlet weak var leftBarButton: UIButton! {
        didSet {
            leftBarButton.setTitle(nil, for: .normal)
            leftBarButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
    }

    @IBOutlet weak var rightBarButton: UIButton! {
        didSet {
            rightBarButton.setTitle(titleSelectAll, for: .normal)
        }
    }

    private var countSelect: Int = 0 {
        didSet {
            let contact = "kContact".localized()
            let selected = "kSelected".localized()
            let ks = "kS".localized()

            self.navTitle.text = "\(countSelect > 0 ? "\(countSelect) " : "")\(contact)\((countSelect > 1) ? "\(ks)" : "") \(selected)"
            self.isSelectAll = (countSelect == totalContact && countSelect != 0)
            self.rightBarButton.setTitle(isSelectAll ? titleDeselectAll : titleSelectAll, for: .normal)

            self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: isShowButton)
            self.showButton(count: self.contactGroupUnique.count)
        }
    }

    lazy var getContactQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "getAllContacts"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    lazy var mergePhonetQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "mergercontactphonenumber"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    private let btnDelete = ButtonDeleteView()
    private var isShowButton: Bool {
        return self.contactGroupUnique.filter { $0.isShowButton }.count > 0
    }

    private var isSelectAll = false

    private let titleSelectAll = "kSelectAll".localized()
    private let titleDeselectAll = "kDeselectAll".localized()
    private var totalContact = 0
    let contactServices = ContactsManagerment()

    var type: AddressBookType = .ManagerContacts
    private var contactGroupUnique: [ContactGroup] = []
    var contacts: [CNContact] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.countSelect = 0
        self.getListContact()
        self.setupButtonDelete()
    }

    func cancelRequestContact() {
        self.getContactQueue.cancelAllOperations()
    }

    private func getListContact() {
        switch type {
        case .NameRepetition:
            self.checkRepeatFullname()
        case .RepeatedNumber:
            self.checkRepeatNumber()
        default:
            break
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.cancelRequestContact()
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionRight(_ sender: Any) {
        for section in self.contactGroupUnique {
            for item in section.duplicates {
                item.isSelect = !self.isSelectAll
            }
        }
        self.countSelect = self.getCountSelect()
        self.reloadData()
    }

    private func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    private func getCountSelect() -> Int {
        var count = 0
        for section in self.contactGroupUnique {
            count += section.countCTSelect
        }
        return count
    }
}

extension AddressBookRepeatedViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.contactGroupUnique.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let group = self.contactGroupUnique[section].duplicates
        return group.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = RepeatedHeaderView()
        let header = self.contactGroupUnique[section]
        headerView.isSelectAll = self.contactGroupUnique[section].isSelect

        headerView.btnSelectAll.setTitle(headerView.isSelectAll ? self.titleDeselectAll : self.titleSelectAll, for: .normal)
        headerView.lbNameContact.text = header.ctMain.fullName
        headerView.lbPhoneNumber.text = header.ctMain.phones

        headerView.didSelectAll = { [weak self] isSelect in
            guard let self = self else { return }
            for contact in header.duplicates {
                contact.isSelect = isSelect
            }
            self.countSelect = self.contactGroupUnique.reduce(0) { $0 + $1.countCTSelect }
            self.tableView.reloadSections(IndexSet([section]), with: .automatic)
        }

        headerView.didSelect = { [weak self] in
            guard let self = self else { return }
            let contact = self.contactGroupUnique[section].ctMain
            self.gotoContactUI(ct: contact)
        }
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: RepeatedTableViewCell.self, for: indexPath)
        let section = self.contactGroupUnique[indexPath.section]
        let contact = section.duplicates[indexPath.row]
        cell.setupData(contact: contact.contact)
        cell.setupConstrainBottom(customBottom: false)
        if (self.contactGroupUnique[indexPath.section].duplicates.count - 1) == indexPath.row {
            cell.setupConstrainBottom(customBottom: true)
        }
        cell.btnRadio.setImage(contact.isSelect ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        cell.didSelect = { [weak self] in
            guard let self = self else { return }
            contact.isSelect = !contact.isSelect
            self.countSelect = self.getCountSelect()
            cell.btnRadio.setImage(contact.isSelect ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == -1 {
            return CGFloat.leastNormalMagnitude
        }
        return 155
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (self.contactGroupUnique[indexPath.section].duplicates.count - 1) == indexPath.row {
            return 85
        }
        return 74
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let header = self.contactGroupUnique[indexPath.section]
        let contact = header.duplicates[indexPath.row].contact
        self.gotoContactUI(ct: contact)
    }

    private func gotoContactUI(ct: CNContact) {
        let contactVC = CNContactViewController(for: ct)
        contactVC.allowsEditing = true
        contactVC.allowsActions = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.pushViewController(contactVC, animated: true)
    }
}

extension AddressBookRepeatedViewController {
    
    private func mergeContactsFullname() {
        Loading.shared.startLoading()
        let block = BlockOperation()
        block.addExecutionBlock {

            let contactGroupUnique = self.contactGroupUnique.filter { $0.isShowButton }
            for contact in contactGroupUnique {
                var listDup: [CNContact] = []
                let contactDup = contact.duplicates.filter { $0.isSelect }
                listDup = contactDup.map({ $0.contact })
                listDup.insert(contact.ctMain, at: 0)

                let newContact = self.contactServices.mergeAllDuplicatesFullName(duplicates: listDup)

                addContact(Contact: newContact.mutableCopy() as! CNMutableContact) { result in
                    switch result {
                    case .success(let bool):
                        print("-----mergeAllDuplicatesFullName-----", newContact.fullName ?? "")
                        if bool {
                            for ct in listDup {
                                deleteContact(Contact: ct.mutableCopy() as! CNMutableContact) { result in
                                    switch result {
                                    case .success(let bool):
                                        if bool {
                                            print("isDelete ", ct.fullName ?? "")
                                        }
                                    case .failure(let error):
                                        print(error.localizedDescription)
                                        Loading.shared.stopLoading()
                                    }
                                }
                            }
                        }
                    case .failure(let error):
                        Loading.shared.stopLoading()
                        print(error.localizedDescription)
                    }
                }
            }
        }
        
        block.completionBlock = { [weak self] in
            print("completionBlock mergeContactsFullname")
            self?.fetchContacts { [weak self] in
                guard let self = self else { return }

                Loading.shared.stopLoading()
                self.getListContact()
            }
        }
        
        self.getContactQueue.addOperation(block)
    }

    private func mergeContactsPhoneNumber() {
        Loading.shared.startLoading()
        let contactGroupUnique = self.contactGroupUnique.filter { $0.isShowButton }
        let block = BlockOperation()
        block.addExecutionBlock {
            for contact in contactGroupUnique {
                var listDup: [CNContact] = []
                let contactDup = contact.duplicates.filter { $0.isSelect }
                listDup = contactDup.map({ $0.contact })

                self.contactServices.mergeAllDuplicatesPhoneNumber(ctMain: contact.ctMain, duplicates: listDup) { bool in
                    if bool {
                        for ct in listDup {
                            deleteContact(Contact: ct.mutableCopy() as! CNMutableContact) { result in
                                switch result {
                                case .success(let bool):
                                    if bool {
                                        print("isDelete ", ct.fullName ?? "")
                                    }
                                case .failure(let error):
                                    print(error.localizedDescription)
                                    Loading.shared.stopLoading()
                                }
                            }
                        }
                    }else {
                        Loading.shared.stopLoading()
                    }
                }
            }
        }

        block.completionBlock = { [weak self] in
            print("completionBlock mergeContactsPhoneNumber")
            self?.fetchContacts { [weak self] in
                guard let self = self else { return }

                Loading.shared.stopLoading()
                self.getListContact()
            }
        }
        self.mergePhonetQueue.addOperation(block)
    }

    private func fetchContactsRepeatFullname(_ completion: (() -> Void)?) {
        self.getContactQueue.addOperation {
            let fullNames = self.contacts.map { CNContactFormatter.string(from: $0, style: .fullName) }
            let uniqueArray = Array(Set(fullNames.filter { name in fullNames.filter({ $0 == name }).count > 1 }))
            self.contactGroupUnique.removeAll()
            self.totalContact = 0

            for fullName in uniqueArray {
                let group = self.contacts.filter {
                    CNContactFormatter.string(from: $0, style: .fullName) == fullName
                }
                if group.count > 1 {
                    let contactMain = group[0]
                    self.totalContact += group.count - 1
                    let duplicates = group.filter { $0.identifier != contactMain.identifier }.map { Contact(contact: $0) }
                    let ctUnique = ContactGroup(ctMain: contactMain, duplicates: duplicates)
                    self.contactGroupUnique.append(ctUnique)
                }
            }

            OperationQueue.main.addOperation {
                completion?()
            }
        }
    }

    private func checkRepeatFullname() {
        Loading.shared.startLoading()
        self.fetchContactsRepeatFullname { [weak self] in
            guard let self = self else { return }
            Loading.shared.stopLoading()
            self.contactGroupUnique = self.contactGroupUnique.sorted { ct1, ct2 -> Bool in
                ct1.ctMain.fullName ?? "" > ct2.ctMain.fullName ?? ""
            }
            self.countSelect = self.getCountSelect()
            self.showButton(count: self.contactGroupUnique.count)
            self.reloadData()
        }
    }

    private func checkRepeatNumber() {
        Loading.shared.startLoading()
        self.getContactQueue.addOperation {

            let list1 = self.contacts.map { Contact(contact: $0) }
            self.contactGroupUnique.removeAll()
            self.totalContact = 0

            for contact1 in list1 {
                if contact1.isSelect == false {
                    let list2 = list1.filter { $0.isSelect == false }.filter { $0.contact.phoneNumbers.count > 0 }.sorted { ct1, ct2 -> Bool in
                        ct1.contact.phoneNumbers.count > ct2.contact.phoneNumbers.count
                    }

                    var group: [CNContact] = []
                    if contact1.contact.phoneNumbers.count > 0 {
                        let ct1 = contact1.contact.numbers

                        for contact2 in list2 {
                            if contact2.contact.phoneNumbers.count > 0 {
                                let ct2 = contact2.contact.numbers
                                
                                if Set(ct1).intersection(Set(ct2)).count > 0 {
                                    group.append(contact2.contact)
                                    contact2.isSelect = true
                                }
                            }
                        }

                        if group.count > 1 {
                            let contactMain = group[0]
                            self.totalContact += group.count - 1
                            let duplicates = group.filter { $0.identifier != contactMain.identifier }.map { Contact(contact: $0) }
                            let ctUnique = ContactGroup(ctMain: contactMain, duplicates: duplicates)
                            self.contactGroupUnique.append(ctUnique)
                        }
                    }
                }
            }

            OperationQueue.main.addOperation { [weak self] in
                guard let self = self else { return }

                Loading.shared.stopLoading()
                self.contactGroupUnique = self.contactGroupUnique.sorted { ct1, ct2 -> Bool in
                    ct1.ctMain.fullName ?? "" > ct2.ctMain.fullName ?? ""
                }
                self.countSelect = self.getCountSelect()
                self.showButton(count: self.contactGroupUnique.count)
                self.reloadData()
            }
        }
    }

    private func fetchContacts(_ completion: (() -> Void)?) {
        SwiftyContacts.fetchContacts(keysToFetch: self.contactServices.keys as! [CNKeyDescriptor], order: .userDefault) { result in
            switch result {
            case .success(let contacts):
                self.contacts = contacts
                completion?()
            case .failure(let error):
                print(error)
                completion?()
            }
        }
    }

    private func setupButtonDelete() {
        self.btnDelete.btnDelete.setTitle("kMerge".localized(), for: .normal)
        self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: false, alpha: 0)
        self.btnDelete.gotoPolicyView = { [weak self] type in
            let vc = PrivacyPolicyViewController()
            vc.type = type
            self?.pushViewWithScreen(vc)
        }

        self.btnDelete.didDelete = { [weak self] in
            guard let self = self else { return }
            switch self.type {
            case .NameRepetition:
                self.mergeContactsFullname()
            case .RepeatedNumber:
                self.mergeContactsPhoneNumber()
            default:
                break
            }
        }
    }

    func showButton(count: Int) {
        if self.tableView != nil, self.rightBarButton != nil {
            self.tableView.isHidden = count == 0
            self.rightBarButton.alpha = count == 0 ? 0 : 1
            self.rightBarButton.isEnabled = count == 0 ? false : true
        }
    }
}

class RepeatedPhones {
    var isCheck: Bool = false
    var contact: CNContact

    init(contact: CNContact) {
        self.contact = contact
    }
}
