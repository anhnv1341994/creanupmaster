//
//  ContactsManagerment.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/14/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Contacts
import ContactsUI
import PhoneNumberKit
import SwiftyContacts
import UIKit

class AddressBookContact: NSObject {
    var contactName: String
    var contactPhone: String
    var contactImage: Data?
    var contact: CNContact
    var isSelect: Bool = false
    var listNumber: [String] = []

    init(name: String, phoneNumber: String, image: Data?, contact: CNContact, listNumber: [String] = []) {
        self.contactName = name
        self.contactPhone = phoneNumber
        self.contactImage = image
        self.contact = contact
        self.listNumber = listNumber
    }
}

typealias DidGetListContact = (_ contacts: [AddressBookContact]?) -> Void

class ContactsManagerment: NSObject {
    fileprivate var getContactQueue: OperationQueue
    fileprivate var didGetListContact: DidGetListContact?
    fileprivate var contacts: [AddressBookContact]?
    let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactMiddleNameKey, CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactViewController.descriptorForRequiredKeys()] as [Any]

    override init() {
        let queue = OperationQueue()
        queue.name = "getAllContacts"
        queue.maxConcurrentOperationCount = 1
        queue.qualityOfService = .background
        self.getContactQueue = queue

        super.init()
    }

    deinit {
        self.cancelRequestContact()
    }

    func cancelRequestContact() {
        self.getContactQueue.cancelAllOperations()
    }

    func requestContact(completion: (([Contact]) -> Void)?) {
        let operation = BlockOperation()
        operation.addExecutionBlock { [weak operation] in
            fetchContacts(keysToFetch: self.keys as! [CNKeyDescriptor], order: .userDefault) { result in
                switch result {
                case .success(let contacts):

                    let list = contacts.map { Contact(contact: $0) }

                    if operation?.isCancelled ?? true {
                        return
                    }
                    OperationQueue.main.addOperation {
                        completion?(list)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
        self.getContactQueue.addOperation(operation)
    }

    func removeContact(contact: CNContact, completion: ((Bool) -> Void)?) {
        let ct = contact.mutableCopy() as! CNMutableContact
        deleteContact(Contact: ct) { result in
            switch result {
            case .success(let bool):
                completion?(bool)
                if bool {
                    print("Contact Sucessfully Deleted")
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func mergeAllDuplicatesFullName(duplicates: [CNContact]) -> CNContact {
        // CNCONTACT PROPERTIES

        var namePrefix: [String] = [String]()
        var givenName: [String] = [String]()
        var middleName: [String] = [String]()
        var familyName: [String] = [String]()
        var previousFamilyName: [String] = [String]()
        var nameSuffix: [String] = [String]()
        var nickname: [String] = [String]()
        var organizationName: [String] = [String]()
        var departmentName: [String] = [String]()
        var jobTitle: [String] = [String]()
        var phoneNumbers: [CNPhoneNumber] = [CNPhoneNumber]()
        var emailAddresses: [NSString] = [NSString]()
        var postalAddresses: [CNPostalAddress] = [CNPostalAddress]()
        var urlAddresses: [NSString] = [NSString]()

        var contactRelations: [CNContactRelation] = [CNContactRelation]()
        var socialProfiles: [CNSocialProfile] = [CNSocialProfile]()
        var instantMessageAddresses: [CNInstantMessageAddress] = [CNInstantMessageAddress]()

        // Filter
        for items in duplicates {
            namePrefix.append(items.namePrefix)
            givenName.append(items.givenName)
            middleName.append(items.middleName)
            familyName.append(items.familyName)
            previousFamilyName.append(items.previousFamilyName)
            nameSuffix.append(items.nameSuffix)
            nickname.append(items.nickname)
            organizationName.append(items.organizationName)
            departmentName.append(items.departmentName)
            jobTitle.append(items.jobTitle)

            for number in items.phoneNumbers {
                phoneNumbers.append(number.value)
            }
            for email in items.emailAddresses {
                emailAddresses.append(email.value)
            }
            for postal in items.postalAddresses {
                postalAddresses.append(postal.value)
            }
            for url in items.urlAddresses {
                urlAddresses.append(url.value)
            }
            for relation in items.contactRelations {
                contactRelations.append(relation.value)
            }
            for social in items.socialProfiles {
                socialProfiles.append(social.value)
            }
            for message in items.instantMessageAddresses {
                instantMessageAddresses.append(message.value)
            }
        }

        let newContact = CNMutableContact()
        newContact.namePrefix = duplicates.first?.namePrefix ?? "merger error"
        newContact.givenName = duplicates.first?.givenName ?? "merger error"
        newContact.middleName = duplicates.first?.middleName ?? "merger error"
        newContact.familyName = duplicates.first?.familyName ?? "merger error"
        newContact.previousFamilyName = duplicates.first?.previousFamilyName ?? "merger error"
        newContact.nameSuffix = duplicates.first?.nameSuffix ?? "merger error"
        newContact.nickname = duplicates.first?.nickname ?? "merger error"
        newContact.organizationName = duplicates.first?.organizationName ?? "merger error"
        newContact.departmentName = duplicates.first?.departmentName ?? "merger error"
        newContact.jobTitle = duplicates.first?.jobTitle ?? "merger error"
        for item in Array(Set(phoneNumbers)) {
            newContact.phoneNumbers.append(CNLabeledValue(label: CNLabelHome, value: item))
        }
        for item in Array(Set(emailAddresses)) {
            newContact.emailAddresses.append(CNLabeledValue(label: CNLabelHome, value: item))
        }
        for item in Array(Set(postalAddresses)) {
            newContact.postalAddresses.append(CNLabeledValue(label: CNLabelHome, value: item))
        }
        for item in Array(Set(urlAddresses)) {
            newContact.urlAddresses.append(CNLabeledValue(label: CNLabelHome, value: item))
        }
        for item in Array(Set(contactRelations)) {
            newContact.contactRelations.append(CNLabeledValue(label: CNLabelHome, value: item))
        }
        for item in Array(Set(socialProfiles)) {
            newContact.socialProfiles.append(CNLabeledValue(label: CNLabelHome, value: item))
        }
        for item in Array(Set(instantMessageAddresses)) {
            newContact.instantMessageAddresses.append(CNLabeledValue(label: CNLabelHome, value: item))
        }

        return newContact
    }

    func mergeAllDuplicatesPhoneNumber(ctMain: CNContact, duplicates: [CNContact], completion: ((Bool) -> Void)?) {
        // Filter
        let listNumber: [String] = ctMain.phoneNumbers.map({ $0.value.stringValue.parsePhoneNumber })

        for items in duplicates {
            let listPhoneCurrent: [String] = items.phoneNumbers.map({ $0.value.stringValue.parsePhoneNumber })

            let listContains = Set(listNumber).intersection(Set(listPhoneCurrent))

            var listPhoneNew: [CNPhoneNumber] = []
            let contactNew = items.mutableCopy() as! CNMutableContact
            contactNew.phoneNumbers.removeAll()

            for number in items.phoneNumbers {
                if !listContains.contains(number.value.stringValue.parsePhoneNumber) {
                    listPhoneNew.append(number.value)
                }
            }

            for item in Array(Set(listPhoneNew)) {
                contactNew.phoneNumbers.append(CNLabeledValue(label: CNLabelHome, value: item))
            }

            updateContact(Contact: contactNew) { result in
                switch result {
                case .success(let bool):
                    completion?(bool)
                    print("isUpdate contact ", bool)
                case .failure(let error):
                    completion?(false)
                    print(error.localizedDescription)
                }
            }
        }
    }
}

extension String {
    var parsePhoneNumber: String {
        let phoneNumberKit = PhoneNumberKit()
        if let phoneNumber = try? phoneNumberKit.parse(self, ignoreType: true) {
            let pNumber = phoneNumberKit.format(phoneNumber, toType: .national, withPrefix: false).replacingOccurrences(of: " ", with: "")
            return pNumber
        }
        return self
    }
}
