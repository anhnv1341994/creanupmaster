//
//  AddressBookDatasource.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/07/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import Contacts
import Foundation

struct AddressBookDatasource {
    static func listData() -> [AddressBookHeader] {
        let items1 = [
            AddressBookItem(image: "addressBook", title: "kManagerContacts".localized(), type: .ManagerContacts),
        ]
        
        let items2 = [
            AddressBookItem(image: "persion", title: "kNameRepetition".localized(), type: .NameRepetition),
            AddressBookItem(image: "phone", title: "kRepeatedNumber".localized(), type: .RepeatedNumber),
        ]
        
        let items3 = [
            AddressBookItem(image: "persion1", title: "kUnknownNumber".localized(), type: .UnknownNumber),
            AddressBookItem(image: "phone1", title: "kNoNumberContact".localized(), type: .NoNumberContact),
        ]
        
        return [
            AddressBookHeader(titleHeader: nil, items: items1),
            AddressBookHeader(titleHeader: "kDuplicateContact".localized(), items: items2),
            AddressBookHeader(titleHeader: "kNoNumberContact".localized(), items: items3),
        ]
    }
}

enum AddressBookType {
    case ManagerContacts
    case NameRepetition
    case RepeatedNumber
    case UnknownNumber
    case NoNumberContact
}

struct AddressBookHeader {
    var titleHeader: String?
    var items: [AddressBookItem]
}

struct AddressBookItem {
    var image: String?
    var title: String?
    var type: AddressBookType
}


class ContactGroup {
    var name: String?
    var ctMain: CNContact
    var duplicates: [Contact]
    
    var isShowButton: Bool {
        return self.countCTSelect > 0
    }
    
    var isSelect: Bool {
        return self.countCTSelect == self.duplicates.count
    }
    
    var countCTSelect: Int {
        return duplicates.filter { $0.isSelect }.count
    }
    
    var contactsSelect: [Contact] {
        let contactsSelect = duplicates.filter { $0.isSelect }
        var newContacts: [Contact] = []
        for ct in contactsSelect {
            ct.isSelect = false
            newContacts.append(ct)
        }
        return newContacts
    }
    
    var contactsNotSelect: [Contact] {
        let contactsNotSelect = duplicates.filter { !$0.isSelect }
        var newContacts: [Contact] = []
        for ct in contactsNotSelect {
            ct.isSelect = false
            newContacts.append(ct)
        }
        return newContacts
    }
    
    init(name: String? = nil, ctMain: CNContact, duplicates: [Contact]) {
        self.name = name
        self.ctMain = ctMain
        self.duplicates = duplicates
    }
}

class Contact {
    var isSelect: Bool = false
    var contact: CNContact
    
    var contactIndexName: String? {
        if let nameIndex = self.contact.fullName?.firstLetter().localizedUppercase, nameIndex.isAlpha {
            return nameIndex
        } else {
            return "#"
        }
    }
    
    func doSearch(_ searchString: String) -> Bool {
        if let contactName = self.contact.fullName, contactName.lowercased().unsign().contains(searchString) {
            return true
        } else if let phoneNumber = self.contact.phones, phoneNumber.lowercased().unsign().contains(searchString) {
            return true
        }
        return false
    }
    
    init(contact: CNContact) {
        self.contact = contact
    }
    
    
}


class ContactRepeat: NSObject {
    var contactMain: Contact?
    var isSelect: Bool {
        return contactDuplicate.filter { $0.isSelect == true }.count == contactDuplicate.count
    }
    var countSelect: Int {
        return contactDuplicate.filter { $0.isSelect == true }.count
    }
    var contactDuplicate: [Contact] = []
}
