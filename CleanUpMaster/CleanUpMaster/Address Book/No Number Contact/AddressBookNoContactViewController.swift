//
//  AddressBookRepeatedViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/18/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import ContactsUI
import PhoneNumberKit
import SwiftyContacts
import UIKit

class AddressBookNoContactViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
            self.tableView.register(cellType: NoNumberContactTableViewCell.self)
            self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        }
    }

    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "kContacts".localized()
        }
    }
    @IBOutlet weak var lbNoData: UILabel! {
        didSet {
            lbNoData.text = "kNoContacts".localized()
        }
    }

    @IBOutlet weak var leftBarButton: UIButton! {
        didSet {
            leftBarButton.setTitle(nil, for: .normal)
            leftBarButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
    }

    @IBOutlet weak var rightBarButton: UIButton! {
        didSet {
            rightBarButton.setTitle(titleSelectAll, for: .normal)
        }
    }

    private var countSelect: Int = 0 {
        didSet {
            let contact = "kContact".localized()
            let selected = "kSelected".localized()
            let ks = "kS".localized()

            self.navTitle.text = "\(countSelect > 0 ? "\(countSelect) " : "")\(contact)\((countSelect > 1) ? "\(ks)" : "") \(selected)"
            self.isSelectAll = (countSelect == totalContact && countSelect != 0)
            self.rightBarButton.setTitle(isSelectAll ? titleDeselectAll : titleSelectAll, for: .normal)
            self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: self.countSelect > 0)
            self.showButton(count: self.contacts.count)
        }
    }

    private var isSelectAll = false

    private let titleSelectAll = "kSelectAll".localized()
    private let titleDeselectAll = "kDeselectAll".localized()
    private var totalContact = 0
    let contactServices = ContactsManagerment()
    private let btnDelete = ButtonDeleteView()
    private var contacts = [Contact]()
    var type: AddressBookType = .ManagerContacts

    lazy var operationQueue: OperationQueue = {
        let queue = OperationQueue.main
        queue.name = "AddressBookNoContact"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.countSelect = 0
        self.getListContact()
        self.setupButtonDelete()
    }

    private func setupButtonDelete() {
        self.btnDelete.showButtonDelete(sView: self.roundedView, isShow: false, alpha: 0)
        self.btnDelete.gotoPolicyView = { [weak self] type in
            let vc = PrivacyPolicyViewController()
            vc.type = type
            self?.pushViewWithScreen(vc)
        }

        self.btnDelete.didDelete = { [weak self] in
            guard let self = self else { return }
            let chooseContact = self.contacts.filter { $0.isSelect }
            for contact in chooseContact {
                self.contactServices.removeContact(contact: contact.contact) { isBool in
                    print("isDelete: \(isBool) ->> \(contact.contact.namePrefix)")
                }
            }
            self.countSelect = self.contacts.filter { $0.isSelect }.count
            self.getListContact()
        }
    }

    private func getListContact() {
        switch type {
        case .NoNumberContact:
            self.checkNoNumberContact()
        case .UnknownNumber:
            self.checkUnknownNumber()
        default:
            break
        }
    }

    private func checkUnknownNumber() {
        Loading.shared.startLoading()
        operationQueue.addOperation {
            fetchContacts(keysToFetch: self.contactServices.keys as! [CNKeyDescriptor], order: .userDefault) { result in
                Loading.shared.stopLoading()
                switch result {
                case .success(let contacts):
                    let contactNoNumber = contacts.filter {
                        var isCheckUnknow = false
                        for number in $0.phoneNumbers {
                            if !number.value.stringValue.isPhoneNumber {
                                isCheckUnknow = true
                                break
                            }
                        }
                        return isCheckUnknow
                    }
                    self.contacts = contactNoNumber.map { Contact(contact: $0) }
                    self.totalContact = self.contacts.count
                    self.showButton(count: self.contacts.count)
                    self.reloadData()
                case .failure(let error):
                    print(error)
                }
            }
            self.reloadData()
        }
    }

    private func checkNoNumberContact() {
        Loading.shared.startLoading()
        operationQueue.addOperation {
            fetchContacts(keysToFetch: self.contactServices.keys as! [CNKeyDescriptor], order: .userDefault) { [weak self] result in
                Loading.shared.stopLoading()

                guard let self = self else { return }
                switch result {
                case .success(let contacts):
                    let contactNoNumber = contacts.filter { $0.phoneNumbers.count == 0 }
                    self.contacts = contactNoNumber.map { Contact(contact: $0) }
                    self.totalContact = self.contacts.count
                    self.showButton(count: self.contacts.count)
                    self.reloadData()
                case .failure(let error):
                    print(error)
                }
            }
            self.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.btnDelete.removeFromSuperview()
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionRight(_ sender: Any) {
        self.countSelect = self.isSelectAll ? 0 : self.totalContact
        self.contacts.forEach { $0.isSelect = self.isSelectAll }
        self.reloadData()
    }

    private func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    func showButton(count: Int) {
        if self.tableView != nil, self.rightBarButton != nil {
            self.tableView.isHidden = count == 0
            self.rightBarButton.alpha = count == 0 ? 0 : 1
            self.rightBarButton.isEnabled = count == 0 ? false : true
        }
    }
}

extension AddressBookNoContactViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: NoNumberContactTableViewCell.self, for: indexPath)
        let contact = self.contacts[indexPath.row]
        cell.setupData(contact: contact)
        cell.btnRadio.setImage(contact.isSelect ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        cell.didSelect = { [weak self] in
            guard let self = self else { return }
            contact.isSelect = !contact.isSelect
            self.countSelect = self.contacts.filter { $0.isSelect }.count
            cell.btnRadio.setImage(contact.isSelect ? #imageLiteral(resourceName: "check").tintColor(.color2): #imageLiteral(resourceName: "unCheck").tintColor(.color2), for: .normal)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let contact = self.contacts[indexPath.item].contact
        let contactVC = CNContactViewController(for: contact)
        contactVC.allowsEditing = true
        contactVC.allowsActions = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.pushViewController(contactVC, animated: true)
    }
}

extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
//            let matches = detector.matchesInString(self, options: [], range: NSMakeRange(0, self.characters.count))
            let matches = detector.matches(in: self, options: [], range: NSRange(location: 0, length: self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}
