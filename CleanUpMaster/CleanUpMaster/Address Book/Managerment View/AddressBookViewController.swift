//
//  AddressBookViewController.swift
//  CleanUpMaster
//
//  Created by Nguyen Van Anh on 01/08/2021.
//  Copyright © 2021 Nguyen Anh. All rights reserved.
//

import UIKit
import Contacts
import SwiftyContacts
import ContactsUI

class AddressBookViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.rowHeight = 95
            tableView.estimatedRowHeight = 95
            tableView.register(cellType: AddressBookTableViewCell.self)
        }
    }
    @IBOutlet weak var navTitle: UILabel! {
        didSet {
            navTitle.text = "kContacts".localized()
        }
    }
    
    let contactServices = ContactsManagerment()
    private var addressBooks = AddressBookDatasource.listData()
    private var contacts: [CNContact] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchContacts()
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    deinit {
        print(String(describing: self.classForCoder))
    }
}

extension AddressBookViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.addressBooks.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addressBooks[section].items.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = AddressBookHeaderView()
        let section = self.addressBooks[section]
        header.lbTitleHeader.text = section.titleHeader
        return header
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == -1 {
            return CGFloat.leastNormalMagnitude
        }
        let section = self.addressBooks[section]
        if section.titleHeader != nil {
            return 50
        }
        return 0.1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.addressBooks[indexPath.section].items[indexPath.item]
        let cell = tableView.dequeueReusableCell(with: AddressBookTableViewCell.self, for: indexPath)
        cell.setDataForRow(item: item)
        return cell
    }
    
    private func fetchContacts() {
        SwiftyContacts.fetchContacts(keysToFetch: self.contactServices.keys as! [CNKeyDescriptor], order: .userDefault) { result in
            switch result {
            case .success(let contacts):
                self.contacts = contacts
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension AddressBookViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.addressBooks[indexPath.section].items[indexPath.item]
        switch item.type {
        case .ManagerContacts:
            let allContactsVC = DetailAllContactsViewController()
            self.navigationController?.pushViewController(allContactsVC, animated: true)
        case .NameRepetition:
            let allContactsVC = AddressBookRepeatedViewController()
            allContactsVC.type = item.type
            allContactsVC.contacts = self.contacts
            self.navigationController?.pushViewController(allContactsVC, animated: true)
        case .RepeatedNumber:
            let allContactsVC = AddressBookRepeatedViewController()
            allContactsVC.type = item.type
            allContactsVC.contacts = self.contacts
            self.navigationController?.pushViewController(allContactsVC, animated: true)
        case .NoNumberContact:
            let noNumberVC = AddressBookNoContactViewController()
            noNumberVC.type = item.type
            self.navigationController?.pushViewController(noNumberVC, animated: true)
        case .UnknownNumber:
            let noNumberVC = AddressBookNoContactViewController()
            noNumberVC.type = item.type
            self.navigationController?.pushViewController(noNumberVC, animated: true)
        }
    }
}
